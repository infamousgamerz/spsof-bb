Welcome to SPSOF!

This file serves as a sort of FAQ and getting started guide for the program.

Let's begin...


-----------------
Q. What is SPSOF?
-----------------
A. SPSOF is a proxy program for use with the Phantasy Star Online Blue Burst client.  (There was a Gamecube
version of this program but it has since been discontinued.  You can still find it available on the
SPSOF homepage @ http://www.pioneer2.net)


----------------------------
Q. What can I do with SPSOF?
----------------------------
A. SPSOF provides protection from malicious hackers who would wish to do you wrong in the game, as well as
provide many ways to cheat via packet sending and chat commands.  You can find a full list of the commands
you can use with SPSOF in a file named fullchat.txt which is included in the archive, though sometimes
the commands may be better explained in the history file, spsof_history.txt

If you're new to SPSOF, I'd recommend you glance at both.


--------------------------------------
Q. So how do I get started with SPSOF?
--------------------------------------
A. First, extract the program into it's own folder on the hard drive.

Whatever server you plan to use this program with, you will need to modify your HOSTS file and possibly
your serverip.txt or serverip2.txt file.

Please consult fullchat.txt for more details.

Once that's all said and done, you should make a shortcut to spsof_bb.exe on your desktop.  If you are
using SPSOF to connect to the official Japanese server, please add a " -a", notice the space, to the
end of the shortcut.  (You can exclude the quotations.)

Double click the shortcut to launch the proxy then run online.exe

You should notice a connection from the client, right away, to the proxy... even during patching.

To use any of the commands listed in fullchat.txt  simply type them in as you normally speak in game.

If you do not see a bunch of scrolling hexadecimal values in the main SPSOF window while you are online,
I wouldn't recommend using any of the commands as it most likely means you did not successfully make a
connection to the server with the proxy.  (You may need to edit proxyip.txt, consult the file for details)

But, if you did, basically, you know how you type "Hello" to people?  You'll be typing commands there instead...
the proxy will intercept them and execute what you ask accordingly.

For example, if in an Episode 1 game, typing $warp 11  would warp you to the Dragon.

It should be noted there are more command line parameters than just -j for connecting to the Japanese
server, you can consult fullchat.txt for more details.


------------------------------------------------------------------------------------------
Q. Where do I find a full list of commands and command line parameters the proxy supports?
------------------------------------------------------------------------------------------
A. Please consult fullchat.txt, it comes with the program.  If, for some reason, your archive does not
have this file included, you can redownload a complete version of the program from http://www.pioneer2.net


-------------------------------------------------------------
Q. How do I send customized packets? (Advanced user question)
-------------------------------------------------------------
A. In the GUI window that pops up with the program, there is an edit control at the very top.  Input your
custom packet data here and either hit enter or click the button to the right of the control.


-----------------------------------------------------------------------------
Q. I've used the $qitem command but the items did not appear in my inventory!
-----------------------------------------------------------------------------
A. Did you change blocks after arriving in the lobby?  Were you even in a quest when you used the command?
Did you make sure to use $qitem with a parameter of an item that the server is confirmed to give out?  
You can check http://www.pioneer2.net/final_create.txt  for a list of items the server will gladly give
out.  If you want to generate any other permanent items on this list, you can only do it via Dangerous 
Deal with Black Paper 1 or 2.

Consult spsof_history.txt for information regarding DDWBP1 & 2 item generation.


-----------------------------
Q. What does SPSOF stand for?
-----------------------------
A. Sodaboy's Phantasy Star Online(PSO) Firewall


... if you have any more questions either use the tag board on http://www.pioneer2.net/ or e-mail me at
ragnarok@vengefulsoda.com

Peace.

- Sodaboy