// SPSOF.C - A proxy for the Blue Burst version of Phantasy Star Online. 

// define _MT so that _beginthread( ) is available 


#ifndef _MT 
#define _MT 
#endif 

#define _CRT_SECURE_NO_DEPRECATE

#define CALLING cdecl


// Disable chat windows?

//#define NO_CHAT


// Disable all windows?

//#define NO_WINDOWS


// Disable .IND support?

#define NO_IND


// Schthack based server?

#define SCHTHACK


// If running a PSO server on the same machine as the proxy.

//#define SERVER_WORKAROUND

#define SPSOF_VERSION "3.38"


#include "time.h"
#include "stdio.h"
#include "windows.h"
#include "process.h"
#include "resource.h"
#define _CRT_SECURE_NO_DEPRECATE
#include "string.h"
#include <winuser.h>
#include <winbase.h>

#include "pso_crypt.h"


#include "bbtable.h"

/* I don't even know why I can't just define the _WIN32_WINNT and include winbase.h for this one. */

WINBASEAPI
BOOL
WINAPI
IsDebuggerPresent(
				  VOID
				  );


/* CRC32 Table used when writing character backup files. */

const unsigned CRCTable[256] =
{0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,

0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,

0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,

0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D};


/* Size check table for 0x6x commands. */

unsigned short size_check_table [] =
{ 
	0x00, 0x00,
	0x01, 0x00,
	0x02, 0x00,
	0x03, 0x00,
	0x04, 0x00,
	0x05, 0x10,
	0x06, 0x98,
	0x07, 0x48,
	0x08, 0x00,
	0x09, 0x00,
	0x0A, 0x00,
	0x0B, 0x00,
	0x0C, 0x00,
	0x0D, 0x10,
	0x0E, 0x00,
	0x0F, 0x00,
	0x10, 0x00,
	0x11, 0x00,
	0x12, 0x00,
	0x13, 0x00,
	0x14, 0x00,
	0x15, 0x00,
	0x16, 0x00,
	0x17, 0x00,
	0x18, 0x00,
	0x19, 0x00,
	0x1A, 0x00,
	0x1B, 0x00,
	0x1C, 0x00,
	0x1D, 0x00,
	0x1E, 0x00,
	0x1F, 0x0C,
	0x20, 0x1C,
	0x21, 0x00,
	0x22, 0x08,
	0x23, 0x08,
	0x24, 0x00,
	0x25, 0x00,
	0x26, 0x00,
	0x27, 0x00,
	0x28, 0x00,
	0x29, 0x00,
	0x2A, 0x00,
	0x2B, 0x00,
	0x2C, 0x18,
	0x2D, 0x08,
	0x2E, 0x00,
	0x2F, 0x00,
	0x30, 0x00,
	0x31, 0x00,
	0x32, 0x00,
	0x33, 0x00,
	0x34, 0x00,
	0x35, 0x00,
	0x36, 0x00,
	0x37, 0x00,
	0x38, 0x00,
	0x39, 0x00,
	0x3A, 0x08,
	0x3B, 0x08,
	0x3C, 0x00,
	0x3D, 0x00,
	0x3E, 0x1C,
	0x3F, 0x1C,
	0x40, 0x14,
	0x41, 0x00,
	0x42, 0x10,
	0x43, 0x00,
	0x44, 0x00,
	0x45, 0x00,
	0x46, 0x00,
	0x47, 0x00,
	0x48, 0x00,
	0x49, 0x00,
	0x4A, 0x00,
	0x4B, 0x00,
	0x4C, 0x00,
	0x4D, 0x00,
	0x4E, 0x00,
	0x4F, 0x00,
	0x50, 0x00,
	0x51, 0x00,
	0x52, 0x10,
	0x53, 0x00,
	0x54, 0x00,
	0x55, 0x00,
	0x56, 0x00,
	0x57, 0x00,
	0x58, 0x0C,
	0x59, 0x00,
	0x5A, 0x00,
	0x5B, 0x00,
	0x5C, 0x00,
	0x5D, 0x00,
	0x5E, 0x00,
	0x5F, 0x00,
	0x60, 0x00,
	0x61, 0x00,
	0x62, 0x00,
	0x63, 0x00,
	0x64, 0x00,
	0x65, 0x00,
	0x66, 0x00,
	0x67, 0x00,
	0x68, 0x00,
	0x69, 0x00,
	0x6A, 0x00,
	0x6B, 0x00,
	0x6C, 0x00,
	0x6D, 0x00,
	0x6E, 0x00,
	0x6F, 0x20C,
	0x70, 0x00,
	0x71, 0x08,
	0x72, 0x00,
	0x73, 0x00,
	0x74, 0x24,
	0x75, 0x00,
	0x76, 0x00,
	0x77, 0x00,
	0x78, 0x00,
	0x79, 0x1C,
	0x7A, 0x00,
	0x7B, 0x00,
	0x7C, 0x00,
	0x7D, 0x00,
	0x7E, 0x00,
	0x7F, 0x00,
	0x80, 0x00,
	0x81, 0x00,
	0x82, 0x00,
	0x83, 0x00,
	0x84, 0x00,
	0x85, 0x00,
	0x86, 0x00,
	0x87, 0x00,
	0x88, 0x00,
	0x89, 0x00,
	0x8A, 0x00,
	0x8B, 0x00,
	0x8C, 0x00,
	0x8D, 0x00,
	0x8E, 0x00,
	0x8F, 0x00,
	0x90, 0x00,
	0x91, 0x00,
	0x92, 0x00,
	0x93, 0x00,
	0x94, 0x00,
	0x95, 0x00,
	0x96, 0x00,
	0x97, 0x00,
	0x98, 0x00,
	0x99, 0x00,
	0x9A, 0x00,
	0x9B, 0x00,
	0x9C, 0x00,
	0x9D, 0x00,
	0x9E, 0x00,
	0x9F, 0x00,
	0xA0, 0x00,
	0xA1, 0x00,
	0xA2, 0x00,
	0xA3, 0x00,
	0xA4, 0x00,
	0xA5, 0x00,
	0xA6, 0x00,
	0xA7, 0x00,
	0xA8, 0x00,
	0xA9, 0x00,
	0xAA, 0x00,
	0xAB, 0x0C,
	0xAC, 0x00,
	0xAD, 0x00,
	0xAE, 0x14,
	0xAF, 0x0C,
	0xB0, 0x0C,
	0xB1, 0x00,
	0xB2, 0x00,
	0xB3, 0x00,
	0xB4, 0x00,
	0xB5, 0x00,
	0xB6, 0x00,
	0xB7, 0x00,
	0xB8, 0x00,
	0xB9, 0x00,
	0xBA, 0x00,
	0xBB, 0x00,
	0xBC, 0x00,
	0xBD, 0x00,
	0xBE, 0x00,
	0xBF, 0x0C,
	0xC0, 0x00,
	0xC1, 0x00,
	0xC2, 0x00,
	0xC3, 0x00,
	0xC4, 0x00,
	0xC5, 0x00,
	0xC6, 0x00,
	0xC7, 0x00,
	0xC8, 0x00,
	0xC9, 0x00,
	0xCA, 0x00,
	0xCB, 0x00,
	0xCC, 0x00,
	0xCD, 0x00,
	0xCE, 0x00,
	0xCF, 0x00,
	0xD0, 0x00,
	0xD1, 0x00,
	0xD2, 0x00,
	0xD3, 0x00,
	0xD4, 0x00,
	0xD5, 0x00,
	0xD6, 0x00,
	0xD7, 0x00,
	0xD8, 0x00,
	0xD9, 0x00,
	0xDA, 0x00,
	0xDB, 0x00,
	0xDC, 0x00,
	0xDD, 0x00,
	0xDE, 0x00,
	0xDF, 0x00,
	0xE0, 0x00,
	0xE1, 0x00,
	0xE2, 0x00,
	0xE3, 0x00,
	0xE4, 0x00,
	0xE5, 0x00,
	0xE6, 0x00,
	0xE7, 0x00,
	0xE8, 0x00,
	0xE9, 0x00,
	0xEA, 0x00,
	0xEB, 0x00,
	0xEC, 0x00,
	0xED, 0x00,
	0xEE, 0x00,
	0xEF, 0x00,
	0xF0, 0x00,
	0xF1, 0x00,
	0xF2, 0x00,
	0xF3, 0x00,
	0xF4, 0x00,
	0xF5, 0x00,
	0xF6, 0x00,
	0xF7, 0x00,
	0xF8, 0x00,
	0xF9, 0x00,
	0xFA, 0x00,
	0xFB, 0x00,
	0xFC, 0x00,
	0xFD, 0x00,
	0xFE, 0x00,
	0xFF, 0x00 
};

// Unit check table for $qplay

unsigned char unit_check[0x64];
unsigned char qtechlv = 25;
unsigned char doll_check = 0;
unsigned char grinder_check[3];
unsigned char mat_check[7];

// Packet check table for $qplay

unsigned char quest_OK[] = {
0x00, // 0x00
0x00, // 0x01
0x00, // 0x02
0x00, // 0x03
0x00, // 0x04
0x01, // 0x05
0x00, // 0x06
0x00, // 0x07
0x00, // 0x08
0x00, // 0x09
0x01, // 0x0A
0x01, // 0x0B
0x00, // 0x0C
0x00, // 0x0D
0x00, // 0x0E
0x00, // 0x0F
0x00, // 0x10
0x00, // 0x11
0x01, // 0x12
0x00, // 0x13
0x00, // 0x14
0x00, // 0x15
0x00, // 0x16
0x00, // 0x17
0x00, // 0x18
0x00, // 0x19
0x00, // 0x1A
0x00, // 0x1B
0x00, // 0x1C
0x00, // 0x1D
0x00, // 0x1E
0x00, // 0x1F
0x00, // 0x20
0x00, // 0x21
0x00, // 0x22
0x00, // 0x23
0x00, // 0x24
0x00, // 0x25
0x00, // 0x26
0x00, // 0x27
0x00, // 0x28
0x00, // 0x29
0x00, // 0x2A
0x00, // 0x2B
0x01, // 0x2C
0x01, // 0x2D
0x00, // 0x2E
0x00, // 0x2F
0x00, // 0x30
0x00, // 0x31
0x00, // 0x32
0x00, // 0x33
0x00, // 0x34
0x00, // 0x35
0x00, // 0x36
0x00, // 0x37
0x00, // 0x38
0x00, // 0x39
0x00, // 0x3A
0x00, // 0x3B
0x00, // 0x3C
0x00, // 0x3D
0x01, // 0x3E
0x00, // 0x3F
0x01, // 0x40
0x00, // 0x41
0x01, // 0x42
0x01, // 0x43
0x01, // 0x44
0x01, // 0x45
0x01, // 0x46
0x01, // 0x47
0x01, // 0x48
0x00, // 0x49
0x00, // 0x4A
0x00, // 0x4B
0x00, // 0x4C
0x00, // 0x4D
0x00, // 0x4E
0x00, // 0x4F
0x00, // 0x50
0x00, // 0x51
0x01, // 0x52
0x00, // 0x53
0x00, // 0x54
0x00, // 0x55
0x00, // 0x56
0x00, // 0x57
0x00, // 0x58
0x00, // 0x59
0x00, // 0x5A
0x00, // 0x5B
0x00, // 0x5C
0x00, // 0x5D
0x00, // 0x5E
0x00, // 0x5F
0x01, // 0x60
0x00, // 0x61
0x00, // 0x62
0x00, // 0x63
0x00, // 0x64
0x00, // 0x65
0x00, // 0x66
0x01, // 0x67
0x00, // 0x68
0x00, // 0x69
0x01, // 0x6A
0x00, // 0x6B
0x00, // 0x6C
0x00, // 0x6D
0x00, // 0x6E
0x00, // 0x6F
0x00, // 0x70
0x00, // 0x71
0x00, // 0x72
0x00, // 0x73
0x00, // 0x74
0x01, // 0x75
0x01, // 0x76
0x01, // 0x77
0x00, // 0x78
0x00, // 0x79
0x00, // 0x7A
0x00, // 0x7B
0x00, // 0x7C
0x00, // 0x7D
0x00, // 0x7E
0x00, // 0x7F
0x00, // 0x80
0x00, // 0x81
0x00, // 0x82
0x00, // 0x83
0x00, // 0x84
0x00, // 0x85
0x00, // 0x86
0x00, // 0x87
0x01, // 0x88
0x00, // 0x89
0x00, // 0x8A
0x00, // 0x8B
0x00, // 0x8C
0x01, // 0x8D
0x00, // 0x8E
0x00, // 0x8F
0x00, // 0x90
0x00, // 0x91
0x00, // 0x92
0x00, // 0x93
0x00, // 0x94
0x00, // 0x95
0x00, // 0x96
0x00, // 0x97
0x00, // 0x98
0x00, // 0x99
0x00, // 0x9A
0x00, // 0x9B
0x00, // 0x9C
0x00, // 0x9D
0x00, // 0x9E
0x00, // 0x9F
0x00, // 0xA0
0x00, // 0xA1
0x01, // 0xA2
0x00, // 0xA3
0x00, // 0xA4
0x00, // 0xA5
0x00, // 0xA6
0x00, // 0xA7
0x00, // 0xA8
0x00, // 0xA9
0x00, // 0xAA
0x00, // 0xAB
0x00, // 0xAC
0x00, // 0xAD
0x00, // 0xAE
0x00, // 0xAF
0x00, // 0xB0
0x00, // 0xB1
0x00, // 0xB2
0x00, // 0xB3
0x00, // 0xB4
0x00, // 0xB5
0x00, // 0xB6
0x00, // 0xB7
0x00, // 0xB8
0x00, // 0xB9
0x00, // 0xBA
0x00, // 0xBB
0x00, // 0xBC
0x00, // 0xBD
0x00, // 0xBE
0x00, // 0xBF
0x00, // 0xC0
0x00, // 0xC1
0x00, // 0xC2
0x00, // 0xC3
0x00, // 0xC4
0x00, // 0xC5
0x00, // 0xC6
0x00, // 0xC7
0x01, // 0xC8
0x00, // 0xC9
0x01, // 0xCA
0x00, // 0xCB
0x00, // 0xCC
0x00, // 0xCD
0x00, // 0xCE
0x00, // 0xCF
0x00, // 0xD0
0x00, // 0xD1
0x00, // 0xD2
0x00, // 0xD3
0x00, // 0xD4
0x00, // 0xD5
0x00, // 0xD6
0x00, // 0xD7
0x00, // 0xD8
0x00, // 0xD9
0x00, // 0xDA
0x00, // 0xDB
0x00, // 0xDC
0x00, // 0xDD
0x00, // 0xDE
0x01, // 0xDF
0x01, // 0xE0
0x00, // 0xE1
0x00, // 0xE2
0x00, // 0xE3
0x00, // 0xE4
0x00, // 0xE5
0x00, // 0xE6
0x00, // 0xE7
0x00, // 0xE8
0x00, // 0xE9
0x00, // 0xEA
0x00, // 0xEB
0x00, // 0xEC
0x00, // 0xED
0x00, // 0xEE
0x00, // 0xEF
0x00, // 0xF0
0x00, // 0xF1
0x00, // 0xF2
0x00, // 0xF3
0x00, // 0xF4
0x00, // 0xF5
0x00, // 0xF6
0x00, // 0xF7
0x00, // 0xF8
0x00, // 0xF9
0x00, // 0xFA
0x00, // 0xFB
0x00, // 0xFC
0x00, // 0xFD
0x00, // 0xFE
0x00  // 0xFF
 };

unsigned char mainsrv_addr[4];
unsigned char altsrv_addr[4];
unsigned char mainpatch_addr[4];
unsigned char altpatch_addr[4];
unsigned short mainpatch_port, 
mainsrv_port, 
altpatch_port, 
altsrv_port,
BB_PATCH,
BB_PORT;

unsigned char shop_pkt[] = {
	0x10, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB5, 0x02, 0xFF, 0xFF, 0x01, 0x00, 0x00, 0x00
};

unsigned char iwarped_pkt[] = {
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x21, 0x02, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00
};

unsigned char jp_ship_pkt[] = {
	0x68, 0x01, 0xA0, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0xF4, 0xFF, 0xFF, 0xFF,
	0x04, 0x00, 0x57, 0x00, 0x6F, 0x00, 0x72, 0x00, 0x6C, 0x00, 0x64, 0x00, 0x2F, 0x00, 0x4A, 0x00,
	0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x01, 0x01, 0x01, 0x04, 0x00, 0x30, 0x00,
	0x31, 0x00, 0x3A, 0x00, 0x41, 0x00, 0x72, 0x00, 0x69, 0x00, 0x65, 0x00, 0x73, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x12, 0x00, 0x00, 0x02, 0x01, 0x01, 0x04, 0x00, 0x30, 0x00, 0x32, 0x00, 0x3A, 0x00,
	0x54, 0x00, 0x61, 0x00, 0x75, 0x00, 0x72, 0x00, 0x75, 0x00, 0x73, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x00,
	0x00, 0x03, 0x01, 0x01, 0x04, 0x00, 0x30, 0x00, 0x33, 0x00, 0x3A, 0x00, 0x47, 0x00, 0x65, 0x00,
	0x6D, 0x00, 0x69, 0x00, 0x6E, 0x00, 0x69, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x04, 0x01, 0x01,
	0x04, 0x00, 0x30, 0x00, 0x34, 0x00, 0x3A, 0x00, 0x43, 0x00, 0x61, 0x00, 0x6E, 0x00, 0x63, 0x00,
	0x65, 0x00, 0x72, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x05, 0x01, 0x01, 0x04, 0x00, 0x30, 0x00,
	0x35, 0x00, 0x3A, 0x00, 0x4C, 0x00, 0x65, 0x00, 0x6F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x12, 0x00, 0x00, 0x0D, 0x01, 0x01, 0x04, 0x00, 0x39, 0x00, 0x39, 0x00, 0x3A, 0x00,
	0x42, 0x00, 0x65, 0x00, 0x67, 0x00, 0x69, 0x00, 0x6E, 0x00, 0x6E, 0x00, 0x65, 0x00, 0x72, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00,
	0x01, 0x00, 0x00, 0x00, 0x05, 0x00, 0x45, 0x00, 0x78, 0x00, 0x69, 0x00, 0x74, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

unsigned char idle_pkt[] = {
	0x08, 0x00, 0x1D, 0x00, 0x00, 0x00, 0x00, 0x00
};


#ifdef SERVER_WORKAROUND

unsigned char server_gg_pkt[] = {
	0x0E, 0x00, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x39, 0x57, 0xCC, 0x26
};	

unsigned char server_ss_pkt[] = {
	0x0A, 0x00, 0x19, 0x00, 0x00, 0x00, 0x00, 0x00, 0xD0, 0x45
};	

#endif


unsigned char patch_pkt[] = { 
	0x0C, 0x00, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00,
	0x0A, 0x00, 0x04, 0x00, 0x12, 0x00
};


unsigned char relocate_pkt[] = { 
	0x1C, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x05, 0x03, 0x00, 0x00, 0x00, 0xB4, 0xC2,
	0x00, 0x00, 0x00, 0x42, 0x00, 0x40, 0x9C, 0xC5, 0x00, 0x40, 0x00, 0x00
};

/* modified */

unsigned char chat_pkt[] = { 
	0x18, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x09, 0x00, 0x45, 0x00, 0x00, 0x00, 0x00, 0x00 
};


unsigned char quest_pkt[] = { 
	0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0E, 0x00, 0x4D, 0xFF, 0xFF, 0xFF 
};

unsigned char silent_pkt[] = { 
	0xEC, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x41, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char rappy_pkt[] = { 
	0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0E, 0x00, 0x15, 0xFF, 0xFF, 0xFF 
};

unsigned char warp_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x94, 0x02, 0x00, 0x0B, 0x00, 0x00, 0x01, 0x00 
};


unsigned char pk_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9A, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF 
};

/* modified */

unsigned char hide_pkt[] = { 
	0x0C, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x22, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

/* modified */

unsigned char show_pkt[] = { 
	0x0C, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x23, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/* modified */

unsigned char picky_pkt[] = { 
	0x14, 0x00, 0x6D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x02, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 
	0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char dc_pkt[] = { 
	0x10, 0x00, 0xC9, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x05, 0x00, 0xB2, 0xFC, 0x7E, 0x01 
};

unsigned char fakequest_pkt[] = { 
	0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0E, 0x00, 0x3A, 0xFF, 0xFF, 0xFF 
};

unsigned char getgame_pkt[] = { 
	0x08, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00 
};

/* modified */

unsigned char bigb_pkt[] = { 
	0x7C, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 
	0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 
	0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 
	0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 
	0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 
	0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 
	0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

unsigned char qfsod_pkt[] = { 
	0x10, 0x00, 0x6D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x73, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00
};


/* modified */

unsigned char schat_pkt[] = { 
	0x4C, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x11, 0xA0, 0xDE, 0x00, 0x00, 0x00, 0x00, 
	0x28, 0x00, 0x00, 0x00, 0x62, 0x03, 0x62, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0x09, 0x16, 0x1B, 0x00, 
	0x09, 0x2B, 0x1B, 0x01, 0x37, 0x20, 0x2C, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 
	0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 
	0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00 
};

unsigned char fsod_pkt[] = { 
	0x10, 0x00, 0x6D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char crash_pkt[] = { 
	0x10, 0x00, 0x6D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char wschat_pkt[] = { 
	0x28, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x74, 0x08, 0x00, 0x04, 0x02, 0x00, 0x01, 0x00, 
	0x26, 0x01, 0xC7, 0x02, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char run_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x02, 0x08, 0x00, 0x02, 0x00, 0x01, 0x00 
};

unsigned char walk_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x02, 0x08, 0x00, 0x01, 0x00, 0x01, 0x00 
};

unsigned char groove_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x02, 0x08, 0x00, 0x2A, 0x00, 0x01, 0x00 
};

unsigned char fakekill_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x02, 0x04, 0x00, 0x12, 0x00, 0x00, 0x00 
};

unsigned char action_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x02, 0x04, 0x00, 0x12, 0x00, 0x00, 0x00 
};

/* modified */

unsigned char npc_pkt[] = { 
	0x24, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x69, 0x07, 0x01, 0x01, 0x00, 0x00, 0x0B, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char lwarp_pkt[] = { 
	0x10, 0x00, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x1A, 0x00, 0xC0, 0xFF, 0xFF, 0xFF 
};

/* modified */

unsigned char reload_pkt[] = { 
	0x0C, 0x00, 0x62, 0x01, 0x00, 0x00, 0x00, 0x00, 0x71, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}; 

/* modified */

unsigned char npcfsod_pkt[] = { 
	0x2C, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x02, 0x07, 0x00, 0x2C, 0x00, 0x00, 0x00, 
	0x69, 0x07, 0x01, 0x01, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char color_data[3] = { 0x09, 0x43, 0x39 };

/* modified */

unsigned char steal_schat[] = { 
	0x4C, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x11, 0xA0, 0xDE, 0x00, 0x00, 0x00, 0x00, 
	0x28, 0x00, 0x00, 0x00, 0x62, 0x03, 0x62, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0x09, 0x16, 0x1B, 0x00, 
	0x09, 0x2B, 0x1B, 0x01, 0x37, 0x20, 0x2C, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 
	0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 
	0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00 
};

/* modified */

unsigned char gcfsod_pkt[] = { 
	0xA4, 0x00, 0x62, 0x03, 0x00, 0x00, 0x00, 0x00, 0x06, 0x25, 0x1F, 0x00, 0x00, 0x00, 0x01, 0x00, 
	0x12, 0xB3, 0x48, 0x14, 0x53, 0x65, 0x72, 0x61, 0x20, 0x42, 0x6C, 0x75, 0x65, 0x20, 0x20, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char gc_search_pkt[] = { 
	0x18, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x58, 0xB5, 0x4F, 0x01, 
	0x58, 0xB5, 0x4F, 0x01, 0x00, 0x00, 0x00, 0x00 
};

unsigned char event_pkt[] = { 
	0x08, 0x00, 0xDA, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char force_pkt[] = { 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x03, 0x00 
};

unsigned char attack_pkt[] = {
	0x14, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x03, 0x88, 0x10, 0x88, 0x00, 0xFE, 0x07,
	0x00, 0x03, 0xB0, 0x20, 0x00, 0x00, 0x00, 0x00
};

/* modified */

unsigned char kill_pkt[] = { 
	0x14, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC8, 0x03, 0x98, 0x10, 0x98, 0x00, 0x00, 0x00, 
	0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char request_drop_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x76, 0x02, 0x1F, 0x10, 0x01, 0x00, 0x08, 0x00 
};

unsigned char arrow_pkt[] = { 
	0x08, 0x00, 0x89, 0x00, 0x08, 0x00, 0x00, 0x00 
};

unsigned char god_pkt[] = { 
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9A, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF 
};

unsigned char ddos_pkt[] = { 
	0x08, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00 
};


#define OPT_PACKETS

/* Opts packets */

unsigned char runtime_pkt[] = { 
	0x18, 0x01, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0xCD, 0x43, 0x09, 0x09, 0x7E, 0x32, 0xEA, 0x01, 
	0x09, 0x09, 0x4A, 0x09, 0x4F, 0x09, 0x70, 0x09, 0x74, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 	
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 
	0x01, 0x01, 0x06, 0x04, 0x00, 0x00, 0x00, 0x00 
};

unsigned char opt_qfsod_pkt[] = { 
	0x18, 0x02, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6D, 0x43, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 	
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 
	0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x73, 0x00, 0x4F, 0x00, 0x70, 0x00, 0x74, 
	0x73, 0x73, 0x73, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char ninefour_pkt[] = {
	0x18, 0x02, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6D, 0x43, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 
	0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94, 0x00, 0x4F, 0x00, 0x70, 0x00, 0x74, 
	0x94, 0x94, 0x94, 0x00, 0x00, 0x00, 0x00, 0x00
};

unsigned char sevenone_pkt[] = { 
	0x18, 0x02, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6D, 0x43, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 
	0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x00, 0x4F, 0x00, 0x70, 0x00, 0x74, 
	0x71, 0x71, 0x71, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char seedee_pkt[] = { 
	0x18, 0x02, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6D, 0x43, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
	0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0x00, 0x4F, 0x00, 0x70, 0x00, 0x74, 
	0xCD, 0xCD, 0xCD, 0x00, 0x00, 0x00, 0x00, 0x00 
};

unsigned char opt_silent_pkt[] = { 
	0x90, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x03, 0x25, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x85, 0x00, 0x49, 0x00, 0x29, 0x00, 0xAB, 0xAA, 0xAA, 0x3E, 
	0x00, 0x00, 0x00, 0x3F, 0x09, 0x00, 0x45, 0x00, 0x4F, 0x00, 0x70, 0x00, 0x74, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x74, 0x4D, 0xA3, 0x07, 0x00, 0x00, 0x00, 0x00 
};

unsigned char opt_fsod_pkt[] = { 
	0x80, 0x01, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x49, 0x00, 0x6E, 0x00, 0x66, 0x00, 0x6F, 0x00, 0x69, 0x00, 0x6D, 0x00, 0x61, 0x00, 0x74, 0x00, 
	0x69, 0x00, 0x6F, 0x00, 0x6E, 0x00, 0x3A, 0x00, 0x20, 0x00, 0x57, 0x00, 0x65, 0x00, 0x6C, 0x00, 
	0x63, 0x00, 0x6F, 0x00, 0x6D, 0x00, 0x65, 0x00, 0x20, 0x00, 0x74, 0x00, 0x6F, 0x00, 0x20, 0x00, 
	0x73, 0x00, 0x63, 0x00, 0x68, 0x00, 0x74, 0x00, 0x73, 0x00, 0x65, 0x00, 0x69, 0x00, 0x76, 0x00, 
	0x2C, 0x00, 0x20, 0x00, 0x74, 0x00, 0x68, 0x00, 0x69, 0x00, 0x73, 0x00, 0x20, 0x00, 0x69, 0x00, 
	0x73, 0x00, 0x20, 0x00, 0x61, 0x00, 0x20, 0x00, 0x6C, 0x00, 0x65, 0x00, 0x67, 0x00, 0x69, 0x00, 
	0x74, 0x00, 0x20, 0x00, 0x73, 0x00, 0x65, 0x00, 0x69, 0x00, 0x76, 0x00, 0x65, 0x00, 0x69, 0x00, 
	0x20, 0x00, 0x66, 0x00, 0x6F, 0x00, 0x69, 0x00, 0x20, 0x00, 0x61, 0x00, 0x6C, 0x00, 0x6C, 0x00, 
	0x20, 0x00, 0x70, 0x00, 0x6C, 0x00, 0x61, 0x00, 0x79, 0x00, 0x65, 0x00, 0x69, 0x00, 0x73, 0x00, 
	0x20, 0x00, 0x74, 0x00, 0x6F, 0x00, 0x20, 0x00, 0x68, 0x00, 0x61, 0x00, 0x76, 0x00, 0x65, 0x00, 
	0x20, 0x00, 0x66, 0x00, 0x75, 0x00, 0x6E, 0x00, 0x20, 0x00, 0x6F, 0x00, 0x6E, 0x00, 0x2E, 0x00, 
	0x20, 0x00, 0x41, 0x00, 0x6E, 0x00, 0x79, 0x00, 0x20, 0x00, 0x74, 0x00, 0x79, 0x00, 0x70, 0x00, 
	0x65, 0x00, 0x20, 0x00, 0x6F, 0x00, 0x66, 0x00, 0x20, 0x00, 0x68, 0x00, 0x61, 0x00, 0x63, 0x00, 
	0x6B, 0x00, 0x69, 0x00, 0x6E, 0x00, 0x67, 0x00, 0x20, 0x00, 0x61, 0x00, 0x6E, 0x00, 0x64, 0x00, 
	0x20, 0x00, 0x6F, 0x00, 0x69, 0x00, 0x20, 0x00, 0x63, 0x00, 0x68, 0x00, 0x65, 0x00, 0x61, 0x00, 
	0x74, 0x00, 0x69, 0x00, 0x6E, 0x00, 0x67, 0x00, 0x20, 0x00, 0x77, 0x00, 0x69, 0x00, 0x6C, 0x00, 
	0x6C, 0x00, 0x20, 0x00, 0x6E, 0x00, 0x6F, 0x00, 0x74, 0x00, 0x20, 0x00, 0x62, 0x00, 0x65, 0x00, 
	0x20, 0x00, 0x61, 0x00, 0x6C, 0x00, 0x6C, 0x00, 0x6F, 0x00, 0x77, 0x00, 0x65, 0x00, 0x64, 0x00, 
	0x20, 0x00, 0x61, 0x00, 0x6E, 0x00, 0x64, 0x00, 0x20, 0x00, 0x79, 0x00, 0x6F, 0x00, 0x75, 0x00, 
	0x20, 0x00, 0x70, 0x00, 0x69, 0x00, 0x6F, 0x00, 0x62, 0x00, 0x61, 0x00, 0x62, 0x00, 0x6C, 0x00, 
	0x79, 0x00, 0x20, 0x00, 0x77, 0x00, 0x69, 0x00, 0x6C, 0x00, 0x6C, 0x00, 0x20, 0x00, 0x62, 0x00, 
	0x65, 0x00, 0x20, 0x00, 0x62, 0x00, 0x61, 0x00, 0x6E, 0x00, 0x6E, 0x00, 0x65, 0x00, 0x64, 0x00, 
	0x2E, 0x00, 0x00, 0x4F, 0x00, 0x70, 0x00, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
};


// When receiving a 0xBE back from this packet, the item was created.

unsigned char quest_create_pkt [] =
{
	0x20, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0xCA, 0x06, 0xFF, 0xFF, 0x00, 0x02, 0x02, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00
};

unsigned char leavequest_pkt [] =
{
	0x10, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x73, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

unsigned char crystal_pkt [] =
{
	0x0C, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0xDF, 0x01, 0x00, 0x00 

};

unsigned char ddwbp2_pkt [] =
{
	0x18, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x04, 0x00, 0x00, 0x08, 0x04, 0x15, 0x00, 
	0x00, 0x60, 0x60, 0x45, 0x00, 0x00, 0xCD, 0xC3
};

unsigned char ddwbp1_rappy_pkt [] =
{
	0x18, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x04, 0x00, 0x00, 0x03, 0x01, 0x14, 0x00, 
	0x00, 0xA0, 0xC3, 0x44, 0x00, 0x00, 0xB4, 0x43
};

unsigned char ddwbp1_zu_pkt [] =
{
	0x18, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x04, 0x00, 0x00, 0x03, 0x02, 0x5A, 0x00, 
	0x00, 0x00, 0x75, 0xC3, 0x00, 0x00, 0xA5, 0xC3
};

unsigned char ddwbp1_dorphon_pkt [] =
{
	0x18, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x04, 0x00, 0x00, 0x03, 0x00, 0x1E, 0x00, 
	0x00, 0xC0, 0x32, 0x45, 0x00, 0x00, 0xC8, 0xC2
};

unsigned char pickup_pkt [] =
{
	0x14, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x5A, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x08, 0x00, 0x00, 0x00
};

unsigned shoplog = 0;
unsigned shop_tick = 0;
unsigned shopcount = 0;
int logged_in = 0;
int lshopcount = -1;
unsigned shop_step = 0;
FILE* shopf;
int bproute;
int bptimes = 0, 
bp2times = 0, 
pickup_times = 0,
qitemtimes = 0,
request_delay = 0;
unsigned ma4_loop = 0;
unsigned ma4_nummobs;
unsigned loop_waitappear = 0;
unsigned transmission_delay = 0, transmission_id = 0;
unsigned loop_index = 0;
unsigned loop_idle = 0;
unsigned loop_tick = 0;
unsigned loop_wait = 100;
unsigned teamname_list[32][30];
unsigned teampassword[17];
int teamname_count = 0;
unsigned monster_index = 0;

unsigned char loop_game_pkt [] = 
{	0x58, 0x00, 0xC1, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x09, 0x00, 0x45, 0x00, 0x61, 0x00, 0x73, 0x00, 0x64, 0x00, 0x66, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x61, 0x00, 0x31, 0x00, 0x61, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00
};

unsigned char loop_setrare_pkt [] =
{
	0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0B, 0x00, 0x18, 0x05, 0x00, 0x00
};

unsigned char loop_squest_pkt [] = 
{
	0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x0E, 0x00, 0x4B, 0x02, 0x00, 0x00
};

unsigned char loop_switch_pkt [] =
{
	0x14, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x77, 0x03, 0x71, 0x00, 0xA4, 0x00, 0x6B, 0x00, 
	0x01, 0x00, 0x00, 0x00
};

unsigned char loop_switch_pkt2 [] =
{
	0x14, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x03, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 
	0x42, 0x00, 0x05, 0x01
};

// 459 total monsters
// 220 of them being in area 0x05
// 239 of them being in area 0x08

unsigned ma4_monster_list [] =
{
	0x05, 0x00B210B2, 0x05, 0x00B310B3, 0x05, 0x00B410B4, 0x05, 0x00B610B6, 0x05, 0x00B510B5, 0x05, 0x00B710B7, 0x05, 0x00B110B1, 0x05, 0x00AF10AF, 
	0x05, 0x00AE10AE, 0x05, 0x00B010B0, 0x05, 0x00AD10AD, 0x05, 0x00AC10AC, 0x05, 0x00C310C3, 0x05, 0x00C410C4, 0x05, 0x00C210C2, 0x05, 0x00C510C5, 
	0x05, 0x00C710C7, 0x05, 0x00C010C0, 0x05, 0x00C110C1, 0x05, 0x00C610C6, 0x05, 0x00BA10BA, 0x05, 0x00B910B9, 0x05, 0x00BB10BB, 0x05, 0x00BD10BD, 
	0x05, 0x00B810B8, 0x05, 0x00BF10BF, 0x05, 0x00BC10BC, 0x05, 0x00BE10BE, 0x05, 0x009B109B, 0x05, 0x00981098, 0x05, 0x00941094, 0x05, 0x009A109A, 
	0x05, 0x00951095, 0x05, 0x00961096, 0x05, 0x00971097, 0x05, 0x00991099, 0x05, 0x00931093, 0x05, 0x00901090, 0x05, 0x00911091, 0x05, 0x00921092, 
	0x05, 0x008F108F, 0x05, 0x008C108C, 0x05, 0x008D108D, 0x05, 0x008E108E, 0x05, 0x00AB10AB, 0x05, 0x00A910A9, 0x05, 0x00A810A8, 0x05, 0x00A410A4, 
	0x05, 0x00A510A5, 0x05, 0x00A610A6, 0x05, 0x00A710A7, 0x05, 0x00AA10AA, 0x05, 0x009F109F, 0x05, 0x009C109C, 0x05, 0x009D109D, 0x05, 0x009E109E, 
	0x05, 0x00A310A3, 0x05, 0x00A210A2, 0x05, 0x00A010A0, 0x05, 0x00A110A1, 0x05, 0x00E610E6, 0x05, 0x00E410E4, 0x05, 0x00E010E0, 0x05, 0x00E310E3, 
	0x05, 0x00E710E7, 0x05, 0x00E210E2, 0x05, 0x00E510E5, 0x05, 0x00E110E1, 0x05, 0x00DE10DE, 0x05, 0x00DD10DD, 0x05, 0x00DF10DF, 0x05, 0x00EE10EE, 
	0x05, 0x00ED10ED, 0x05, 0x00EF10EF, 0x05, 0x00EB10EB, 0x05, 0x00E810E8, 0x05, 0x00EC10EC, 0x05, 0x00E910E9, 0x05, 0x00EA10EA, 0x05, 0x00CC10CC, 
	0x05, 0x00CE10CE, 0x05, 0x00CD10CD, 0x05, 0x00D110D1, 0x05, 0x00D010D0, 0x05, 0x00CB10CB, 0x05, 0x00CF10CF, 0x05, 0x00CA10CA, 0x05, 0x00C910C9, 
	0x05, 0x00C810C8, 0x05, 0x00DA10DA, 0x05, 0x00DB10DB, 0x05, 0x00D810D8, 0x05, 0x00DC10DC, 0x05, 0x00D510D5, 0x05, 0x00D910D9, 0x05, 0x00D710D7, 
	0x05, 0x00D610D6, 0x05, 0x00D310D3, 0x05, 0x00D210D2, 0x05, 0x00D410D4, 0x05, 0x00861086, 0x05, 0x00871087, 0x05, 0x008A108A, 0x05, 0x00891089, 
	0x05, 0x008B108B, 0x05, 0x00851085, 0x05, 0x00881088, 0x05, 0x00841084, 0x05, 0x00351035, 0x05, 0x00361036, 0x05, 0x00371037, 0x05, 0x00381038, 
	0x05, 0x003B103B, 0x05, 0x003A103A, 0x05, 0x00391039, 0x05, 0x003C103C, 0x05, 0x00301030, 0x05, 0x00311031, 0x05, 0x00341034, 0x05, 0x00321032, 
	0x05, 0x00331033, 0x05, 0x00461046, 0x05, 0x00471047, 0x05, 0x00491049, 0x05, 0x00481048, 0x05, 0x00441044, 0x05, 0x00451045, 0x05, 0x004A104A, 
	0x05, 0x003E103E, 0x05, 0x003F103F, 0x05, 0x00421042, 0x05, 0x00411041, 0x05, 0x003D103D, 0x05, 0x00401040, 0x05, 0x00431043, 0x05, 0x001C101C, 
	0x05, 0x001D101D, 0x05, 0x001E101E, 0x05, 0x001F101F, 0x05, 0x00201020, 0x05, 0x00211021, 0x05, 0x00231023, 0x05, 0x00221022, 0x05, 0x001B101B, 
	0x05, 0x00171017, 0x05, 0x00161016, 0x05, 0x00151015, 0x05, 0x00141014, 0x05, 0x00181018, 0x05, 0x00191019, 0x05, 0x001A101A, 0x05, 0x002F102F, 
	0x05, 0x002E102E, 0x05, 0x002D102D, 0x05, 0x002B102B, 0x05, 0x002C102C, 0x05, 0x00271027, 0x05, 0x00261026, 0x05, 0x00251025, 0x05, 0x00241024, 
	0x05, 0x00281028, 0x05, 0x00291029, 0x05, 0x002A102A, 0x05, 0x00501050, 0x05, 0x00511051, 0x05, 0x004D104D, 0x05, 0x004B104B, 0x05, 0x004E104E, 
	0x05, 0x004C104C, 0x05, 0x004F104F, 0x05, 0x00771077, 0x05, 0x00751075, 0x05, 0x00761076, 0x05, 0x00721072, 0x05, 0x00701070, 0x05, 0x00731073, 
	0x05, 0x00711071, 0x05, 0x00741074, 0x05, 0x006F106F, 0x05, 0x00681068, 0x05, 0x00691069, 0x05, 0x006A106A, 0x05, 0x006E106E, 0x05, 0x006D106D, 
	0x05, 0x006B106B, 0x05, 0x006C106C, 0x05, 0x00801080, 0x05, 0x007F107F, 0x05, 0x00831083, 0x05, 0x00821082, 0x05, 0x00811081, 0x05, 0x007C107C, 
	0x05, 0x007E107E, 0x05, 0x007B107B, 0x05, 0x007D107D, 0x05, 0x00781078, 0x05, 0x00791079, 0x05, 0x007A107A, 0x05, 0x005D105D, 0x05, 0x005C105C, 
	0x05, 0x00571057, 0x05, 0x00581058, 0x05, 0x00591059, 0x05, 0x005A105A, 0x05, 0x005B105B, 0x05, 0x00551055, 0x05, 0x00561056, 0x05, 0x00541054, 
	0x05, 0x00531053, 0x05, 0x00521052, 0x05, 0x00641064, 0x05, 0x00661066, 0x05, 0x00651065, 0x05, 0x00671067, 0x05, 0x00631063, 0x05, 0x00611061, 
	0x05, 0x00601060, 0x05, 0x005E105E, 0x05, 0x00621062, 0x05, 0x005F105F, 0x08, 0x015E115E, 0x08, 0x015F115F, 0x08, 0x01601160, 0x08, 0x015D115D, 
	0x08, 0x015C115C, 0x08, 0x015B115B, 0x08, 0x015A115A, 0x08, 0x01591159, 0x08, 0x01581158, 0x08, 0x01571157, 0x08, 0x01541154, 0x08, 0x01531153, 
	0x08, 0x01551155, 0x08, 0x01561156, 0x08, 0x01671167, 0x08, 0x01621162, 0x08, 0x01631163, 0x08, 0x01611161, 0x08, 0x01661166, 0x08, 0x01641164, 
	0x08, 0x01651165, 0x08, 0x01751175, 0x08, 0x01761176, 0x08, 0x01731173, 0x08, 0x01721172, 0x08, 0x01741174, 0x08, 0x01701170, 0x08, 0x016F116F, 
	0x08, 0x01711171, 0x08, 0x016C116C, 0x08, 0x016B116B, 0x08, 0x016A116A, 0x08, 0x01681168, 0x08, 0x016D116D, 0x08, 0x016E116E, 0x08, 0x01691169, 
	0x08, 0x013C113C, 0x08, 0x013D113D, 0x08, 0x013E113E, 0x08, 0x013F113F, 0x08, 0x01381138, 0x08, 0x01391139, 0x08, 0x013A113A, 0x08, 0x013B113B, 
	0x08, 0x01311131, 0x08, 0x01301130, 0x08, 0x01321132, 0x08, 0x01331133, 0x08, 0x01371137, 0x08, 0x01351135, 0x08, 0x01361136, 0x08, 0x01341134, 
	0x08, 0x012B112B, 0x08, 0x012D112D, 0x08, 0x012A112A, 0x08, 0x012F112F, 0x08, 0x01281128, 0x08, 0x01291129, 0x08, 0x012E112E, 0x08, 0x012C112C, 
	0x08, 0x014E114E, 0x08, 0x014F114F, 0x08, 0x01501150, 0x08, 0x01511151, 0x08, 0x01521152, 0x08, 0x014D114D, 0x08, 0x014C114C, 0x08, 0x014B114B, 
	0x08, 0x01491149, 0x08, 0x014A114A, 0x08, 0x01471147, 0x08, 0x01481148, 0x08, 0x01461146, 0x08, 0x01441144, 0x08, 0x01451145, 0x08, 0x01401140, 
	0x08, 0x01421142, 0x08, 0x01431143, 0x08, 0x01411141, 0x08, 0x01271127, 0x08, 0x01121112, 0x08, 0x010F110F, 0x08, 0x01111111, 0x08, 0x01101110, 
	0x08, 0x010E110E, 0x08, 0x010C110C, 0x08, 0x010A110A, 0x08, 0x01091109, 0x08, 0x01071107, 0x08, 0x01081108, 0x08, 0x010B110B, 0x08, 0x01051105, 
	0x08, 0x01041104, 0x08, 0x01061106, 0x08, 0x01211121, 0x08, 0x01251125, 0x08, 0x01241124, 0x08, 0x01231123, 0x08, 0x01261126, 0x08, 0x011F111F, 
	0x08, 0x01201120, 0x08, 0x01221122, 0x08, 0x011B111B, 0x08, 0x011D111D, 0x08, 0x011E111E, 0x08, 0x011C111C, 0x08, 0x011A111A, 0x08, 0x01161116, 
	0x08, 0x01151115, 0x08, 0x01171117, 0x08, 0x01191119, 0x08, 0x01181118, 0x08, 0x01141114, 0x08, 0x01131113, 0x08, 0x01831183, 0x08, 0x01821182, 
	0x08, 0x01811181, 0x08, 0x01861186, 0x08, 0x01841184, 0x08, 0x01851185, 0x08, 0x017F117F, 0x08, 0x01801180, 0x08, 0x017E117E, 0x08, 0x017A117A, 
	0x08, 0x01781178, 0x08, 0x01791179, 0x08, 0x017D117D, 0x08, 0x01771177, 0x08, 0x017C117C, 0x08, 0x017B117B, 0x08, 0x018A118A, 0x08, 0x018C118C, 
	0x08, 0x018F118F, 0x08, 0x018B118B, 0x08, 0x018E118E, 0x08, 0x018D118D, 0x08, 0x01871187, 0x08, 0x01891189, 0x08, 0x01881188, 0x08, 0x00FC10FC, 
	0x08, 0x00FB10FB, 0x08, 0x00FD10FD, 0x08, 0x01021102, 0x08, 0x01031103, 0x08, 0x01011101, 0x08, 0x01001100, 0x08, 0x00FF10FF, 0x08, 0x00FE10FE, 
	0x08, 0x00F810F8, 0x08, 0x00F910F9, 0x08, 0x00FA10FA, 0x08, 0x00F010F0, 0x08, 0x00F110F1, 0x08, 0x00F410F4, 0x08, 0x00F510F5, 0x08, 0x00F310F3, 
	0x08, 0x00F210F2, 0x08, 0x00F710F7, 0x08, 0x00F610F6, 0x08, 0x01C111C1, 0x08, 0x01C211C2, 0x08, 0x01C511C5, 0x08, 0x01C611C6, 0x08, 0x01C311C3, 
	0x08, 0x01C411C4, 0x08, 0x01BB11BB, 0x08, 0x01BC11BC, 0x08, 0x01BD11BD, 0x08, 0x01BE11BE, 0x08, 0x01C011C0, 0x08, 0x01BF11BF, 0x08, 0x01B811B8, 
	0x08, 0x01B911B9, 0x08, 0x01BA11BA, 0x08, 0x01C911C9, 0x08, 0x01C811C8, 0x08, 0x01C711C7, 0x08, 0x01DA11DA, 0x08, 0x01DB11DB, 0x08, 0x01D811D8, 
	0x08, 0x01D911D9, 0x08, 0x01DC11DC, 0x08, 0x01DD11DD, 0x08, 0x01DE11DE, 0x08, 0x01DF11DF, 0x08, 0x01D211D2, 0x08, 0x01D311D3, 0x08, 0x01D611D6, 
	0x08, 0x01D711D7, 0x08, 0x01D411D4, 0x08, 0x01D511D5, 0x08, 0x01D111D1, 0x08, 0x01CC11CC, 0x08, 0x01D011D0, 0x08, 0x01CF11CF, 0x08, 0x01CB11CB, 
	0x08, 0x01CE11CE, 0x08, 0x01CA11CA, 0x08, 0x01CD11CD, 0x08, 0x019F119F, 0x08, 0x01A011A0, 0x08, 0x019D119D, 0x08, 0x019E119E, 0x08, 0x019C119C, 
	0x08, 0x01951195, 0x08, 0x01961196, 0x08, 0x01991199, 0x08, 0x019A119A, 0x08, 0x01941194, 0x08, 0x019B119B, 0x08, 0x01971197, 0x08, 0x01981198, 
	0x08, 0x01911191, 0x08, 0x01931193, 0x08, 0x01901190, 0x08, 0x01921192, 0x08, 0x01A511A5, 0x08, 0x01A111A1, 0x08, 0x01A311A3, 0x08, 0x01A211A2, 
	0x08, 0x01A411A4, 0x08, 0x01A611A6, 0x08, 0x01A711A7, 0x08, 0x01B211B2, 0x08, 0x01B311B3, 0x08, 0x01B611B6, 0x08, 0x01B711B7, 0x08, 0x01B111B1, 
	0x08, 0x01B511B5, 0x08, 0x01B411B4, 0x08, 0x01AB11AB, 0x08, 0x01B011B0, 0x08, 0x01AC11AC, 0x08, 0x01AE11AE, 0x08, 0x01AF11AF, 0x08, 0x01AD11AD, 
	0x08, 0x01AA11AA, 0x08, 0x01A911A9, 0x08, 0x01A811A8
};

unsigned char quest_unlock_pkt[] = 
{
	0x14, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x03, 0x7B, 0x01, 0x65, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
};
	

/* This is the timeout for select.  It's needed or else user key presses won't be processed. */

struct timeval pso_timeout = { 0, 10000 };

// If the following is defined during compilation, SPSOF will only bind to the loopback IP address.
//#define USE_LOOPBACK

unsigned char proxy_addr[4] = { 127, 0, 0, 1 };


// Some of Myria's defines

#define htodl(x) (x)
#define dtohl(x) (x)

#define BUF_SIZE 65530

// Macro used during firewall checks

#define block_packet(reason,...) { \
	pkt[0x02] = 0x1D; \
	pkt[0x03] = 0x00; \
	client_broadcast(reason); \
	return 1; \
}

#define CP_COPY(cpsrc,cpsize) { \
	if (custom_data_in_buf + cpsize < BUF_SIZE) \
	{ \
		memcpy (&custom_buf[custom_data_in_buf], cpsrc, cpsize); \
		custom_data_in_buf += cpsize; \
		custom_buf[cpsize] = 0; \
	} \
	else \
		debug ("Lost some data while creating a custom packet."); }

#define PCLI_COPY(cpsrc,cpsize) { \
	if (client_data_in_buf + cpsize < BUF_SIZE) \
	{ \
		if (!patching) \
		{ \
			cipher_ptr = &proxy_to_client; \
			encryptcopy (&client_buf[client_data_in_buf], cpsrc, cpsize, &client_data_in_buf); \
		} \
		else \
		{ \
			p_cipher_ptr = &p_proxy_to_client; \
			pcencryptcopy (&client_buf[client_data_in_buf], cpsrc, cpsize); \
			client_data_in_buf += cpsize; \
		} \
	} \
	else  \
		debug ("Lost some data while sending to the client."); }

#define PSRV_COPY(cpsrc,cpsize) { \
	if (server_data_in_buf + cpsize < BUF_SIZE) \
	{ \
		if (!patching) \
		{	\
			cipher_ptr = &proxy_to_server; \
			encryptcopy (&server_buf[server_data_in_buf], cpsrc, cpsize, &server_data_in_buf); \
		} \
		else \
		{	\
			p_cipher_ptr = &p_proxy_to_server; \
			pcencryptcopy (&server_buf[server_data_in_buf], cpsrc, cpsize); \
			server_data_in_buf += cpsize; \
		} \
	} \
	else  \
		debug ("Lost some data while sending to the server."); }


#define KEYDOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)

#define CALLING cdecl

unsigned char create_pkt[] = { 
	0x30, 0x00, 0x6C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x5D, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00 
};

unsigned char give_pkt[] = { 
	0x18, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x59, 0x05, 0x00, 0x00, 
	0x00, 0x00, 0x0F, 0x00, 0x06, 0x00, 0x01, 0x00 
};

unsigned long currentitemid = 0x0081000D;
unsigned long itemok[30];
unsigned long itemcheck;
unsigned long validitem = 0;
int ddos, ddos_uselast = 0;
int quest_unlocking = 0;
unsigned short quest_flag = 0x0065;

/*****************************************************************************/
/* Global variables */
/*****************************************************************************/

int crypt_on = 0;
unsigned char exam_buf[BUF_SIZE+32];
unsigned char client_buf[BUF_SIZE+32];
unsigned char server_buf[BUF_SIZE+32];
unsigned char last_custom[BUF_SIZE+32];
unsigned last_custom_length;

/* Schthack Fix #4 */

unsigned char fixup_buf[32];

PSO_CRYPT server_to_client, client_to_server, proxy_to_client, proxy_to_server;

int client_data_in_buf = 0, server_data_in_buf = 0,
client_data_written = 0, server_data_written = 0;

/* function protos */
void debug(char *fmt, ...);
void debug_perror(char * msg);
void tcp_listen (int sockfd);
int tcp_accept (int sockfd, struct sockaddr *client_addr, int *addr_len );
int tcp_sock_connect(char* dest_addr, int port);
int tcp_sock_open(struct in_addr ip, int port);
int tcp_packet_write(int sockfd, struct tcp_packet *tcp_pkt);
int tcp_packet_read(int sockfd, struct tcp_packet *tcp_pkt);

PSO_CRYPT* cipher_ptr;

void encryptcopy (unsigned char* dest, const unsigned char* src, unsigned size, int* size_mod);
void decryptcopy (unsigned char* dest, const unsigned char* src, unsigned size);

int s_sockfd = 0, pc_sockfd = 0; // becomes global as of 2.67
fd_set ReadFDs, WriteFDs; // same here


/* Fun procedure. */

void client_broadcast(char *fmt, ...);
void top_broadcast(char *fmt, ...);

// DEBUG SHIT

#define BYTE unsigned char
#define DWORD unsigned


// Encryption data struct 
typedef struct {
    unsigned long type; // what kind of encryption is this? 
    unsigned long keys[1042]; // encryption stream 
    unsigned long pc_posn; // PSOPC crypt position 
    //unsigned long* gc_block_ptr; // PSOGC crypt position 
    //unsigned long* gc_block_end_ptr; // PSOGC key end pointer 
    //unsigned long gc_seed; // PSOGC seed used 
    //unsigned long bb_posn; // BB position (not used) 
    //unsigned long bb_seed[12]; // BB seed used 
} CRYPT_SETUP;

// Internal functions (don't call these) 
unsigned long CRYPT_PC_GetNextKey(CRYPT_SETUP*);
void CRYPT_PC_MixKeys(CRYPT_SETUP*);
void CRYPT_PC_CreateKeys(CRYPT_SETUP*,unsigned long);
void CRYPT_PC_CryptData(CRYPT_SETUP*,void*,unsigned long);

CRYPT_SETUP p_server_to_client, p_client_to_server, p_proxy_to_client, p_proxy_to_server;

CRYPT_SETUP* p_cipher_ptr;

void pcdecryptcopy ( void* dest, void* source, unsigned size );
void pcencryptcopy ( void* dest, void* source, unsigned size );

unsigned char hexToByte ( char* hs )
{
	unsigned b;

	if ( hs[0] < 58 ) b = (hs[0] - 48); else b = (hs[0] - 55);
	b *= 16;
	if ( hs[1] < 58 ) b += (hs[1] - 48); else b += (hs[1] - 55);
	return (unsigned char) b;
}

char dp[BUF_SIZE*4];

void display_packet ( unsigned char* buf, int len )
{
	int c, c2, c3, c4;

	c = c2 = c3 = c4 = 0;

	for (c=0;c<len;c++)
	{
		if (c3==16)
		{
			sprintf (&dp[c2++], " ");
			for (;c4<c;c4++)
				if (buf[c4] >= 0x20) 
					dp[c2++] = buf[c4];
				else
					dp[c2++] = 0x2E;
			c3 = 0;
			sprintf (&dp[c2++], "\n" );
		}

		if ((c == 0) || !(c % 16))
		{
			sprintf (&dp[c2], "(%04X) ", c);
			c2 += 7;
		}

		sprintf (&dp[c2], "%02X ", buf[c]);
		c2 += 3;
		c3++;
	}

	if ( len % 16 )
	{
		c3 = len;
		while (c3 % 16)
		{
			sprintf (&dp[c2], "   ");
			c2 += 3;
			c3++;
		}
	}

	sprintf (&dp[c2++], " ");
	for (;c4<c;c4++)
		if (buf[c4] >= 0x20) 
			dp[c2++] = buf[c4];
		else
			dp[c2++] = 0x2E;

	dp[c2] = 0;
	debug ("%s\n", &dp[0]);
}


// this function is called by a new thread 

unsigned char custom_buf[BUF_SIZE+32];
int custom_data_in_buf = 0;

int isHex ( char* hs )
{
	unsigned p, l = strlen (hs);

	for (p=0;p<l;p++)
	{
		if (!(((hs[p] > 47) && (hs[p] < 58)) || ((hs[p] > 64) && (hs[p] < 71))))
			return 0;
	}
	return 1;

}

unsigned char WindowRetrieve[30002];
unsigned char cpkt_data[30002+16];
unsigned char chpkt_data[250];
unsigned mThreadID, mThread_Terminated = 0;
HWND MultihWnd = NULL;

BOOL CALLBACK MultiwinDialogProc ( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	HGDIOBJ aBrush;

	switch (msg)
	{
	case WM_INITDIALOG:
		/* Set icons */
		SendMessage (hWnd, WM_SETICON, ICON_SMALL,
					(LPARAM) LoadIcon (GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON2)));
		SendMessage (hWnd, WM_SETICON, ICON_BIG,
					(LPARAM) LoadIcon (GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON2)));
		return FALSE;
		break;
	case WM_CLOSE:
		mThread_Terminated = 1;
		return FALSE;
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{			
		case IDC_SENDPACKET:
			{
				// get the edit control 
				HWND hEdit = GetDlgItem(hWnd,IDC_CHATINPUT); 
				unsigned char b;
				int c;
				unsigned short newPktLen;

				// Add packet sender here too...
				// check for custom packet data... if present, send it

				hEdit = GetDlgItem(hWnd,IDC_PACKETSENDER);
				if ( hEdit )
				{
					int newSize = GetWindowText( hEdit, WindowRetrieve, 30000 );
					WindowRetrieve[newSize] = 0;
					newPktLen = 0;

					for (c=0;c<newSize;c++)
					{
						if (WindowRetrieve[c] != 0x20) 
							cpkt_data[newPktLen++] = WindowRetrieve[c];
					}

					cpkt_data[newPktLen] = 0;

					if ( newPktLen )
					{
						_strupr(&cpkt_data[0]);
						if ( !isHex(&cpkt_data[0]) )
							debug ("Cannot send custom packet: Data input must be in HEX format!"); 
						else
						{
							if ( ( custom_data_in_buf + ( newPktLen / 2 ) < BUF_SIZE ) && ( s_sockfd ) )
							{
								for (c=0;c<newPktLen;c+=2)
								{
									if ( cpkt_data[c] < 58 ) b = (cpkt_data[c] - 48); else b = (cpkt_data[c] - 55);
									b *= 16;
									if ( cpkt_data[c+1] < 58 ) b += (cpkt_data[c+1] - 48); else b += (cpkt_data[c+1] - 55);
									custom_buf[custom_data_in_buf+(c/2)] = b;
								}
								newPktLen /= 2;
								memcpy (&custom_buf[custom_data_in_buf], &newPktLen, 2 );
								memcpy (&last_custom[0], &custom_buf[custom_data_in_buf], newPktLen);
								last_custom[newPktLen] = 0;
								last_custom_length = newPktLen;
								custom_data_in_buf += newPktLen;
								custom_buf[custom_data_in_buf] = 0x00;
							}
							else
								debug ("Custom: Lost some data while creating a custom packet.");
						}
					}
				}
				else 
					debug ("Failed to get custom packet control.");
			}
			return FALSE;
			break;
		default:
			return FALSE;
			break;
		}
		break;
	case WM_CTLCOLORSTATIC:
		aBrush = CreateSolidBrush (RGB(255,255,255));
		return (LRESULT)aBrush;
		break;
	case WM_CTLCOLORDLG:
		aBrush = CreateSolidBrush (RGB(166,202,240));
		return (LRESULT)aBrush;
	default:
		return FALSE;
		break;
	}
}

HBITMAP gohan_BMP, old_hbitmap;
HDC hdc, image_dc;


unsigned __stdcall MultiwinThreadProc (void* dummy)
{ 
	MSG msg;
	int rarepic;
	unsigned short picresource;

	// create the dialog window 

	MultihWnd = CreateDialog(NULL, MAKEINTRESOURCE (IDD_MULTIWINDOW), NULL, &MultiwinDialogProc);

	if ( MultihWnd != NULL )
	{ 
		// show dialog 
		ShowWindow(MultihWnd,SW_SHOW); 
	} 
	else 
	{ 
		debug("Failed to create dialog.  GetLastError: %u", GetLastError()); 
		_endthreadex (0);
		return 0; 
	}

	PeekMessage (&msg, MultihWnd, 0, 0, PM_NOREMOVE);
	PostThreadMessage (mThreadID, WM_NULL, 0, 0);
	WaitMessage();

	srand ( (unsigned) time(NULL) );
	rarepic = rand () % 100;
	if (((rarepic > 35) && (rarepic < 40)) ||
		((rarepic > 75) && (rarepic < 80)))
	{
		rarepic = rand () % 12;
		switch (rarepic)
		{
		case 0x00:
			picresource = IDB_RARE01;
			break;
		case 0x01:
			picresource = IDB_RARE02;
			break;
		case 0x02:
			picresource = IDB_RARE03;
			break;
		case 0x03:
			picresource = IDB_RARE04;
			break;
		case 0x04:
			picresource = IDB_RARE05;
			break;
		case 0x05:
			picresource = IDB_RARE06;
			break;
		case 0x06:
			picresource = IDB_RARE07;
			break;
		case 0x07:
			picresource = IDB_RARE08;
			break;
		case 0x08:
			picresource = IDB_RARE09;
			break;
		case 0x09:
			picresource = IDB_RARE10;
			break;
		case 0x0A:
			picresource = IDB_RARE11;
			break;
		case 0x0B:
		default:
			picresource = IDB_RARE12;
			break;
		}
	}
	else
		picresource = IDB_GOHAN256;
	gohan_BMP = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE (picresource));
	hdc = GetDC(MultihWnd);
	image_dc = CreateCompatibleDC(hdc);
	old_hbitmap = (HBITMAP) SelectObject(image_dc, gohan_BMP);
	BitBlt(hdc, 10 , 12, 320, 480, image_dc, 0, 0, SRCCOPY);

	do
	{
		if ( PeekMessage ( &msg, MultihWnd, 0, 0, PM_REMOVE ) )
		{
			if (msg.message == WM_QUIT)
			{
				mThread_Terminated = 1;
				break;
			}
			// process message 
			TranslateMessage(&msg); 
			DispatchMessage(&msg); 
			switch (msg.message)
			{
			case WM_PAINT:
				gohan_BMP = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE (picresource));
				hdc = GetDC(MultihWnd);
				image_dc = CreateCompatibleDC(hdc);
				old_hbitmap = (HBITMAP) SelectObject(image_dc, gohan_BMP);
				BitBlt(hdc, 10 , 12, 320, 480, image_dc, 0, 0, SRCCOPY);
				break;
			case WM_KEYUP:
				{
					// When user hits return, check to see if there's any data input in the chat
					// area.  If there is, send chat stuff instead of a packet, otherwise check
					// to see if the packet area has data in it.
					int VirtKey = (int)msg.wParam;
					if ( VirtKey == 0x000D )
					{ 
						// get the edit control 
						HWND hEdit = GetDlgItem(MultihWnd,IDC_CHATINPUT); 
						if ( hEdit )
						{
							int chatSize = GetWindowText ( hEdit, WindowRetrieve, 200 );
							if ( chatSize )
							{
								// get the input text the user entered 
								// and print it to the console window 
								unsigned short chat_size;
								int p;
								unsigned s;

								if ( chatSize > 199 ) chatSize = 199;
								WindowRetrieve[chatSize] = 0;

								for (p=0;p<chatSize-1;p++)
								{
									if ((WindowRetrieve[p] == 0x24) && (WindowRetrieve[p+1] != 0x24))
										WindowRetrieve[p] = 0x09;
								}

								memcpy ( &chpkt_data[0], &chat_pkt[0], sizeof (chat_pkt));
								chat_size = sizeof (chat_pkt) - 4;
								for ( s=0;s<strlen(&WindowRetrieve[0]);s++)
								{
									chpkt_data[chat_size++] = WindowRetrieve[s];
									chpkt_data[chat_size++] = 0x00;
									if (WindowRetrieve[s] == 0x09)
									{
										chpkt_data[chat_size++] = 0x20;
										chpkt_data[chat_size++] = 0x00;
									}
								}

								chpkt_data[chat_size++] = 0x00;
								chpkt_data[chat_size++] = 0x00;

								while (chat_size % 8)
									chpkt_data[chat_size++] = 0x00;

								memcpy (&chpkt_data[0], &chat_size, 2);

								if (s_sockfd) 
								{
									CP_COPY (&chpkt_data[0], chat_size); 
								}
								else
									debug ("Cannot send packets while not connected!");

								SetDlgItemText ( MultihWnd, IDC_CHATINPUT, 0x00 );
							}
							else
							{
								// check for custom packet data... if present, send it
								unsigned char b;
								int c;
								unsigned short newPktLen;

								hEdit = GetDlgItem(MultihWnd,IDC_PACKETSENDER);
								if ( hEdit )
								{
									int newSize = GetWindowText( hEdit, WindowRetrieve, 30000 );
									WindowRetrieve[newSize] = 0;
									newPktLen = 0;

									for (c=0;c<newSize;c++)
									{
										if (WindowRetrieve[c] != 0x20) 
											cpkt_data[newPktLen++] = WindowRetrieve[c];
									}

									cpkt_data[newPktLen] = 0;

									if ( newPktLen )
									{
										_strupr(&cpkt_data[0]);
										if ( !isHex(&cpkt_data[0]) )
											debug ("Cannot send custom packet: Data input must be in HEX format!"); 
										else
										{
											if ( ( custom_data_in_buf + ( newPktLen / 2 ) < BUF_SIZE ) && ( s_sockfd ) )
											{
												for (c=0;c<newPktLen;c+=2)
												{
													if ( cpkt_data[c] < 58 ) b = (cpkt_data[c] - 48); else b = (cpkt_data[c] - 55);
													b *= 16;
													if ( cpkt_data[c+1] < 58 ) b += (cpkt_data[c+1] - 48); else b += (cpkt_data[c+1] - 55);
													custom_buf[custom_data_in_buf+(c/2)] = b;
												}
												newPktLen /= 2;
												memcpy (&custom_buf[custom_data_in_buf], &newPktLen, 2 );
												memcpy (&last_custom[0], &custom_buf[custom_data_in_buf], newPktLen);
												last_custom[newPktLen] = 0;
												last_custom_length = newPktLen;
												custom_data_in_buf += newPktLen;
												custom_buf[custom_data_in_buf] = 0x00;
											}
											else
												debug ("Custom: Lost some data while creating a custom packet.");
										}
									}
								}
								else 
									debug ("Failed to get custom packet control.");
							}
						} 
						else 
						{ 
							debug("Failed to get chat control"); 
						} 
					} 
				}
				break;
			default:
				break;
			}
		}
		else
		{
			Sleep (100);
		}
	} while (!mThread_Terminated);
	DestroyWindow (MultihWnd);
	MultihWnd = NULL;
	_endthreadex (0);
	return 0;
}

unsigned char lobby_clone_full[12][1244];
unsigned char my_data_full[1244];
unsigned char stored_full[1244];
int clone_now = -1;
int partial_clone = 0;
unsigned lobby_gcn[12];
//unsigned char lobby_ip[12][4];
char lobby_name[12][15];
unsigned char lobby_level[12];
int lobby_user_active[12];
unsigned char lobby_packet[(1312 * 12) + 32];
int lobby_pktsize, lobby_erase, tlc, lobby_update, team_mode, 
reset_yes, team_create, schthack_mode, allow_quest = 0;
int steal_quest = 0;
unsigned short lobby_class[12];
unsigned my_gcn, my_clientid;
unsigned char my_name[12];
unsigned short my_class;
unsigned char my_level;
int steal_symbolnow = 0;
struct hostent *schthack_host;  
char chatlines[32][255];
char chat_data[32*2*256]; // In case, some day, I support unicode... *2'd it.
char lobby_data[4096];
int chat_start = 1;


/* Whether we're automatically owning certain GC#s or not... */

int auto_own = 1;


/* Pointer to the autoown.txt file */

FILE *OwnFile;

/* Data being read from own file */

char OwnData[256];


void UpdateChatWindow()
{
	/* we could probably update a chat log here.. */
	char p;
	int total = 0, chat_now;

	chat_data[0] = 0;
	chat_now = chat_start;
	for (p=0;p<32;p++)
	{
		sprintf (&chat_data[total], "%s\n", (char*) &chatlines[chat_now][0] );
		chat_now++;
		if (chat_now > 31) chat_now = 0;
		total = strlen (&chat_data[0]);
	}
	if ( MultihWnd )
		SetDlgItemText ( MultihWnd, IDC_CHATBOX, &chat_data[0] );
	//UpdateWindow ( MultihWnd );
	chat_start++;
	if (chat_start > 31) chat_start = 0;
}

void UpdateLobbyWindow()
{
	char p;
	int total = 0;

	lobby_data[0] = 0;
	sprintf (&lobby_data[0], "[Lobby users:]\n\n");
	total = strlen (&lobby_data[0]);
	for (p=0;p<0x0C;p++)
	{
		if (lobby_user_active[p])
		{
			if (p < 0x0A) lobby_data[total++] = 48;
			sprintf (&lobby_data[total], "%u : %s", p, &lobby_name[p]);
			total = strlen (&lobby_data[0]);
			if (!team_mode)
			{
				sprintf (&lobby_data[total], " (Lv");
				total = strlen (&lobby_data[0]);
				if (lobby_level[p] < 0x0A) lobby_data[total++] = 48;
				if (lobby_level[p] < 0x64) lobby_data[total++] = 48;
				sprintf (&lobby_data[total], "%u)\n", lobby_level[p]);
			}
			else
				sprintf (&lobby_data[total], "\n");
			total = strlen (&lobby_data[0]);
			sprintf (&lobby_data[total], "(%u)\n", lobby_gcn[p]);
			total = strlen (&lobby_data[0]);
		}
	}
	sprintf (&lobby_data[total], "\n");
	if ( MultihWnd )
		SetDlgItemText ( MultihWnd, IDC_TEAMLIST, &lobby_data[0] );
	//UpdateWindow ( MultihWnd );
	lobby_update = 0;
}

unsigned zero_dword = 0;
unsigned set_dword = 0xFFFFFFFF;

unsigned auto_own_list[256];
int own_count = 0;

unsigned char null_outfit[10];

void process_lobby_data(int erase)
{
	int o;
	int j,j2, j3, j4;
	unsigned char cid;

	if (erase)
	{
		for (j=0;j<0x0C;j++)
			lobby_user_active[j] = 0;
	}

	if ( team_mode )
	{
		if ( team_mode == 1 )
		{
			/* Joining a team. */
			j4 = 0x88;
			for (j=0;j<lobby_packet[0x04];j++)
			{
				if  ( ( lobby_packet[j4+0x0A] )  ||
					  ( lobby_packet[j4+0x0B] )  ||
					  ( lobby_packet[j4+0x0C] ) )
					cid = lobby_packet[j4+0x18]; else
					cid = lobby_packet[j4+0x14];
				if (cid < 0x04)
				{
					memcpy (&lobby_gcn[cid], &lobby_packet[j4 + 0x04], 4);
					j3 = 0;
					for (j2=j4+0x20;j2<j4+0x38;j2+=2)
					{
						if ((lobby_packet[j2] == 0x00) && (lobby_packet[j2+1] == 0x00))
							break;
						if (((lobby_packet[j2] == 0x09) && (lobby_packet[j2+1] == 0x00)) ||
						   ((lobby_packet[j2] == 0x0A) && (lobby_packet[j2+1] == 0x00)))
							j2 += 2;
						else
							lobby_name[cid][j3++] = lobby_packet[j2];
					}
					lobby_name[cid][j3] = 0x00;
					lobby_level[cid] = 0;
					lobby_class[cid] = 0;
					lobby_user_active[cid] = 1;
					if (lobby_gcn[cid] == my_gcn)
					{
						my_clientid = cid;
					}
				}
				j4 += 0x44;
			}
		}
		else
		{
			/* Another user joining the game. */
			j4 = 0x14;
			cid = lobby_packet[j4+0x1C];
			if (cid < 0x04)
			{
				memcpy (&lobby_gcn[cid], &lobby_packet[j4 + 0x04], 4);
				j3 = 0;
				for (j2=j4+0x20;j2<j4+0x38;j2+=2)
				{
					if ((lobby_packet[j2] == 0x00) && (lobby_packet[j2+1] == 0x00))
						break;
					if (((lobby_packet[j2] == 0x09) && (lobby_packet[j2+1] == 0x00)) ||
					   ((lobby_packet[j2] == 0x0A) && (lobby_packet[j2+1] == 0x00)))
						j2 += 2;
					else
						lobby_name[cid][j3++] = lobby_packet[j2];
				}
				lobby_name[cid][j3] = 0x00;
				lobby_level[cid] = 0;
				lobby_class[cid] = 0;
				lobby_user_active[cid] = 1;
				if (lobby_gcn[cid] == my_gcn)
				{
					my_clientid = cid;
				}
			}
		}
	}
	else
	{
		j = 0x16;
		/* skip the first 22 bytes and copy user information.  data is in 1312 byte chunks per user. */
		for (j4=0;j4<lobby_packet[0x04];j4++)
		{
			memcpy (&cid, &lobby_packet[j+0x1A], 1);
			if (cid < 0x0C)
			{
				memcpy ( &lobby_clone_full[cid][0], &lobby_packet[j+0x42], 1244 );
				memcpy ( &lobby_gcn[cid], &lobby_packet[j+0x02], 4 );
				j3 = 0;
				for (j2=(j+0x22);j2<j+0x3C;j2+=2)
				{
					if ((lobby_packet[j2] == 0x00) && (lobby_packet[j2+1] == 0x00))
						break;
					if (lobby_packet[j2] != 0x09)
						lobby_name[cid][j3++] = lobby_packet[j2];
				}
				lobby_level[cid] = lobby_packet[j+0x3A6] + 1;
				memcpy ( &lobby_class[cid], &lobby_packet[j+0x3E2], 2 );
				/* Force outfit and hair to zero for skin users. */
				if ( lobby_packet[j+0x3E4] > 0 )
				{
					memcpy (&exam_buf[j+0x3EA], &null_outfit[0], 10);
				}
				lobby_name[cid][j3] = 0x00;
				lobby_user_active[cid] = 1;
				if (lobby_gcn[cid] == my_gcn)
				{
					my_clientid = cid;
					my_class = lobby_class[cid];
					my_level = lobby_level[cid];
					memcpy ( &gcfsod_pkt[150], &my_class, 2 );
					if (clone_now == -1)
					{
						memcpy ( &my_data_full[0], &lobby_packet[j+0x42], 1244 );
					}
				}

				if (auto_own == 1)
				{
					for (o=0;o<own_count;o++)
					{
						if (lobby_gcn[cid] == auto_own_list[o])
						{
							gcfsod_pkt[3] = cid;
							CP_COPY (&gcfsod_pkt[0], sizeof(gcfsod_pkt));
							break;
						}
					}					
				}
			}
		j += 0x520;
		}
	}
		lobby_update = 1;
}

int steal_schats = 1, forced_item = 0, quick_level = 0,
one_hit_kill = 0, skin_id = 0, firewall_on = 1, kill_steal = 0,
invinci_mob = 0;
// int noob_time = 0;
int god_mode = 0, god_tick = 0, levelup_tick = 0;
int name_override = 0;
unsigned char bb_name[4];
unsigned clientarea[12];
float clientx[12], clienty[12], clientz[12], clienth[12];
unsigned char cid;
unsigned short size_check, size_check2, size_check3, size_check_index;

int check_packet (unsigned char* pkt)
{	
	if (!firewall_on) return 0;	

	switch (pkt[0x02])
	{
	case 0x60:
	case 0x62:
	case 0x6C:
	case 0x6D:

		/* Check size of packet first. */

		size_check_index = pkt[0x08];
		size_check_index *= 2;

		memcpy ( &size_check, &pkt[0x00], 2 );

		/* Should just be four plus the Gamecube length. (Too lazy to update the table myself...) */

		size_check2 = size_check_table[size_check_index+1] + 4;

		/* Guild card */

		if (pkt[0x08] == 0x06) size_check2 = 0x114;

		if ( ( size_check != size_check2 ) && ( size_check2 > 4 )  && ( pkt[0x08] != 0x05 ) )
		{
			if ((!team_mode) || (pkt[0x02] != 0x60))
				block_packet ("Packet size incorrect.")
		}

		/* Steal symbol chats. */

		if ((pkt[0x00] == 0x4C) && (pkt[0x08] == 0x07) && (pkt[0x09] == 0x11) && (steal_schats))
			memcpy (&steal_schat[0x00], pkt, 0x4C);

		/* "Door" FSOD */

		if ((pkt[0x08] == 0x76) && (pkt[0x0D] != 0x00)) 
			block_packet ("Door FSOD blocked.")

		/* TP FSOD */

		if ((pkt[0x08] == 0x68) && (pkt[0x09] == 0x07) && (pkt[0x0A] == 0x1B))
		{
			if ((pkt[0x0C] > 0x03) || (pkt[0x0E] > 0x11))
			{
				cid = pkt[0x0C];
				if (cid < 0x04)
					block_packet ("Bad telepipe set by %s blocked.", (char*) &lobby_name[cid][0])
				else
					block_packet ("Bad telepipe set blocked.")
			}
		}

		// Fake QFSOD

		if (pkt[0x08] == 0x73) 
			block_packet ("Fake QFSOD blocked.")

		// Crash packets

		if (pkt[0x08] == 0x18)
		{
			/* Only allow in Vol Opt's area. */				
			if ( ((pkt[0x02] != 0x60) || (!team_mode)) || (clientarea[my_clientid] != 0x0D)) 
				block_packet ("Crash packet blocked.")
		}

		if ((pkt[0x08] == 0x63) || (pkt[0x08] == 0x6A) || (pkt[0x08] == 0x6B) || (pkt[0x08] == 0x6C) || 
			(pkt[0x08] == 0x6D) || (pkt[0x08] == 0x6E) || (pkt[0x08] == 0x70))
		{
			/* 0x6A is the boss teleporter. */

			if (((pkt[0x02] == 0x60) && (pkt[0x08] == 0x6A)) && (pkt[0x0A] < 0x04))
				block_packet ("Crash packet blocked. condition1")
			else
			{
				/* Forcing a size check here... */
				size_check2 = 0;
				memcpy ( &size_check2, &pkt[0x09], 1 );
				size_check2 *= 4;
				size_check2 += 8;
				if ((pkt[0x08] == 0x6A) && ((pkt[0x02] != 0x60) || (size_check != size_check2)))
					block_packet ("Crash packet blocked. condition2")
			}
		}

		// Relocate packet.

		if ((pkt[0x08] == 0x17) && ((pkt[0x02] != 0x60) || (!team_mode)))
			block_packet ("Relocate packet blocked.")

		// Warp time.  (Yes, I'm aware 0x62 target variation warps go through the Firewall.)

		if (pkt[0x08] == 0x94)
		{
			if ( team_mode == 0 ) 
				block_packet ("Warp packet blocked.")
			else
				if ( ( pkt[0x09] < 0x02 ) || 
					( ( pkt[0x0C] > 0x11) || ( pkt[0x02] == 0x6D ) ) ) 
					block_packet ("Bad warp packet blocked.")
		}

		// PK packet.  (0x6C not blocked..)

		if  ( pkt[0x08] == 0x9A )
		{
			if ( ( ((pkt[0x02] == 0x60) && (pkt[0x0A] == my_clientid)) ||
				(((pkt[0x02] == 0x62) || (pkt[0x02] == 0x6D)) && ((pkt[0x03] == my_clientid) || (pkt[0x04] == my_clientid))) ) && ( pkt [0x0E] < 0x03 ) )
				block_packet ("PK attempt blocked.")
		}

		// Lobby reset. 0x6D is target version.  Allow only one reset when joining a team...

		if (pkt[0x08] == 0x71)
		{
			if ((pkt[0x02] != 0x62) || (!team_mode))
				block_packet ("Character reload blocked.")
			if (!reset_yes) block_packet ("Double character reload blocked.")
			else
				reset_yes = 0;
		}

		// Spawn NPC packet.

		if (((pkt[0x02] == 0x6D) || (pkt[0x02] == 0x62)) && (pkt[0x08] == 0x69))
			block_packet ("NPC spawn packet blocked.")

		// ELENOR packet.

		if ((pkt[0x08] == 0x58) && ((pkt[0x00] != 0x10) || (pkt[0x01] != 0x00)))
			block_packet ("NPC spawn packet blocked.")

		/* Unknown target packet. */

		if ( ( pkt[0x02] == 0x6D ) || ( pkt[0x02] == 0x62 ) )
		{
			int target_size;
			unsigned short size62check, size62;

			target_size = 0;

			if (pkt[0x02] == 0x62)
			{
				size62check = 0;
				memcpy (&size62, &pkt[0x00], 2);
				memcpy (&size62check, &pkt[0x09], 1);
				size62check *= 4;
				size62check += 8;
				if (size62check != size62)
					target_size = 1;
			}
			else
			{
				size62check = 0;
				memcpy (&size62, &pkt[0x00], 2);
				memcpy (&size62check, &pkt[0x0C], 2);
				size62check += 8;
				if (size62check != size62)
					target_size = 1;
			}

			if (target_size)
				block_packet ("Target packet blocked due to size check failure.")
			else
			{
				if (!team_mode)
				{
					switch (pkt[0x08])
					{
					case 0x07:
						if (!steal_symbolnow)
							block_packet ("Target packet blocked.")
						else
						steal_symbolnow = 0;
						debug ("Command: 0x07");
						break;
					case 0xC1:
					case 0xC2:
					case 0xCD:
					case 0xCE:
						break;
					case 0xAE:
						break;
					default:
						block_packet ("Target packet blocked.")
							debug ("Command: %02X", pkt[8]);
						break;
					}
				}
			}
		}

		/* Malformed packet check.

		size_check2 = size_check_table[size_check_index+1] + 4;

		size_check3 = pkt[0x09];
		size_check3 *= 4;
		size_check3 += 8;

		// All of the secondary sizes of these packets are not uniform
		//////////////////////////////////////////////////////////////
		// 6F = ??
		// 72 = After join game
		// 5F = Box drop
		// B6 = Visiting Shop
		// BE = Item Withdrawn
		// BC = Visiting bank
		// BF = Gained exp

		if ((pkt[0x02] == 0x6D) && ((pkt[0x08] > 0x6A) && (pkt[0x08] < 0x71))) size_check3 = size_check;

		if (( size_check3 != size_check ) || ((size_check2 == 4) && (!team_mode)) ) 
		{
			debug ("Command: %02X has an invalid size.", pkt[0x08]);
			if (size_check2 != 4)
			{
				debug ("EXPECT=%u, RECV=%u, SUB=%u.", size_check2, size_check, size_check3);
			}
			else
			{
				debug ("EXPECT=%u, RECV=%u", size_check, size_check3);
			}
			// block_packet ("Packet size incorrect or unknown packet.");
		}


		*/

		/* Unknown server packet. */

		if ((pkt[0x02] == 0x6C) && (!team_mode))
			block_packet ("Forged \"global\" packet blocked.  Command: %02X", pkt[8])

		break;
	case 0x10:
		if ((pkt[0x09] == 0x01) && (pkt[0x0A] == 0x0E))
		{
			if (!team_mode) block_packet ("Quest data detected while in lobby.") else
				if (!allow_quest) block_packet ("Double quest attempt blocked.")
		}
		break;
	case 0x44:
	case 0xA6:
	case 0xA7:
		if (!team_mode) block_packet ("Quest data detected while in lobby.") else
			if (!allow_quest) block_packet ("Double quest attempt blocked.")
		break;
	case 0xAC:
		if (allow_quest) allow_quest = 0;
		break;
	case 0xC9:
		block_packet ("Disconnect attempt blocked.")
		break;
		/*
		#ifdef OPT_PACKETS
		case 0xEA:
		if ((pkt[0x20] == 0x50) && (pkt[0x22] == 0x53) && (pkt[0x24] == 0x4F) && (pkt[0x26] == 0x50) && (pkt[0x28] == 0x43))
		{
		if (noob_time)
		{
		top_broadcast ("$C4PSO:PC User has been detected. Removing from lobby....");
		opt_qfsod_pkt[0x04] = pkt[0x40];
		CP_COPY (&opt_qfsod_pkt[0], sizeof (opt_qfsod_pkt));
		}
		else
		top_broadcast ("$C4PSO:PC User has been detected. Doing nothing....");

		}
		else
		if ((pkt[0x20] == 0x50) && (pkt[0x22] == 0x53) && (pkt[0x24] == 0x4F) && (pkt[0x26] == 0x44) && (pkt[0x28] == 0x43))
		{
		if (noob_time)
		{
		top_broadcast ("$C1PSO:DC User has been detected. Removing from lobby....");
		opt_qfsod_pkt[0x04] = pkt[0x40];
		CP_COPY (&opt_qfsod_pkt[0], sizeof (opt_qfsod_pkt));
		}
		else
		top_broadcast ("$C1PSO:DC User has been detected. Doing nothing....");

		}
		break;
		#endif
		*/
	default:
		break;
	}
	return 0;
}

void duplicate_for_all ( char* buf, char* pkt, int pktsize, int target )
{
	char p;

	for (p=0;p<0x0C;p++)
	{
		memcpy (buf, pkt, pktsize);
		buf[target] = p;
		buf += pktsize;
	}
}

unsigned getgcn ( char* cname )
{
	int p,p2,p3,p4;

	char s1[15], s2[15];

	p4 = strlen ( cname );
	if (p4 > 14) p4 = 14;

	memcpy ( &s1[0], cname, p4 + 1 );
	s1[14] = 0;

	_strupr ( s1 );

	for (p=0;p<0x0E;p++)
	{
		p3 = 1;

		memcpy ( &s2[0], &lobby_name[p][0], 15 );
		_strupr ( s2 );

		for (p2=0;p2<p4;p2++)
		{
			if ( s1[p2] != s2[p2] ) p3 = 0;
		}

		if ( p3 ) return lobby_gcn[p];
	}

	return 0;

}

unsigned char getcid ( char* cname )
{
	int p2,p3,p4;
	unsigned char p;

	char s1[15], s2[15];

	p4 = strlen ( cname );
	if (p4 > 14) p4 = 14;

	memcpy ( &s1[0], cname, p4 + 1 );
	s1[14] = 0;

	_strupr ( s1 );

	for (p=0;p<0x0E;p++)
	{
		p3 = 1;

		memcpy ( &s2[0], &lobby_name[p][0], 15 );
		_strupr ( s2 );

		for (p2=0;p2<p4;p2++)
		{
			if ( s1[p2] != s2[p2] ) p3 = 0;
		}

		if ( p3 ) return p;
	}

	return 255;
}

/* Ports 12000 - 12019 have to be reserved for proxy. */

#ifdef NO_WINDOWS
int START_PORT = 10020;
#else
int START_PORT = 10000;
#endif

#define MAX_PORTS 20

unsigned short listen_ports[MAX_PORTS];
int listen_active[MAX_PORTS];
unsigned short server_ports[MAX_PORTS];
struct in_addr server_addrs[MAX_PORTS];
unsigned listen_creation[MAX_PORTS];  /* Used for overwriting when all ports becomes full. */ 
unsigned creation_num = 0;
int listen_sockfds[MAX_PORTS]; 
unsigned char last_used_addr[4];
unsigned short last_used_port, my_used_port;
int first_select = 1;
#ifdef SERVER_WORKAROUND
int connect_step = 1;
#endif

void select_portidx ( unsigned short *listen_port, int *listen_idx )
{
	int p, p2, piu;
	unsigned cn;


	if ( first_select )
	{
		*listen_port = BB_PORT;
		*listen_idx = 0;
		first_select = 0;
	}
	else
	{
		*listen_port = 0;
		*listen_idx = -1;

		/* Search for a free port. */

		for (p=START_PORT;p<START_PORT+MAX_PORTS;p++)
		{
			piu = 0;
			for (p2=0;p2<MAX_PORTS;p2++)
			{
				if ((listen_ports[p2] == p) && (listen_active[p2]))
				{
					piu = 1;
					break;
				}
			}
			if (!piu) 
			{
				*listen_port = (unsigned short) p;
				break;
			}
		}

		/* No free ports?  Use the oldest created active listening port. */

		if (! *listen_port)
		{
			cn = 0xFFFFFFFF;
			for (p=0;p<MAX_PORTS;p++)
			{
				if ((listen_creation[p] < cn) && (listen_active[p] == 1))
				{
					cn = listen_creation[p];
					*listen_idx = p;
					*listen_port = listen_ports[p];
				}
			}

			/* We definitely had to have gotten a port. */

			closesocket (listen_sockfds[*listen_idx]);
			listen_active[*listen_idx] = 0;

		}

		/* Need an index for the port we got at the very beginning. */

		if (*listen_idx == -1)
		{
			for (p=0;p<MAX_PORTS;p++)
			{
				if (listen_active[p] == 0)
				{
					*listen_idx = p;
					break;
				}
			}

			/* This should never occur, but whatever. */

			if ( *listen_idx == -1 )
			{
				cn = 0xFFFFFFFF;
				for (p=0;p<MAX_PORTS;p++)
				{
					if ((listen_creation[p] < cn) && (listen_active[p] == 1))
					{
						cn = listen_creation [p];
						*listen_idx = p;
					}
				}

				/* We definitely had to have gotten a port. */

				closesocket (listen_sockfds[*listen_idx]);
				listen_active[*listen_idx] = 0;

			}
		}
	}
}


char log_file_name[256];
char chat_file_name[256];
char trail_work[256];
unsigned char top_broadcast_pkt[] = { 0x68, 0x01, 0xEE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char broadcast_pkt[] = { 0x40, 0x00, 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x45, 0x00 };

#define MAX_BROADCAST_LEN 2048

char work_pkt[(MAX_BROADCAST_LEN*2)+2];

int patching = 1;

void top_broadcast(char *fmt, ...)
{
	unsigned short work_len;

	va_list args;
	char text[ MAX_BROADCAST_LEN ];

	FILE *fp;

	unsigned short j;

	va_start (args, fmt);
	strcpy (text + vsprintf( text,fmt,args), "\n"); 
	va_end (args);

	fp = fopen ( &log_file_name[0], "a");
	if (!fp)
	{
		printf ("Unable to log to %s\n", &log_file_name[0]);
	}
	fprintf (fp, "%s", text);
	fclose (fp);

	fprintf( stderr, "%s", text);

	memcpy (&work_pkt[0], &top_broadcast_pkt[0], sizeof (top_broadcast_pkt) );
	work_len = sizeof ( top_broadcast_pkt );

	for (j=0;j<strlen(text);j++)
	{
		if (text[j] != 0x0A)
		{
			if (text[j] == 0x24) 
				work_pkt[work_len++] = 0x09; else
				work_pkt[work_len++] = text[j];
			work_pkt[work_len++] = 0x00;
		}
	}

	work_pkt[work_len++] = 0x00;
	work_pkt[work_len++] = 0x00;
	work_pkt[work_len++] = 0x00;
	work_pkt[work_len++] = 0x00;

	memcpy ( &work_pkt[0], &work_len, 2 );

	PCLI_COPY (&work_pkt[0], work_len);

	debug ("Top_Broadcast to Client (Decrypted:%u):", work_len );

	display_packet (&work_pkt[0], work_len);

}

void client_broadcast(char *fmt, ...)
{
	unsigned short work_len;

	va_list args;
	char text[ MAX_BROADCAST_LEN ];

	FILE *fp;


	unsigned short j;

	va_start (args, fmt);
	strcpy (text + vsprintf( text,fmt,args), "\n"); 
	va_end (args);

	fp = fopen ( &log_file_name[0], "a");
	if (!fp)
	{
		printf ("Unable to log to %s\n", &log_file_name[0]);
	}
	fprintf (fp, "%s", text);
	fclose (fp);

	fprintf( stderr, "%s", text);


	memcpy (&work_pkt[0], &broadcast_pkt[0], sizeof (broadcast_pkt) );
	work_len = sizeof ( broadcast_pkt );

	for (j=0;j<strlen(text);j++)
	{
		if (text[j] != 0x0A)
		{
			if (text[j] == 0x24)
				work_pkt[work_len++] = 0x09; else
				work_pkt[work_len++] = text[j];

			work_pkt[work_len++] = 0x00;
		}
	}

	work_pkt[work_len++] = 0x00;
	work_pkt[work_len++] = 0x00;
	work_pkt[work_len++] = 0x00;
	work_pkt[work_len++] = 0x00;

	memcpy ( &work_pkt[0], &work_len, 2 );

	PCLI_COPY (&work_pkt[0], work_len);

	debug ("Broadcast to Client (Decrypted:%u):", work_len );

	display_packet (&work_pkt[0], work_len);
}


int exam_data_in_buf = 0;
SYSTEMTIME now_t;
unsigned char c_exam_buf[BUF_SIZE+32];
int c_exam_data_in_buf = 0;
unsigned char rewrite_buf[BUF_SIZE+32];
int program_exit = 0;
char myCmd[256];
char myArgs[4][256];
int cmdLen, argLen, cmdArgs;
unsigned char tid, wa, nid, cid, lid;
char convert_buffer[255];
unsigned rewrite_len;
int check_bypass = 0;
int wrote_item = 0;
unsigned char item_param_1[4], item_param_2[4], item_param_3[4], item_param_4[4];
int alternate_server = 0;
int trial_bypass = 0;
unsigned short port_to_save;


unsigned CRC32 ( unsigned char* p, unsigned byteCount)
{
	unsigned i;
	unsigned CRCValue = 0xFFFFFFFF;

	for (i=0;i<byteCount;i++)
	{
		CRCValue = ( CRCValue >> 8 ) ^ CRCTable [ *p ^ ( CRCValue & 0x000000FF ) ];
		p++;
	}
	return CRCValue;
}


unsigned char quest_learn_pkt [] = 
{
	0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x0E, 0x00, 0x4B, 0x02, 0x00, 0x00
};

char backup_FileName[256];
char learn_FileName[256];
char marathon_FileName[256];
char QM_Files[256][256];
int quest_learn = 0;
unsigned quest_speed = 100;
unsigned quest_identifier = 0xBEEFDEAD;
unsigned char quest_gametype = 0x00;
unsigned char quest_episode = 0x01;
unsigned quest_delay = 0;
int quest_play = 0;
int quest_marathon = 0;
int quest_step = 0;
unsigned marathon_index = 0;
unsigned marathon_quests = 0;
unsigned quest_percent, now_percent = 0;
char name_temp[15];
unsigned char mob_info[4];
unsigned mobs_logged = 0;
unsigned damage_modifier = 0;
FILE* QLFile;
unsigned QLRead, QLSize;
unsigned char QLBuf[BUF_SIZE];
unsigned QLWarped = 0, QLInvSpace = 0;
unsigned positions_Count;
char positions_Name[256][256];
float positions_Data[256][4];
int login_message = 0;

unsigned short client_commands1[8192];
unsigned short client_commands2[8192];
unsigned short cc_index = 0;

unsigned short server_commands1[8192];
unsigned short server_commands2[8192];
unsigned short sc_index = 0;


void client_examine_buffer()
{
	int ch2,ch;
	unsigned long gc_num;
	unsigned short bb_temp, bb_temp2, check_port;
	int chat_idx = 0, line_now;
	FILE* fp;
	unsigned gcn_temp;
 	int item_flag;
	unsigned cc;
	int found_cc = 0;

	if (team_mode)
	{
		memcpy (&bb_temp, &c_exam_buf[0x02],  2);
		memcpy (&gcn_temp, &c_exam_buf[0x04], 4);
		memcpy (&bb_temp2, &c_exam_buf[0x08], 2);

		for (cc=0;cc<cc_index;cc++)
		{
			if ((c_exam_buf[0x02] != 0x06) &&
				(bb_temp  == client_commands1[cc]) &&
				((bb_temp2 == client_commands2[cc]) || (c_exam_data_in_buf <= 8)))
				found_cc = 1;
		}

		if ((!found_cc) && (cc_index < 8192))
		{
			client_commands1[cc_index] = bb_temp;
			client_commands2[cc_index++] = bb_temp2;
			fp = fopen ( "client_commands.txt", "a");
			if (!fp)
			{
				printf ("Unable to log packet data.\n");
			}
			else
			{
				fprintf (fp, "New command: %04X\n", bb_temp);
				fprintf (fp, "Flags: %04X\n", gcn_temp);
				if (c_exam_data_in_buf > 8)
					fprintf (fp, "More info: %04X\n\n", bb_temp2);
				fprintf (fp, "\n");
				fclose (fp);
			}
		}
	}

	rewrite_len = 0;
	check_bypass = 0;
	wrote_item = 0;

	switch ( c_exam_buf[0x02] )
	{
	case 0x06:
		/* Normal text. */
		if (c_exam_buf[0x14] != 0x24)
		{
			memcpy (&rewrite_buf[0], &c_exam_buf[0], 0x14 );
			rewrite_len = 0x14;
			for (ch2 = 0x14;ch2<c_exam_data_in_buf;ch2++)
			{
				/* Replace $ with special char. */
				if ((c_exam_buf[ch2] == 0x24) && (c_exam_buf[ch2+1] == 0x00))
				{
					ch2 += 1;
					rewrite_buf[rewrite_len++] = 0x09;
					rewrite_buf[rewrite_len++] = 0x00;
					rewrite_buf[rewrite_len++] = 0x20;
					rewrite_buf[rewrite_len++] = 0x00;
					if ((c_exam_buf[ch2+1] == 0x24) && (c_exam_buf[ch2+2] == 0x00))
					{
						ch2 += 2;
						rewrite_buf[rewrite_len++] = 0x24;
						rewrite_buf[rewrite_len++] = 0x00;
					}
				}
				else
					rewrite_buf[rewrite_len++] = c_exam_buf[ch2];
			}

			while (rewrite_len % 8)
				rewrite_buf[rewrite_len++] = 0x00;

			memcpy (&rewrite_buf[0], &rewrite_len, 2);
		}
		else
		{
			/* Process custom commands. */

			cmdLen = 0;

			for (ch2=0x16;ch2<c_exam_data_in_buf;ch2+=2)
			{
				if ((c_exam_buf[ch2] != 0x20) && (c_exam_buf[ch2] != 0x00))
				{
					myCmd[cmdLen++] = c_exam_buf[ch2];
				}
				else
					break;
			}

			myCmd[cmdLen] = 0;

			cmdArgs = 0;
			argLen = 0;

			ch2 += 2;

			for (;ch2<c_exam_data_in_buf;ch2+=2)
			{
				if ((c_exam_buf[ch2] != 0x2C)  && (c_exam_buf[ch2] != 0x00))
				{
					myArgs[cmdArgs][argLen++] = c_exam_buf[ch2];
				}
				else
				{
					myArgs[cmdArgs][argLen] = 0;

					/* Was non-zero length? */

					if (argLen)
					{
						cmdArgs++;
					}

					argLen = 0;
					if ( c_exam_buf[ch2] == 0x00 ) break;
				}
			}

			myArgs[cmdArgs][argLen] = 0; /* Just in case it didn't terminate. */

			/* Could use else statements after every if, but fuck that... not needed. */

			if ( ! strcmp ( &myCmd[0], "rawquest") )
			{
				/* Enables or disables quest data logging. */
				if (steal_quest)
				{
					steal_quest = 0;
					client_broadcast ("Raw logging of quest data disabled.");
				}
				else
				{
					steal_quest = 2;
					client_broadcast ("Raw logging of quest data enabled.");

				}
			}

			if ( ! strcmp ( &myCmd[0], "questlog") )
			{
				/* Enables or disables quest data logging. */
				if (steal_quest)
				{
					steal_quest = 0;
					client_broadcast ("Logging of quest data disabled.");
				}
				else
				{
					steal_quest = 1;
					client_broadcast ("Logging of quest data enabled.");

				}
			}

			if ( ! strcmp ( &myCmd[0], "clone") )
			{
				/* Accepts a client ID to clone. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("CLONE: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &stored_full[0], &lobby_clone_full[tid][0], 1244 );
						clone_now = tid;
						partial_clone = 0;
						client_broadcast ("CLONE: User %s cloned! (Change blocks now)", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("CLONE: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "pclone") )
			{
				/* Accepts a client ID to clone. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("PCLONE: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &stored_full[0], &lobby_clone_full[tid][0], 1244 );
						clone_now = tid;
						partial_clone = 1;
						client_broadcast ("PCLONE: User %s partially cloned! (Change blocks now)", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("PCLONE: No such user.");
				}
			}


			if ( ! strcmp ( &myCmd[0], "stopclone") )
			{
				/* Stops cloning a person. */
				clone_now = 0xFF;
				client_broadcast ("STOPCLONE: Clone stopped.  (Change blocks now)");
			}


			if ( ! strcmp ( &myCmd[0], "ql") )
			{
				/* Starts up a quest. */

				if ( cmdArgs < 1 )
				{
					client_broadcast ("QL: Insufficient number of arguments for command..");
				}
				else
				{
					if (strlen (&myArgs[0][0]) < 16)
					{
						client_broadcast ("QL: Invalid quest data.");
					}
					else
					{
						_strupr (&myArgs[0][0]);
						quest_pkt[0x08] = hexToByte (&myArgs[0][0]);
						quest_pkt[0x09] = hexToByte (&myArgs[0][2]);
						quest_pkt[0x0A] = hexToByte (&myArgs[0][4]);
						quest_pkt[0x0B] = hexToByte (&myArgs[0][6]);
						quest_pkt[0x0C] = hexToByte (&myArgs[0][8]);
						quest_pkt[0x0D] = hexToByte (&myArgs[0][10]);
						quest_pkt[0x0E] = hexToByte (&myArgs[0][12]);
						quest_pkt[0x0F] = hexToByte (&myArgs[0][14]);
						memcpy ( &rewrite_buf[0], &quest_pkt[0], sizeof ( quest_pkt ) );
						rewrite_len = sizeof ( quest_pkt );
						client_broadcast ("QL: Loading quest...", lid);
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "reload") )
			{
				/* Accepts a client ID to reload. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("RELOAD: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
#ifdef OPT_PACKETS
						memcpy ( &rewrite_buf[0], &sevenone_pkt[0], sizeof ( sevenone_pkt ) );
						rewrite_buf[0x04] = tid;
						rewrite_len = sizeof ( sevenone_pkt );
#else
						memcpy ( &rewrite_buf[0], &reload_pkt[0], sizeof ( reload_pkt ) );
						rewrite_buf[0x03] = tid;
						rewrite_len = sizeof ( reload_pkt );
#endif
						client_broadcast ("RELOAD: User %s reloaded.", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("RELOAD: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "reloadall") )
			{
				/* Reloads all users. */
				memcpy ( &rewrite_buf[0], &reload_pkt[0], sizeof ( reload_pkt ) );
				rewrite_buf[0x02] = 0x60;
				rewrite_buf[0x03] = 0x00;
				rewrite_len = sizeof ( reload_pkt );
				client_broadcast ("RELOADALL: Reloaded all users.");
			}


			if ( ! strcmp ( &myCmd[0], "pk" ) )
			{
				/* Accepts a client ID to PK. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("PK: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						pk_pkt[0x0A] = tid;
						pk_pkt[0x0E] = 0;
						memcpy ( &rewrite_buf[0], &pk_pkt[0], sizeof ( pk_pkt ) );
						rewrite_len = sizeof ( pk_pkt );
						client_broadcast ("PK: PK'd user %s", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("PK: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "silentall") )
			{
				/* Silent FSODs the room. */
				memcpy ( &rewrite_buf[0], &silent_pkt[0], sizeof ( silent_pkt ) );
				rewrite_buf[0x02] = 0x60;
				rewrite_buf[0x03] = 0x00;
				rewrite_len = sizeof ( silent_pkt );
				client_broadcast ("SILENTALL: Silent FSOD'd room.");
			}


			if ( ! strcmp ( &myCmd[0], "silent") )
			{
				/* Silent FSODs the a person. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SILENT: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
#ifdef OPT_PACKETS
						memcpy ( &rewrite_buf[0], &opt_silent_pkt[0], sizeof ( opt_silent_pkt ) );
						rewrite_buf[0x04] = tid;
						rewrite_len = sizeof ( opt_silent_pkt );
#else
						memcpy ( &rewrite_buf[0], &silent_pkt[0], sizeof ( silent_pkt ) );
						rewrite_buf[0x02] = 0x6D;
						rewrite_buf[0x03] = tid;
						rewrite_len = sizeof ( silent_pkt );
#endif
						client_broadcast ("SILENT: Silent FSOD'd user %s", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("SILENT: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "hide") )
			{
				/* Accepts a client ID to make invisible. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("HIDE: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &hide_pkt[0], sizeof ( hide_pkt ) );
						rewrite_buf[0x0A] = tid;
						rewrite_len = sizeof ( hide_pkt );
						client_broadcast ("HIDE: Hid user %s", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("HIDE: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "show") )
			{
				/* Accepts a client ID to make visible */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SHOW: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &show_pkt[0], sizeof ( show_pkt ) );
						rewrite_buf[0x0A] = tid;
						rewrite_len = sizeof ( show_pkt );
						client_broadcast ("SHOW: Showed user %s", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("SHOW: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "hideall") )
			{
				/* Hides all users. */
				duplicate_for_all ( &rewrite_buf[0], &hide_pkt[0], sizeof ( hide_pkt ), 0x0A );
				rewrite_len = sizeof ( hide_pkt ) * 12;
				client_broadcast ("HIDEALL: Hiding all users." );
			}

			if ( ! strcmp ( &myCmd[0], "showall") )
			{
				/* Shows all users. */
				duplicate_for_all ( &rewrite_buf[0], &show_pkt[0], sizeof ( show_pkt ), 0x0A );
				rewrite_len = sizeof ( show_pkt ) * 12;
				client_broadcast ("SHOWALL: Showing all users." );
			}

			if ( ! strcmp ( &myCmd[0], "teammate") )
			{
				/* Spawns an NPC team mate in slot x. */
				if ( cmdArgs < 2 )
				{
					client_broadcast ("TEAMMATE: Insufficient number of arguments for command. ");
				}
				else
				{
					tid = atoi ( &myArgs[0][0] );
					check_bypass = 1;
					npc_pkt[0x0E] = tid;
					npc_pkt[0x0C] = my_clientid;
					nid = atoi ( &myArgs[1][0] );
					npc_pkt[0x12] = nid;
					memcpy ( &rewrite_buf[0], &npc_pkt[0], sizeof ( npc_pkt ) );
					rewrite_len = sizeof ( npc_pkt );
					client_broadcast ("TEAMMATE: NPC %u spawned in slot %u.", nid, tid );
				}
			}

			if ( ! strcmp ( &myCmd[0], "npcfsod") )
			{
				/* Accepts a client ID to NPCFSOD. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("NPCFSOD: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &npcfsod_pkt[0], sizeof ( npcfsod_pkt ) );
						rewrite_buf[0x02] = 0x6D;
						rewrite_buf[0x03] = tid;
						rewrite_len = sizeof ( npcfsod_pkt );
						client_broadcast ("NPCFSOD: User %s NPCFSOD'd", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("NPCFSOD: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "npcfsodall") )
			{
				memcpy ( &rewrite_buf[0], &npcfsod_pkt[0], sizeof ( npcfsod_pkt ) );
				rewrite_len = sizeof ( npcfsod_pkt );
				client_broadcast ("NPCFSODALL: NPCFSOD'd all." );
			}

			if ( ! strcmp ( &myCmd[0], "disconnect") )
			{
				memcpy ( &rewrite_buf[0], &dc_pkt[0], sizeof ( dc_pkt ) );
				rewrite_len = sizeof ( dc_pkt );
				client_broadcast ("DISCONNECT: Episode 1 & 2 users disconnected." );

			}

			if ( ! strcmp ( &myCmd[0], "fakequest") )
			{
				/* Starts a fake quest. */
				memcpy ( &rewrite_buf[0], &fakequest_pkt[0], sizeof ( fakequest_pkt ) );
				rewrite_len = sizeof ( fakequest_pkt );
				client_broadcast ("FAKEQUEST: Initiated fake quest." );
			}

#ifdef OPT_PACKETS

			if ( ! strcmp ( &myCmd[0], "crash") )
			{
				/* Accepts a client ID to crash. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("CRASH: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &runtime_pkt[0], sizeof ( runtime_pkt ) );
						rewrite_buf[0x04] = tid;
						rewrite_len = sizeof ( runtime_pkt );
						client_broadcast ("CRASH: User %s is recovering from a runtime error", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("CRASH: No such user.");
				}
			}


			if ( ! strcmp ( &myCmd[0], "wreck") )
			{
				/* Accepts a client ID to wreck. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("WRECK: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &seedee_pkt[0], sizeof ( seedee_pkt ) );
						rewrite_buf[0x04] = tid;
						rewrite_len = sizeof ( seedee_pkt );
						client_broadcast ("WRECK: User %s has been WRECKED!", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("WRECK: No such user.");
				}
			}

#endif

			if ( ! strcmp ( &myCmd[0], "qfsod") )
			{
				/* Accepts a client ID to Quest FSOD. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("QFSOD: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
#ifdef OPT_PACKETS
						memcpy ( &rewrite_buf[0], &opt_qfsod_pkt[0], sizeof ( opt_qfsod_pkt ) );
						rewrite_buf[0x04] = tid;
						rewrite_len = sizeof ( opt_qfsod_pkt );
#else
						memcpy ( &rewrite_buf[0], &qfsod_pkt[0], sizeof ( qfsod_pkt ) );
						rewrite_buf[0x03] = tid;
						rewrite_len = sizeof ( qfsod_pkt );
#endif
						client_broadcast ("QFSOD: User %s QFSOD'd", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("QFSOD: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "qfsodall") )
			{
				/* Quest FSODs everyone. */
				duplicate_for_all ( &rewrite_buf[0], &qfsod_pkt[0], sizeof ( qfsod_pkt ), 0x03 );
				rewrite_len = sizeof ( qfsod_pkt ) * 12;
				client_broadcast ("QFSODALL: Quest FSOD's everyone.", tid );
			}

			if ( ! strcmp ( &myCmd[0], "fsod") )
			{
				/* Accepts a client ID to FSOD. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("FSOD: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
#ifdef OPT_PACKETS
						memcpy ( &rewrite_buf[0], &opt_fsod_pkt[0], sizeof ( opt_fsod_pkt ) );
						rewrite_buf[0x04] = tid;
						rewrite_len = sizeof ( opt_fsod_pkt );
#else
						memcpy ( &rewrite_buf[0], &fsod_pkt[0], sizeof ( fsod_pkt ) );
						rewrite_buf[0x03] = tid;
						rewrite_len = sizeof ( fsod_pkt );
#endif
						client_broadcast ("FSOD: User %s FSOD'd", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("FSOD: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "fsodall") )
			{
				/* FSODs everyone. */
				duplicate_for_all ( &rewrite_buf[0], &fsod_pkt[0], sizeof ( fsod_pkt ), 0x03 );
				rewrite_len = sizeof ( fsod_pkt ) * 12;
				client_broadcast ("FSODALL: FSOD'd all." );
			}

			if ( ! strcmp ( &myCmd[0], "schat") )
			{
				/* Accepts a client ID for forced symbol chat. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SCHAT: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &schat_pkt[0], sizeof ( schat_pkt ) );
						rewrite_buf[0x0C] = tid;
						rewrite_len = sizeof ( schat_pkt );
						client_broadcast ("SCHAT: User %s forced to symbol chat.", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("SCHAT: No such user.");
				}

			}

			if ( ! strcmp ( &myCmd[0], "schatall") )
			{
				/* Forces everyone to symbol chat. */
				duplicate_for_all ( &rewrite_buf[0], &schat_pkt[0], sizeof ( schat_pkt ), 8 );
				rewrite_len = sizeof ( schat_pkt ) * 12;
				client_broadcast ("SCHATALL: Everyone forced to symbol chat." );
			}

			if ( ! strcmp ( &myCmd[0], "groove") )
			{
				/* Accepts a client ID for forced dancing. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("GROOVE: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[rewrite_len], &groove_pkt[0], sizeof ( groove_pkt ) );
						rewrite_buf[0x0A] = tid;
						rewrite_len = sizeof ( groove_pkt );
						client_broadcast ("GROOVE: User %s forced to grOoOove!", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("GROOVE: No such user.");
				}   
			}

			if ( ! strcmp ( &myCmd[0], "grooveall") )
			{
				/* Forces everyone to dance. */
				duplicate_for_all ( &rewrite_buf[0], &groove_pkt[0], sizeof ( groove_pkt ), 0x0A );
				rewrite_len = sizeof ( groove_pkt ) * 12;
				client_broadcast ("GROOVEALL: Everyone forced to dance!" );
			}

			if ( ! strcmp ( &myCmd[0], "fakekill") )
			{
				/* Accepts a client ID to fake kill. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("FAKEKILL: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &fakekill_pkt[0], sizeof ( fakekill_pkt ) );
						rewrite_buf[0x0A] = tid;
						rewrite_len = sizeof ( fakekill_pkt );
						client_broadcast ("FAKEKILL: User %s has fainted!", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("FAKEKILL: No such user.");
				}

			}

			if ( ! strcmp ( &myCmd[0], "fakekillall") )
			{
				/* Fake kills everyone. */
				duplicate_for_all ( &rewrite_buf[0], &fakekill_pkt[0], sizeof ( fakekill_pkt ), 0x0A );
				rewrite_len = sizeof ( fakekill_pkt ) * 12;
				client_broadcast ("FAKEKILLALL: Everyone forced to faint!" );
			}

			if ( ! strcmp ( &myCmd[0], "action") )
			{
				/* Accepts a client ID to force an action upon. */
				if ( cmdArgs < 2 )
				{
					client_broadcast ("ACTION: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &action_pkt[0], sizeof ( action_pkt ) );
						rewrite_buf[0x0A] = tid;
						_strupr ( &myArgs[1][0] );
						rewrite_buf[0x0C] = hexToByte ( &myArgs[1][0] );
						if ( cmdArgs > 1 )
							rewrite_buf[0x0E] = 1; else
							rewrite_buf[0x0E] = 0;
						rewrite_len = sizeof ( action_pkt );
						client_broadcast ("ACTION: Forced user %s to do action %u.", (char*) &lobby_name[tid][0], hexToByte ( &myArgs[1][0] ) );
					}
					else
						client_broadcast ("ACTION: No such user.");
				}

			}

			if ( ! strcmp ( &myCmd[0], "actionall") )
			{
				/* Forces an action on everybody. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("ACTIONALL: Insufficient number of arguments for command..");
				}
				else
				{
					_strupr ( &myArgs[0][0] );
					action_pkt[0x0C] = hexToByte ( &myArgs[0][0] );
					if ( cmdArgs > 1 )
						action_pkt[0x0E] = 1; else
						action_pkt[0x0E] = 0;
					duplicate_for_all ( &rewrite_buf[0], &action_pkt[0], sizeof ( action_pkt ), 0x0A );
					rewrite_len = sizeof ( action_pkt ) * 12;
					client_broadcast ("ACTIONALL: Everyone forced to do action %u!", hexToByte ( &myArgs[0][0]) );
				}
			}


			if ( ! strcmp ( &myCmd[0], "walk") )
			{
				/* Accepts a client ID for forced walking. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("WALK: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &walk_pkt[0], sizeof ( walk_pkt ) );
						rewrite_buf[0x0A] = tid;
						rewrite_len = sizeof ( walk_pkt );
						client_broadcast ("WALK: User %s forced to walk!", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("WALK: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "walkall") )
			{
				/* Forces everyone to walk. */
				duplicate_for_all ( &rewrite_buf[0], &walk_pkt[0], sizeof ( walk_pkt ), 0x0A );
				rewrite_len = sizeof ( walk_pkt ) * 12;
				client_broadcast ("WALKALL: Everyone forced to walk!" );
			}



			if ( ! strcmp ( &myCmd[0], "run") )
			{
				/* Accepts a client ID for forced running. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("RUN: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &run_pkt[0], sizeof ( run_pkt ) );
						rewrite_buf[0x0A] = tid;
						rewrite_len = sizeof ( run_pkt );
						client_broadcast ("RUN: User %s forced to run!", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("RUN: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "runall") )
			{
				/* Forces everyone to run. */
				duplicate_for_all ( &rewrite_buf[0], &run_pkt[0], sizeof ( run_pkt ), 0x0A );
				rewrite_len = sizeof ( run_pkt ) * 12;
				client_broadcast ("RUNALL: Everyone forced to run!" );
			}

			if ( ! strcmp ( &myCmd[0], "givesym") )
			{
				/* Gives last symbol chat used with capture flag off. */
				steal_symbolnow = 1;
				memcpy ( &rewrite_buf[0], &steal_schat[0], sizeof ( steal_schat ) );
				rewrite_buf[0x02] = 0x6D;
				rewrite_buf[0x03] = my_clientid;
				rewrite_buf[0x0C] = my_clientid;
				rewrite_buf[0x11] = 0x02;
				rewrite_len = sizeof (steal_schat);
				client_broadcast ("GIVESYM: Displaying last stolen symbol chat.");
			}

			if ( ! strcmp ( &myCmd[0], "lschat") )
			{
				/* Accepts a client ID for forced symbol chat (last stolen). */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("LSCHAT: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						memcpy ( &rewrite_buf[0], &steal_schat[0], sizeof ( steal_schat ) );
						rewrite_buf[0x0C] = tid;
						rewrite_len = sizeof (steal_schat);
						client_broadcast ("LSCHAT: User %s forced to symbol chat.", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("LSCHAT: No such user.");
				}							
			}


			if ( ! strcmp ( &myCmd[0], "lschatall") )
			{
				/* Forces everyone to symbol chat (last stolen). */
				//duplicate_for_all ( &rewrite_buf[0], &steal_schat[0], sizeof ( steal_schat ), 8 );
				//rewrite_len = sizeof ( steal_schat ) * 12;
				//client_broadcast ("LSCHATALL: Everyone forced to symbol chat." );
			}

			if ( ! strcmp ( &myCmd[0], "togglesteal") )
			{
				/* Enables or disables symbol chat theft. */
				if (steal_schats == 1)
				{
					steal_schats = 0;
					client_broadcast ("Symbol chat theft disabled.");
				}
				else
				{
					steal_schats = 1;
					client_broadcast ("Symbol chat theft enabled.");

				}
			}

			if ( ! strcmp ( &myCmd[0], "reparseown") )
			{
				/* Reparses the autoown.txt */
				debug ("Dumped old shit list data.");
				own_count = 0;
				debug ("Loading shit list file: autoown.txt  ");
				if (  ( OwnFile = fopen ("autoown.txt", "r") ) == NULL )
				{
					debug ("File not present.\n");
				}
				else
				{
					while (fgets (&OwnData[0], 255, OwnFile) != NULL)
					{
						auto_own_list[own_count++] = atoi (&OwnData[0]);
						debug ("GCN # %u added to shit list.", auto_own_list[own_count-1]);
						if (fgets (&OwnData[0], 255, OwnFile) == NULL)
							break;
					}
					fclose ( OwnFile );
				}
				client_broadcast ("REPARSEOWN: Reparsed autoown.txt" );
			}

			if ( ! strcmp ( &myCmd[0], "setmycid") )
			{
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SETMYCID: Insufficient number of arguments for command.");
				}
				else
				{
					my_clientid = atoi ( &myArgs[0][0] );
					client_broadcast ("SETMYCID: Set client ID to %u.", my_clientid);
				}
			}

			if ( ! strcmp ( &myCmd[0], "maketech") )
			{
				/* Accepts HEX value for making weapons. */
				if ( cmdArgs < 2 )
				{
					client_broadcast ("MAKETECH: Insufficient number of arguments for command..");
				}
				else
				{
					memcpy (&item_param_2, &zero_dword, 4);
					item_param_2[0] = atoi (&myArgs[0][0]);
					create_pkt[0x18] = 0x03;
					create_pkt[0x19] = 0x02;								
					create_pkt[0x1A] = atoi (&myArgs[1][0]);
					if (create_pkt[0x1A]) create_pkt[0x1A]--;
					create_pkt[0x1B] = 0x00;
					create_pkt[0x0A] = (unsigned char) my_clientid;
					memcpy ( &create_pkt[0x0C], &clientarea[my_clientid], 4 );
					memcpy ( &create_pkt[0x10], &clientx[my_clientid], 4 );
					memcpy ( &create_pkt[0x14], &clienty[my_clientid], 4 );
					memcpy ( &create_pkt[0x1C], &item_param_2[0], 4 );
					memcpy ( &create_pkt[0x20], &zero_dword, 4 );
					memcpy ( &create_pkt[0x28], &zero_dword, 4 );
					memcpy ( &create_pkt[0x24], &currentitemid, 4 );

					for (item_flag=0;item_flag<30;item_flag++)
					{
						if (itemok[item_flag] == 0)
						{
							itemok[item_flag] = currentitemid;
							break;
						}
					}

					give_pkt[0x10] = my_clientid;
					memcpy ( &give_pkt[0x14], &currentitemid, 4);

					currentitemid += 0x00010000;

					memcpy (&rewrite_buf[0], &create_pkt[0], sizeof (create_pkt) );
					rewrite_len = sizeof (create_pkt);

					client_broadcast ("MAKETECH: Item obtained!" );
					wrote_item = 1;
				}
			}

			if ( ! strcmp ( &myCmd[0], "qtech") )
			{
				/* Accepts values for making texts. */
				if ( cmdArgs < 2 )
				{
					client_broadcast ("QTECH: Insufficient number of arguments for command..");
				}
				else
				{
					memcpy (&item_param_2, &zero_dword, 4);
					item_param_2[0] = atoi (&myArgs[0][0]);
					quest_create_pkt[0x0C] = 0x03;
					quest_create_pkt[0x0D] = 0x02;								
					quest_create_pkt[0x0E] = atoi (&myArgs[1][0]);
					if (quest_create_pkt[0x0E]) quest_create_pkt[0x0E]--;
					quest_create_pkt[0x0F] = 0x00;
					memcpy ( &quest_create_pkt[0x10], &item_param_2[0], 4 );
					memcpy ( &quest_create_pkt[0x14], &zero_dword, 4 );

					memcpy (&rewrite_buf[0], &quest_create_pkt[0], sizeof (quest_create_pkt) );
					rewrite_len = sizeof (quest_create_pkt);

					client_broadcast ("QTECH: Sent tech request!" );
				}
			}

			if ( ! strcmp ( &myCmd[0], "bpitem" ) )
			{
				if ( cmdArgs < 1 ) 
				{
					client_broadcast ("BPITEM: Please specify the route for item request.");
				}
				else 
				{
					bproute = atoi ( &myArgs[0][0] );
					if ( bproute > 3 ) bproute = 1;
					if ( bproute < 1 ) bproute = 1;
					if ( cmdArgs < 2 ) bptimes = 1; else
						bptimes = atoi ( &myArgs[1][0] );
					if ( bptimes < 0 ) bptimes = 0;
					pickup_times = bptimes;
					switch ( bproute )
					{
					case 0x01:
						client_broadcast ("BPITEM: Will request %u items from DDWBP1's Rappy route.", bptimes);
						break;
					case 0x02:
						client_broadcast ("BPITEM: Will request %u items from DDWBP1's Zu route.", bptimes);
						break;
					case 0x03:
						client_broadcast ("BPITEM: Will request %u items from DDWBP1's Dorphon route.", bptimes);
						break;
					default: 
						break;
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "bp2item" ) )
			{
				if ( cmdArgs < 1 ) bp2times = 1;
				else bp2times = atoi ( &myArgs[0][0] );
				if ( bp2times < 0 ) bp2times = 0;
				pickup_times = bp2times;
				client_broadcast ("BP2ITEM: Will request %u items from DDWBP2.", bp2times);
			}

			if ( ! strcmp ( &myCmd[0], "qitem") )
			{
				if ( cmdArgs < 1 )
				{
					client_broadcast ("QITEM: Insufficient number of arguments for command..");
				}
				else
				{
					if (strlen (&myArgs[0][0]) < 8)
					{
						client_broadcast ("QITEM: Invalid item length.");
					}
					else
					{
						_strupr (&myArgs[0][0]);
						quest_create_pkt[0x0C] = hexToByte (&myArgs[0][0]);
						quest_create_pkt[0x0D] = hexToByte (&myArgs[0][2]);
						quest_create_pkt[0x0E] = hexToByte (&myArgs[0][4]);
						quest_create_pkt[0x0F] = hexToByte (&myArgs[0][6]);

						memcpy (&quest_create_pkt[0x10], &item_param_2[0], 4 );
						memcpy (&quest_create_pkt[0x14], &item_param_3[0], 4 );

						if (cmdArgs > 1)
						{
							qitemtimes = atoi (&myArgs[1][0]);
						}
						else
						{
							memcpy (&rewrite_buf[0], &quest_create_pkt[0], sizeof (quest_create_pkt) );
							rewrite_len = sizeof (quest_create_pkt);
						}

						client_broadcast ("QITEM: Sent item request!" );
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "makeitem") )
			{
				/* Accepts HEX value for making weapons. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("MAKEITEM: Insufficient number of arguments for command..");
				}
				else
				{
					if (strlen (&myArgs[0][0]) < 8)
					{
						client_broadcast ("MAKEITEM: Invalid item length.");
					}
					else
					{
						//create_pkt[0x0A] = 0x03 - (unsigned char) my_clientid;
						create_pkt[0x0A] = (unsigned char) my_clientid;
						memcpy ( &create_pkt[0x0C], &clientarea[my_clientid], 4 );
						memcpy ( &create_pkt[0x10], &clientx[my_clientid], 4 );
						memcpy ( &create_pkt[0x14], &clienty[my_clientid], 4 );
						_strupr (&myArgs[0][0]);
						create_pkt[0x18] = hexToByte (&myArgs[0][0]);
						create_pkt[0x19] = hexToByte (&myArgs[0][2]);
						create_pkt[0x1A] = hexToByte (&myArgs[0][4]);
						create_pkt[0x1B] = hexToByte (&myArgs[0][6]);
						memcpy (&create_pkt[0x1C], &item_param_2[0], 4 );
						memcpy (&create_pkt[0x20], &item_param_3[0], 4 );
						memcpy (&create_pkt[0x28], &item_param_4[0], 4 );
						memcpy ( &create_pkt[0x24], &currentitemid, 4 );
						currentitemid += 0x00010000;

						memcpy (&rewrite_buf[0], &create_pkt[0], sizeof (create_pkt) );
						rewrite_len = sizeof (create_pkt);

						//client_broadcast ("MAKEITEM: Item ghosted!" );
						//PCLI_COPY (&create_pkt[0], sizeof (create_pkt));
						client_broadcast ("MAKEITEM: Item created!" );
						wrote_item = 1;
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "newskin") )
			{
				/* Changes skin on the fly.  (Jump blocks for effect) */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("NEWSKIN: Insufficient number of arguments for command..");
				}
				else
				{
					wa = atoi ( &myArgs[0][0] );
					//if ( wa > 0x07 ) wa = 0;
					skin_id = wa;
					client_broadcast ("NEWSKIN: Skin set to %u.", wa);
				}
			}

			if ( ! strcmp ( &myCmd[0], "newcolor") )
			{
				/* Changes name color on the fly. (Jump blocks for effect) */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("NEWCOLOR: Insufficient number of arguments for command..");
				}
				else
				{
					if (strlen (&myArgs[0][0]) < 6)
					{
						client_broadcast ("NEWCOLOR: Invalid item length.");
					}
					else
					{
						_strupr (&myArgs[0][0]);
						name_override = 1;
						bb_name[2] = hexToByte (&myArgs[0][0]);
						bb_name[1] = hexToByte (&myArgs[0][2]);
						bb_name[0] = hexToByte (&myArgs[0][4]);
						client_broadcast ("NEWCOLOR Rd: %u Gr:%u Bl:%u", bb_name[2], bb_name[1], bb_name[0] );
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "leavequest") )
			{
				PCLI_COPY (&leavequest_pkt[0], sizeof (leavequest_pkt));
				client_broadcast ("Returning to lobby...", wa);
			}


			if ( ! strcmp ( &myCmd[0], "lw") )
			{
				/* Accepts a lobby to warp self to. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("LW: Insufficient number of arguments for command..");
				}
				else
				{
					wa = atoi ( &myArgs[0][0] );
					if ( ( wa > 0x0F ) || ( wa == 0x00 ) ) wa = 0x01;
					if (!schthack_mode)
						wa = 0xF5 - wa;
					else
						wa--;

					memcpy ( &rewrite_buf[0], &lwarp_pkt[0], sizeof ( lwarp_pkt ) );
					rewrite_buf[0x0C] = wa;
					rewrite_len = sizeof ( lwarp_pkt );
					client_broadcast ("LW: Warped to Lobby %u.", wa);
				}
			}


			if ( ! strcmp ( &myCmd[0], "event") )
			{
				/* Changes lobby event. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("EVENT: Insufficient number of arguments for command..");
				}
				else
				{
					lid = atoi (&myArgs[0][0]);
					if (lid > 0x0E) lid = 0x0E;
					event_pkt[0x04] = lid;

					PCLI_COPY ( &event_pkt[0], sizeof ( event_pkt ) );

					client_broadcast ("EVENT: Set lobby event to %u", lid);
				}
			}


			if ( ! strcmp ( &myCmd[0], "arrow") )
			{
				/* Changes lobby arrow color. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("ARROW: Insufficient number of arguments for command..");
				}
				else
				{
					memcpy ( &rewrite_buf[0], &arrow_pkt[0], sizeof (arrow_pkt) );
					lid = atoi (&myArgs[0][0]);
					rewrite_buf[0x04] = lid;
					rewrite_len = sizeof (arrow_pkt);

					client_broadcast ("ARROW: Set lobby arrow to %u", lid);
				}
			}


			if ( ! strcmp ( &myCmd[0], "warp") )
			{
				/* Accepts a client ID and area to warp them to. */
				if ( cmdArgs < 2 )
				{
					client_broadcast ("WARP: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						wa = atoi ( &myArgs[1][0] );
						memcpy ( &rewrite_buf[0], &warp_pkt[0], sizeof ( warp_pkt ) );
						rewrite_buf[0x02] = 0x6D;
						rewrite_buf[0x03] = tid;
						rewrite_buf[0x0C] = wa;
						rewrite_len = sizeof ( warp_pkt );
						client_broadcast ("WARP: Warp %s to %u.", (char*) &lobby_name[tid][0], wa);

					}
					else
						client_broadcast ("WARP: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "badwarp") )
			{
				/* Accepts a client ID and area to warp them to. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("BADWARP: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						wa = atoi ( &myArgs[1][0] );
						memcpy ( &rewrite_buf[0], &ninefour_pkt[0], sizeof ( ninefour_pkt ) );
						rewrite_buf[0x04] = tid;
						rewrite_len = sizeof ( ninefour_pkt );
						client_broadcast ("BADWARP: Byebye %s.", (char*) &lobby_name[tid][0]);

					}
					else
						client_broadcast ("BADWARP: No such user.");
				}
			}


			if ( ! strcmp ( &myCmd[0], "warpme") )
			{
				/* Accepts an area to warp myself to. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("WARPME: Insufficient number of arguments for command..");
				}
				else
				{
					wa = atoi ( &myArgs[0][0] );

					warp_pkt[0x02] = 0x62;
					warp_pkt[0x03] = my_clientid;
					warp_pkt[0x0C] = wa;

					PCLI_COPY (&warp_pkt[0], sizeof (warp_pkt) );								

					client_broadcast ("WARPME: Warp to %u.", wa);

				}

			}						

			if ( ! strcmp ( &myCmd[0], "warpall") )
			{
				/* Warps everyone to an area */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("WARPALL: Insufficient number of arguments for command..");
				}
				else
				{
					wa = atoi ( &myArgs[0][0] );
					memcpy ( &rewrite_buf[0], &warp_pkt[0], sizeof ( warp_pkt ) );
					rewrite_buf[0x02] = 0x60;
					rewrite_buf[0x03] = 00;
					rewrite_buf[0x0C] = wa;
					rewrite_len = sizeof ( warp_pkt );
					client_broadcast ("WARPALL: Warped all to %u.", wa);
				}
			}

			if ( ! strcmp ( &myCmd[0], "tp" ) )
			{
				/* Accepts a client ID to TP. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("TP: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						pk_pkt[0x0A] = tid;
						pk_pkt[0x0E] = 4;
						memcpy ( &rewrite_buf[0], &pk_pkt[0], sizeof ( pk_pkt ) );
						rewrite_len = sizeof ( pk_pkt );
						client_broadcast ("TP: Restored user %s TP", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("TP: No such user.");
				}
			}


			if ( ! strcmp ( &myCmd[0], "heal" ) )
			{
				/* Accepts a client ID to HEAL. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("HEAL: Insufficient number of arguments for command..");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						pk_pkt[0x0A] = tid;
						pk_pkt[0x0E] = 3;
						memcpy ( &rewrite_buf[0], &pk_pkt[0], sizeof ( pk_pkt ) );
						rewrite_len = sizeof ( pk_pkt );
						client_broadcast ("HEAL: Healed user %s", (char*) &lobby_name[tid][0] );
					}
					else
						client_broadcast ("HEAL: No such user.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "godmode" ) )
			{
				if ( god_mode )
				{
					god_mode = 0;
					client_broadcast ("GODMODE: God mode has been disabled.");
				}
				else
				{
					if ( cmdArgs )
					{
						god_mode = 2;
						client_broadcast ("GODMODE: Team god mode has been enabled.");
					}
					else
					{
						god_mode = 1;
						client_broadcast ("GODMODE: God mode has been enabled.");
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "shoplog" ) )
			{
				if (shoplog)
				{
					shoplog = 0;
					client_broadcast ("No longer logging shop data...");
					fclose (shopf);

				}
				else
				{
					shopcount = 0;
					shoplog = 1;
					client_broadcast ("Now logging shop data...");
					shopf = fopen ( "shop.dat", "ab" );
				}
			}

			if ( ! strcmp ( &myCmd[0], "ddos" ) )
			{
				if ( ddos )
				{
					ddos = 0;
					pso_timeout.tv_usec = 10000;
					client_broadcast ("DDOS: No longer denying access to ships. Console log disabled.");
				}
				else
				{
					ddos = 1;
					if ( cmdArgs  )
					{
						pso_timeout.tv_usec = atoi ( &myArgs[0][0] );
						if ( !pso_timeout.tv_usec  )
							pso_timeout.tv_usec = 1000;
						client_broadcast ("Delay set to %u microseconds", pso_timeout.tv_usec );
					}
					else
						pso_timeout.tv_usec = 1000;
					if ( cmdArgs > 1 )
						ddos_uselast = 1; else
						ddos_uselast = 0;
					client_broadcast ("DDOS: Denying access to ships. Console log enabled.");
					if (ddos_uselast)
						client_broadcast ("Using last input custom packet...");
				}
			}


			if ( ! strcmp ( &myCmd[0], "getgamelist") )
			{
				/* Shows game list. */
				memcpy ( &rewrite_buf[0], &getgame_pkt[0], sizeof ( getgame_pkt ) );
				rewrite_len = sizeof ( getgame_pkt );
				client_broadcast ("GETGAMELIST: Requested game list." );
			}


			if ( ! strcmp ( &myCmd[0], "gcsearch") )
			{
				/* Accepts a guild card # to search.. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("GCSEARCH: Insufficient number of arguments for command..");
				}
				else
				{
					gc_num = atoi ( &myArgs[0][0] );
					memcpy ( &gc_search_pkt[0x0C], &my_gcn, 4 );
					memcpy ( &gc_search_pkt[0x10], &gc_num, 4 );
					memcpy ( &rewrite_buf[0], &gc_search_pkt[0], sizeof ( gc_search_pkt ) );
					rewrite_len = sizeof ( gc_search_pkt );
					client_broadcast ("GCSEARCH: Guild card # %u searched!", gc_num );
				}
			}


			if ( ! strcmp ( &myCmd[0], "firewall") )
			{
				/* Enables or disables the firewall. */
				if (firewall_on)
				{
					firewall_on = 0;
					client_broadcast ("Firewall disabled.");
				}
				else
				{
					firewall_on = 1;
					client_broadcast ("Firewall enabled.");

				}
			}

			/*
			if ( ! strcmp ( &myCmd[0], "noobs") )
			{
			// Enables or disables "noobs only" mode.
			if (noob_time)
			{
			noob_time = 0;
			client_broadcast ("Veterans allowed back in lobby.");
			}
			else
			{
			noob_time = 1;
			client_broadcast ("Veterans are no longer permitted in lobby.");

			}
			}
			*/


			if ( ! strcmp ( &myCmd[0], "settech") )
			{
				/* Accepts HEX value for making techs. */
				if ( cmdArgs < 2 )
				{
					client_broadcast ("SETTECH: Insufficient number of arguments for command..");
				}
				else
				{
					memcpy (&item_param_2, &zero_dword, 4);
					memcpy (&item_param_3, &zero_dword, 4);
					memcpy (&item_param_4, &zero_dword, 4);
					item_param_2[0] = atoi (&myArgs[0][0]);
					item_param_1[0] = 0x03;
					item_param_1[1] = 0x02;
					item_param_1[2] = atoi (&myArgs[1][0]);
					if (item_param_1[2]) item_param_1[2]--;
					item_param_1[3] = 0x00;

					client_broadcast ("SETTECH: Item parameters set!" );
					wrote_item = 1;
				}
			}

			if ( ! strcmp ( &myCmd[0], "toggleforce") )
			{
				/* Enables or disables forced item drops. */
				if (forced_item == 1)
				{
					forced_item = 0;
					client_broadcast ("Forced item drop disabled.");
				}
				else
				{
					forced_item = 1;
					client_broadcast ("Forced item drop enabled.");

				}
			}

			if ( ! strcmp ( &myCmd[0], "levelup") )
			{
				/* Levels up fast with last enemy killed. */
				switch (quick_level)
				{
				case 0x00:
					top_broadcast ("WARNING: Using the levelup command will get you banned from Schtserv.  This command is only in place for use on the official Japanese Blue Burst servers...  Type levelup again if you're serious about invoking it!");
					quick_level = 1;
					break;
				case 0x01:
					kill_pkt[0x0E] = my_clientid;
					quick_level = 2;
					client_broadcast ("Leveling up..!");
					break;
				case 0x02:
				default:
					quick_level = 0;
					client_broadcast ("Level up stopped.");
					break;
				}
			}


			if ( ! strcmp ( &myCmd[0], "setma4") )
			{
				/* Should the MA4 value to start the quest ever change,
				you can use this command to change the packet. */

				if ( cmdArgs < 1 )
				{
					client_broadcast ("SETMA4: Insufficient number of arguments for command..");
				}
				else
				{
					if (strlen (&myArgs[0][0]) < 16)
					{
						client_broadcast ("SETMA4: Invalid quest data.");
					}
					else
					{
						_strupr (&myArgs[0][0]);
						loop_squest_pkt[0x08] = hexToByte (&myArgs[0][0]);
						loop_squest_pkt[0x09] = hexToByte (&myArgs[0][2]);
						loop_squest_pkt[0x0A] = hexToByte (&myArgs[0][4]);
						loop_squest_pkt[0x0B] = hexToByte (&myArgs[0][6]);
						loop_squest_pkt[0x0C] = hexToByte (&myArgs[0][8]);
						loop_squest_pkt[0x0D] = hexToByte (&myArgs[0][10]);
						loop_squest_pkt[0x0E] = hexToByte (&myArgs[0][12]);
						loop_squest_pkt[0x0F] = hexToByte (&myArgs[0][14]);

						client_broadcast ("SETMA4: The MA4 quest start packet has been updated. (Check console)" );
						debug ("Maximum Attack 4 quest starting packet updated (16):");
						display_packet (&loop_squest_pkt[0], sizeof (loop_squest_pkt));

					}
				}
			}


			if ( ! strcmp ( &myCmd[0], "qmarathon") )
			{
				switch ( quest_marathon )
				{
				case 0x00:
					if ( cmdArgs < 1 )
						client_broadcast ("Please provide a file name to load quest queue.");
					else
					{
						memcpy ( &marathon_FileName[0], &myArgs[0][0], strlen ( &myArgs[0][0] ) + 1 );
						client_broadcast ("Loading quest queue from %s ", (char*) &marathon_FileName[0] );
						if ( QLFile = fopen (&marathon_FileName[0], "r") )
						{
							unsigned p;
							marathon_index = 0;
							while (fgets (&QM_Files[marathon_index][0], 255, QLFile) != NULL)
							{
								p = strlen (&QM_Files[marathon_index][0]);
								if (p>255) p=255;
								if (QM_Files[marathon_index][p-1] == 0x0A)
									QM_Files[marathon_index][p--] = 0x00;
								QM_Files[marathon_index++][p] = 0;
							}
							marathon_quests = marathon_index;
							marathon_index = 0;
							fclose ( QLFile );
							if ( marathon_quests > 1 )
							{
								quest_play = 1;
								quest_marathon = 1;
								loop_waitappear = 0;
								loop_index = 0;
								loop_idle = 0;
								loop_tick = 0;
								loop_wait =	10;

								memcpy ( &learn_FileName[0], &QM_Files[0][0], 256 );

								QLFile = fopen (&learn_FileName[0], "rb");
								fseek (QLFile, 0, SEEK_END);
								QLSize = ftell (QLFile);
								fseek (QLFile, 0, SEEK_SET);
								fread ( &quest_identifier, 1, 0x04, QLFile );
								if ( quest_identifier != 0xBEEFDEAD )
								{
									quest_play = 0;
									quest_marathon = 0;
									fclose (QLFile);
									QLFile = NULL;
									client_broadcast ("Failed to read first quest in queue file.  Marathon aborted.");
								}
								else
								{
									fread ( &quest_gametype, 1, 0x01, QLFile );
									fread ( &quest_episode, 1, 0x01, QLFile );
									fread ( &quest_learn_pkt[0], 1, 0x10, QLFile );
									QLWarped = 0;
									if (cmdArgs > 1)
									{
										QLInvSpace = atoi (&myArgs[1][0]);
										client_broadcast ("Will also pick up a max of %u items!", QLInvSpace);
									}
									else
										QLInvSpace = 0;
									if (cmdArgs > 2)
									{
										quest_speed = atoi (&myArgs[2][0]);
										if (quest_speed == 0) quest_speed = 100;
									}
									else
										quest_speed = 100;
									client_broadcast ("Playing quests back @ %u%% speed...", quest_speed );
								}
							}
							else
							{
								client_broadcast ("Not enough quests in file.  Marathon aborted!");
								quest_play = 0;
								quest_marathon = 0;
								QLFile = NULL;
							}
						}
						else
						{
							client_broadcast ("Failed to open file.");
							quest_play = 0;
							QLFile = NULL;
						}
					}
					break;
				case 0x01:
					client_broadcast ("Quest marathon stopped.");
					quest_marathon = 0;
					quest_play = 0;
					if ( QLFile )
						fclose ( QLFile );
					QLFile = NULL;
					break;
				default:
					break;
				}
			}

			if ( ! strcmp ( &myCmd[0], "qplay") )
			{
				switch ( quest_play )
				{
				case 0x00:
					if ( cmdArgs < 1 )
						client_broadcast ("Please provide a file name to load learning data.");
					else
					{
						memcpy ( &learn_FileName[0], &myArgs[0][0], strlen ( &myArgs[0][0] ) + 1 );
						client_broadcast ("Loading quest from %s ", (char*) &learn_FileName[0] );
						if ( QLFile = fopen (&learn_FileName[0], "rb") )
						{
							fseek (QLFile, 0, SEEK_END);
							QLSize = ftell (QLFile);
							fseek (QLFile, 0, SEEK_SET);					
							fread ( &quest_identifier, 1, 0x04, QLFile );
							if ( quest_identifier == 0xBEEFDEAD )
							{
								fread ( &quest_gametype, 1, 0x01, QLFile );
								fread ( &quest_episode, 1, 0x01, QLFile );
								if ( fread ( &quest_learn_pkt[0], 1, 0x10, QLFile ) == 0x10 )
								{
									quest_play = 1;
									quest_marathon = 0;
									loop_waitappear = 0;
									loop_index = 0;
									loop_idle = 0;
									loop_tick = 0;
									loop_wait =	10;
									QLWarped = 0;
									if (cmdArgs > 1)
									{
										QLInvSpace = atoi (&myArgs[1][0]);
										client_broadcast ("Will also pick up a max of %u items!", QLInvSpace);
									}
									else
										QLInvSpace = 0;
									if (cmdArgs > 2)
									{
										quest_speed = atoi (&myArgs[2][0]);
										if (quest_speed == 0) quest_speed = 100;
									}
									else
										quest_speed = 100;
									client_broadcast ("Playing quest back @ %u%% speed...", quest_speed );
								}
								else
								{
									client_broadcast ("Failed to read quest information.");
									quest_play = 0;
									QLFile = NULL;
								}
							}
							else
							{
								client_broadcast ("Quest file incompatible with this version of SPSOF!");
								quest_play = 0;
								QLFile = NULL;
							}
						}
						else
						{
							client_broadcast ("File not found.");
							quest_play = 0;
						}
					}
					break;
				case 0x01:
					client_broadcast ("Quest playback stopped.");
					quest_play = 0;
					quest_marathon = 0;
					if ( QLFile )
						fclose ( QLFile );
					QLFile = NULL;
					break;
				default:
					break;
				}
			}

			if ( ! strcmp ( &myCmd[0], "qlearn") )
			{
				switch ( quest_learn )
				{
				case 0x00:
					if ( cmdArgs < 1 )
						client_broadcast ("Please provide a file name to save learning data.");
					else
					{
						memcpy ( &learn_FileName[0], &myArgs[0][0], strlen ( &myArgs[0][0] ) + 1 );
						client_broadcast ("Learning quest to %s ", (char*) &learn_FileName[0] );							
						quest_learn = 1;
						quest_step = 0;
					}
					break;
				case 0x01:
					client_broadcast ("Quest learning already in progress!  Type $qlearn again to abort");
					quest_learn = 2;
					break;
				case 0x02:
					client_broadcast ("Quest learning aborted.");
					quest_learn = 0;
					break;
				default:
					break;
				}
			}

			if ( ! strcmp ( &myCmd[0], "qunlockep1") )
			{
				quest_unlocking = 1;
				quest_flag = 0x65;
			}

			if ( ! strcmp ( &myCmd[0], "ma4level") )
			{
				/* Levels up fast using Maximum Attack 4.
				(Designed for Schtserv) */
				switch (ma4_loop)
				{
				case 0x00:
					ma4_loop = 1;
					loop_waitappear = 0;
					loop_index = 0;
					loop_idle = 0;
					loop_tick = 0;
					loop_wait = 100;
					monster_index = 0;
					client_broadcast ("Maximum Attack 4 level up loop started!");
					break;
				case 0x01:
				default:
					ma4_loop = 0;
					client_broadcast ("Maximum Attack 4 level up loop stopped.");
					break;
				}
			}


			if ( ! strcmp ( &myCmd[0], "easykill") )
			{
				/* Not quite one hitter quitters activated. */
				if (one_hit_kill == 1)
				{
					one_hit_kill = 0;
					client_broadcast ("Not quite one hit kills disabled.");
				}
				else
				{
					one_hit_kill = 1;
					client_broadcast ("Not quite one hit kills enabled.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "immortalmobs") )
			{
				/* Makes all monsters immortal.  Takes priority over $easykill and $ks */
				if (invinci_mob == 1)
				{
					invinci_mob = 0;
					client_broadcast ("The monsters can hear the black wind...");
				}
				else
				{
					invinci_mob = 1;
					client_broadcast ("Monsters have no fear of death!");
				}
			}



			/* Kill stealing! */ 
			if ( ! strcmp ( &myCmd[0], "ks") )
			{
				if (kill_steal == 1)
				{
					kill_steal = 0;
					client_broadcast ("No longer stealing kills.");
				}
				else
				{
					kill_steal = 1;
					client_broadcast ("Kill stealing enabled!");
				}
			}


			if ( ! strcmp ( &myCmd[0], "setitem1") )
			{
				/* Accepts HEX value for making weapons. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SETITEM1: Insufficient number of arguments for command..");
				}
				else
				{
					_strupr (&myArgs[0][0]);
					if (strlen(&myArgs[0][0]) < 8)
					{
						client_broadcast ("SETITEM1: Invalid parameter length.");
					}
					else
					{
						item_param_1[0] = hexToByte (&myArgs[0][0]);
						item_param_1[1] = hexToByte (&myArgs[0][2]);
						item_param_1[2] = hexToByte (&myArgs[0][4]);
						item_param_1[3] = hexToByte (&myArgs[0][6]);
						client_broadcast ("SETITEM1: Item creation parameters set." );
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "setitem2") )
			{
				/* Accepts HEX value for making weapons. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SETITEM2: Insufficient number of arguments for command..");
				}
				else
				{
					_strupr (&myArgs[0][0]);
					if (strlen(&myArgs[0][0]) < 8)
					{
						client_broadcast ("SETITEM2: Invalid parameter length.");
					}
					else
					{
						item_param_2[0] = hexToByte (&myArgs[0][0]);
						item_param_2[1] = hexToByte (&myArgs[0][2]);
						item_param_2[2] = hexToByte (&myArgs[0][4]);
						item_param_2[3] = hexToByte (&myArgs[0][6]);
						client_broadcast ("SETITEM2: Item creation parameters set." );
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "setitem3") )
			{
				/* Accepts HEX value for making weapons. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SETITEM3: Insufficient number of arguments for command..");
				}
				else
				{
					_strupr (&myArgs[0][0]);
					if (strlen(&myArgs[0][0]) < 8)
					{
						client_broadcast ("SETITEM3: Invalid parameter length.");
					}
					else
					{
						item_param_3[0] = hexToByte (&myArgs[0][0]);
						item_param_3[1] = hexToByte (&myArgs[0][2]);
						item_param_3[2] = hexToByte (&myArgs[0][4]);
						item_param_3[3] = hexToByte (&myArgs[0][6]);
						client_broadcast ("SETITEM3: Item creation parameters set." );
					}
				}
			}


			if ( ! strcmp ( &myCmd[0], "setitem4") )
			{
				/* Accepts HEX value for making weapons. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SETITEM4: Insufficient number of arguments for command..");
				}
				else
				{
					_strupr (&myArgs[0][0]);
					if (strlen(&myArgs[0][0]) < 8)
					{
						client_broadcast ("SETITEM4: Invalid parameter length.");
					}
					else
					{
						item_param_4[0] = hexToByte (&myArgs[0][0]);
						item_param_4[1] = hexToByte (&myArgs[0][2]);
						item_param_4[2] = hexToByte (&myArgs[0][4]);
						item_param_4[3] = hexToByte (&myArgs[0][6]);
						client_broadcast ("SETITEM4: Item creation parameters set." );
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "resetitem") )
			{
				/* Resets item creation parameters. */
				_strupr (&myArgs[0][0]);
				memcpy (&item_param_2, &zero_dword, 4);
				memcpy (&item_param_3, &zero_dword, 4);
				memcpy (&item_param_4, &zero_dword, 4);
				client_broadcast ("RESETITEM: All item creation parameters reset." );
			}

			if ( ! strcmp ( &myCmd[0], "getpos") )
			{
				if ( cmdArgs < 1 )
					tid = my_clientid;
				else
					tid = getcid (&myArgs[0][0]);
				/* Shows the client's current position. */
				if ( tid != 0xFF )
					client_broadcast ("(Current position)\nX=%f,\nY=%f,\nZ=%f,\nH=%f", clientx[tid], clienty[tid],
					clientz[tid], clienth[tid] );
				else
					client_broadcast ("Unable to retrieve position.");
			}

			if ( ! strcmp ( &myCmd[0], "setpos") )
			{
				/* Sets the client's current position. */
				if ( cmdArgs < 2 )
				{
					client_broadcast ("SETPOS: Please specify both the x and y coordinates.");
				}
				else
				{
					if ( cmdArgs < 3 )
						tid = my_clientid;
					else
						tid = getcid (&myArgs[2][0]);
					if ( tid != 0xFF )
					{
						clientx[tid] = (float)atof (&myArgs[0][0]);
						clienty[tid] = (float)atof (&myArgs[1][0]);
						memcpy ( &relocate_pkt[0x0A], &tid, 1 );
						memcpy ( &relocate_pkt[0x0C], &clientx[tid], 4 );
						memcpy ( &relocate_pkt[0x10], &clienth[tid], 4 );
						memcpy ( &relocate_pkt[0x14], &clienty[tid], 4 );
						memcpy ( &relocate_pkt[0x18], &clientz[tid], 4 );
						if (tid == my_clientid)
							PCLI_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) )
						else
						PSRV_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) );
						client_broadcast ("(Set position)\nX=%f,\nY=%f,\nZ=%f,\nH=%f", clientx[tid], clienty[tid],
							clientz[tid], clienth[tid] );
					}
					else
						client_broadcast ("Unable to set position.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "seth") )
			{
				/* Sets the client's current height. */
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SETH: Please specify a desired height.");
				}
				else
				{
					if ( cmdArgs < 2 )
						tid = my_clientid;
					else
						tid = getcid ( &myArgs[1][0] );
					if ( tid != 0xFF )
					{
						clienth[tid] = (float)atof (&myArgs[0][0]);
						memcpy ( &relocate_pkt[0x0A], &tid, 1 );
						memcpy ( &relocate_pkt[0x0C], &clientx[tid], 4 );
						memcpy ( &relocate_pkt[0x10], &clienth[tid], 4 );
						memcpy ( &relocate_pkt[0x14], &clienty[tid], 4 );
						memcpy ( &relocate_pkt[0x18], &clientz[tid], 4 );
						if (tid == my_clientid)
							PCLI_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) )
						else
						PSRV_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) );
						client_broadcast ("(Set position)\nX=%f,\nY=%f,\nZ=%f,\nH=%f", clientx[tid], clienty[tid],
							clientz[tid], clienth[tid] );
					}
					else
						client_broadcast ("Unable to set height.");
				}
			}

			if ( ! strcmp ( &myCmd[0], "savepos" ) )
			{
				if ( cmdArgs < 1 )
				{
					client_broadcast ("SAVEPOS: You need to specify a $recall name for the current position.");
				}
				else
				{
					fp = fopen ( "positions.txt", "a" );
					if ( !fp )
					{
						debug ("Failed to open positions.txt for appending.");
						client_broadcast ("Could not open positions.txt for appending!");
					}
					else
					{
						if (positions_Count < 256)
						{
							if ( cmdArgs < 2 )
								tid = my_clientid;
							else
								tid = getcid ( &myArgs[1][0] );
							if ( tid != 0xFF )
							{
								fprintf (fp, "%s\n", &myArgs[0][0]);
								fprintf (fp, "%f\n", clientx[tid]);
								fprintf (fp, "%f\n", clienty[tid]);
								fprintf (fp, "%f\n", clientz[tid]);
								fprintf (fp, "%f\n", clienth[tid]);
								_strupr (&myArgs[0][0]);
								memcpy (&positions_Name[positions_Count][0], &myArgs[0][0], strlen (&myArgs[0][0])+1);
								memcpy (&positions_Data[positions_Count][0], &clientx[tid], 4 );
								memcpy (&positions_Data[positions_Count][1], &clienty[tid], 4 );
								memcpy (&positions_Data[positions_Count][2], &clientz[tid], 4 );
								memcpy (&positions_Data[positions_Count++][3], &clienth[tid], 4 );
								fclose (fp);
								client_broadcast ("Position saved to positions.txt!");
							}
							else
								client_broadcast ("Unable to save position.");
						}
						else
						{
							client_broadcast ("SAVEPOS: No more room for positions.  (256 maximum limit reached...)");
						}
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "it" ) )
			{
				if ( cmdArgs < 1 )
				{
					client_broadcast ("IT: Please specify a person to instant transmission to.");
				}
				else
				{
					tid = getcid ( &myArgs[0][0] );
					if ( tid != 0xFF )
					{
						if ((clientarea[my_clientid] != clientarea[tid]) && (team_mode))
						{
							warp_pkt[0x02] = 0x62;
							warp_pkt[0x03] = my_clientid;
							memcpy (&warp_pkt[0x0C], &clientarea[tid], 1 );
							PCLI_COPY (&warp_pkt[0], sizeof (warp_pkt) );
							transmission_delay = 1;
							transmission_id = tid;
						}
						else
						{
							memcpy ( &clientx[my_clientid], &clientx[tid], 4 );
							memcpy ( &clienty[my_clientid], &clienty[tid], 4 );
							memcpy ( &clientz[my_clientid], &clientz[tid], 4 );
							memcpy ( &clienth[my_clientid], &clienth[tid], 4 );
							memcpy ( &relocate_pkt[0x0A], &my_clientid, 1 );
							memcpy ( &relocate_pkt[0x0C], &clientx[tid], 4 );
							memcpy ( &relocate_pkt[0x10], &clienth[tid], 4 );
							memcpy ( &relocate_pkt[0x14], &clienty[tid], 4 );
							memcpy ( &relocate_pkt[0x18], &clientz[tid], 4 );
							PCLI_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) )
								client_broadcast ("(Set position)\nX=%f,\nY=%f,\nZ=%f,\nH=%f", clientx[tid], clienty[tid],
								clientz[tid], clienth[tid] );
						}
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "recall" ) )
			{
				if ( cmdArgs < 1 )
				{
					client_broadcast ("RECALL: Please specify a saved position to recall to.");
				}
				else
				{
					_strupr (&myArgs[0][0]);
					//debug ("Total number of %u positions in the recall buffer.", positions_Count );
					for (gcn_temp=0;gcn_temp<positions_Count;gcn_temp++)
					{
						if ( !strcmp (&myArgs[0][0], &positions_Name[gcn_temp][0]) )
						{
							if ( cmdArgs < 2 )
								tid = my_clientid;
							else
								tid = getcid ( &myArgs[1][0] );
							if ( tid != 0xFF )
							{
								memcpy ( &clientx[tid], &positions_Data[gcn_temp][0], 4 );
								memcpy ( &clienty[tid], &positions_Data[gcn_temp][1], 4 );
								memcpy ( &clientz[tid], &positions_Data[gcn_temp][2], 4 );
								memcpy ( &clienth[tid], &positions_Data[gcn_temp][3], 4 );
								memcpy ( &relocate_pkt[0x0A], &tid, 1 );
								memcpy ( &relocate_pkt[0x0C], &clientx[tid], 4 );
								memcpy ( &relocate_pkt[0x10], &clienth[tid], 4 );
								memcpy ( &relocate_pkt[0x14], &clienty[tid], 4 );
								memcpy ( &relocate_pkt[0x18], &clientz[tid], 4 );
								if (tid == my_clientid)
									PCLI_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) )
								else
								PSRV_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) );
								client_broadcast ("(Set position)\nX=%f,\nY=%f,\nZ=%f,\nH=%f", clientx[tid], clienty[tid],
									clientz[tid], clienth[tid] );
							}
							else
								client_broadcast ("Unable to recall position.");
						}
					}
				}
			}

			if ( ! strcmp ( &myCmd[0], "recallall" ) )
			{
				if ( cmdArgs < 1 )
				{
					client_broadcast ("RECALLALL: Please specify a saved position to recall to.");
				}
				else
				{
					_strupr (&myArgs[0][0]);
					//debug ("Total number of %u positions in the recall buffer.", positions_Count );
					for (gcn_temp=0;gcn_temp<positions_Count;gcn_temp++)
					{
						if ( !strcmp (&myArgs[0][0], &positions_Name[gcn_temp][0]) )
						{
							for (ch2=0;ch2<0x0C;ch2++)
							{
								memcpy ( &clientx[tid], &positions_Data[gcn_temp][0], 4 );
								memcpy ( &clienty[tid], &positions_Data[gcn_temp][1], 4 );
								memcpy ( &clientz[tid], &positions_Data[gcn_temp][2], 4 );
								memcpy ( &clienth[tid], &positions_Data[gcn_temp][3], 4 );
								memcpy ( &relocate_pkt[0x0A], &ch2, 1 );
								memcpy ( &relocate_pkt[0x0C], &clientx[ch2], 4 );
								memcpy ( &relocate_pkt[0x10], &clienth[ch2], 4 );
								memcpy ( &relocate_pkt[0x14], &clienty[ch2], 4 );
								memcpy ( &relocate_pkt[0x18], &clientz[ch2], 4 );
								if (ch2 == my_clientid)
								{
									PCLI_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) );
									client_broadcast ("(Set positions)\nX=%f,\nY=%f,\nZ=%f,\nH=%f", clientx[ch2], clienty[ch2],
										clientz[ch2], clienth[ch2] );
								}
								else
									PSRV_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) );
							}
						}
					}
				}
			}

		}

		/* Resend 0x60 packets to the client if they're not destructive. */

		if ( rewrite_len )
		{
			if ( ( rewrite_buf[0x02] == 0x60 ) && ( !wrote_item ) )
			{
				if ( ( !check_packet (&rewrite_buf[0]) ) || ( check_bypass == 1 ) )
					PCLI_COPY (&rewrite_buf[0], rewrite_len);
			}

			if ( ( schthack_mode ) && ( wrote_item ) )
			{
				debug ("GM MAKEITEM changing byte 0x08 to 0xFF.");
				rewrite_buf[0x08] = 0xFF;
			}

			if (!ddos)
			{
				debug ("ModifiedClient (Decrypted:%u):", rewrite_len );
				display_packet (&rewrite_buf[0], rewrite_len );
			}

			PSRV_COPY (&rewrite_buf[0], rewrite_len);
		}
		break;
	case 0x10:
		if ((quest_learn) && (!quest_step) && (c_exam_data_in_buf == 0x10))
			memcpy (&quest_learn_pkt[0], &c_exam_buf[0], 0x10);
		break;
/*	case 0x1D:
		if ( ( ma4_loop ) || ( quest_play ) )
		{
			if ( ( loop_index == 0x02 ) || ( loop_index == 0x04 ) )
			{
				loop_idle++;
				if ( loop_idle == 2 )
				{
					loop_idle = 0;
					loop_index++;
				}
			}
		}
		break; */
	case 0x60:
	case 0x62:
	case 0x6C:
	case 0x6D:
		if ( c_exam_buf[0x02] == 0x60 )
		{
			switch ( c_exam_buf[0x08] )
			{
			case 0x0A:
				// One hit kill?
				if ( ( ( one_hit_kill ) || ( invinci_mob ) ) && ( c_exam_buf[0x09] == 0x03 ) )
				{
						damage_modifier += rand() % 100;
						if (damage_modifier > 2700)
							damage_modifier = 0;
						if (!invinci_mob)
							bb_temp = 0x7530 + damage_modifier; else
							bb_temp = 0xEC77 - damage_modifier;
						memcpy ( &c_exam_buf[0x0E], &bb_temp, 2 );
						PCLI_COPY (&c_exam_buf[0], c_exam_data_in_buf );
						memcpy ( &rewrite_buf[0], &c_exam_buf[0], c_exam_data_in_buf );
						rewrite_len = c_exam_data_in_buf;
				}
				break;
			case 0x1F:
				// Remember client's position.
				memcpy ( &clientarea[my_clientid], &c_exam_buf[0x0C], 4 );
				break;
			case 0x23:
				if (!login_message)
				{
					top_broadcast ("$C7You are logged on with $C4SPSOF$C7.");
					login_message = 1;
				}
				if ( transmission_delay )
				{
					transmission_delay = 0;
					tid = transmission_id;
					memcpy ( &clientx[my_clientid], &clientx[tid], 4 );
					memcpy ( &clienty[my_clientid], &clienty[tid], 4 );
					memcpy ( &clientz[my_clientid], &clientz[tid], 4 );
					memcpy ( &clienth[my_clientid], &clienth[tid], 4 );
					memcpy ( &relocate_pkt[0x0A], &my_clientid, 1 );
					memcpy ( &relocate_pkt[0x0C], &clientx[tid], 4 );
					memcpy ( &relocate_pkt[0x10], &clienth[tid], 4 );
					memcpy ( &relocate_pkt[0x14], &clienty[tid], 4 );
					memcpy ( &relocate_pkt[0x18], &clientz[tid], 4 );
					PCLI_COPY ( &relocate_pkt[0], sizeof ( relocate_pkt ) )
						client_broadcast ("(Set position)\nX=%f,\nY=%f,\nZ=%f,\nH=%f", clientx[tid], clienty[tid],
						clientz[tid], clienth[tid] );
				}

				if ( ( ( ma4_loop ) || ( quest_play ) ) && ( loop_waitappear ) )
				{
					loop_waitappear = 0;
					switch ( loop_index )
					{
					case 0x05:
						// Don't advance the loop index, we're busy killing monsters...
						if ( quest_play )
							QLWarped = 1;
						break;
					case 0x07:
						// Restart the loop when returning to the lobby...
						loop_index = 0;
						break;
					default:
						// Keep going in the loop...
						loop_index++;
					}
					break;
				}
				break;
			case 0x3E:
			case 0x3F:
				// Remember client's position.
				memcpy ( &clientz[my_clientid], &c_exam_buf[0x0C], 4 );
				memcpy ( &clientx[my_clientid], &c_exam_buf[0x14], 4 );
				memcpy ( &clienth[my_clientid], &c_exam_buf[0x18], 4 );
				memcpy ( &clienty[my_clientid], &c_exam_buf[0x1C], 4 );
				break;
			case 0x40:
			case 0x42:
				memcpy ( &clientx[my_clientid], &c_exam_buf[0x0C], 4 );
				memcpy ( &clienty[my_clientid], &c_exam_buf[0x10], 4 );
				break;
				/*		
				case 0x70:
				memcpy ( &rewrite_buf[0], &c_exam_buf[0], c_exam_data_in_buf );
				rewrite_buf[0xF4] = 0x0A;
				rewrite_buf[0x10A] = 0x06;
				rewrite_len = c_exam_data_in_buf;
				break; 
				*/
			case 0xC8:
				// Remember last killed monster.
				memcpy ( &kill_pkt[0], &c_exam_buf[0], c_exam_data_in_buf );
				break;
			default:
				break;
			}
		}

		if ( (c_exam_buf[0x08] == 0xA2) && (forced_item) )
		{
			memcpy ( &rewrite_buf[0], &c_exam_buf[0], c_exam_data_in_buf );
			memcpy ( &rewrite_buf[0x18], &force_pkt[0], 12 );

			// Next values have been verified

			/// Fixes problem with slot number for armor in the wrong byte. (Fix 1)

			if (item_param_1[0] != 0x01) rewrite_buf[0x24] = item_param_2[0]; else
				rewrite_buf[0x24] = item_param_2[1];

			rewrite_buf[0x25] = item_param_1[2];

			// Fixes problem with 02xxxxxx and 03xxxxxx items.

			if ((item_param_1[1]) && (item_param_1[0] < 0x02)) rewrite_buf[0x26] = item_param_1[1] - 1; else
				rewrite_buf[0x26] = item_param_1[1];

			rewrite_buf[0x27] = item_param_1[0];
			rewrite_buf[0x2B] = item_param_1[3];
			rewrite_buf[0x28] = item_param_2[3];
			rewrite_buf[0x29] = item_param_2[2];

			// Fixes problem with slot number for armor in the wrong byte. (Fix 2)

			if (item_param_1[0] != 0x01) rewrite_buf[0x2A] = item_param_2[1]; else
				rewrite_buf[0x2A] = item_param_2[0];

			// Not so much, but they work either way.  (Weapons)

			rewrite_buf[0x2C] = item_param_3[3];
			rewrite_buf[0x2D] = item_param_3[2];
			rewrite_buf[0x2E] = item_param_3[1];
			rewrite_buf[0x2F] = item_param_3[0];
			rewrite_len = c_exam_data_in_buf;

		}
		break;
#ifndef NO_CHAT
	case 0x81:
		/* Log mail! */
		if (c_exam_buf[0x03] == 0x00)
		{
			fp = fopen ( &chat_file_name[0], "a");
			if (!fp)
			{
				printf ("Unable to log to %s\n", &chat_file_name[0]);
			}
			if (chat_start == 0) line_now = 31; else line_now = chat_start - 1;
			chatlines[line_now][0] = 0;
			fprintf (fp, "%s\n", &chatlines[line_now][0]);
			line_now++;
			if (line_now > 31) line_now = 0;
			chatlines[line_now][chat_idx++] = 91;
			chatlines[line_now][chat_idx] = 0;
			_itoa ( now_t.wHour, &convert_buffer[0], 10 );
			if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
			strcat (&chatlines[line_now][0], &convert_buffer[0]);
			strcat (&chatlines[line_now][0], ":");
			_itoa ( now_t.wMinute, &convert_buffer[0], 10 );
			if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
			strcat (&chatlines[line_now][0], &convert_buffer[0]);
			chat_idx = strlen (&chatlines[line_now][0]);
			chatlines[line_now][chat_idx++] = 93;  /*] <*/
			chatlines[line_now][chat_idx++] = 32;
			memcpy (&gcn_temp, &c_exam_buf[48], 4);
			ch = 0;
			for (ch2=16;ch2<40;ch2+=2)
			{
				if (c_exam_buf[ch2] == 0x09) ch2 += 2;
				else
				{
					if ((c_exam_buf[ch2] > 0x20) || (c_exam_buf[ch2] == 0x00))
						name_temp[ch++] = c_exam_buf[ch2]; else
						name_temp[ch++] = 0x20;
				}
			}
			name_temp[ch] = 0;
			sprintf (&chatlines[line_now][chat_idx], "Sent mail as %s to %u ", &name_temp[0], gcn_temp );
			fprintf (fp, "%s\n", &chatlines[line_now][0]);
			line_now++;
			if (line_now > 31) line_now = 0;
			//sprintf (&chatlines[line_now][0], "\"%s\"", &c_exam_buf[34]); /* Naw mean? */
			ch = 0;
			ch2 = 96;
			while (c_exam_buf[ch2] != 0x00)
			{
				if (c_exam_buf[ch2] > 0x20)
					chatlines[line_now][ch++] = c_exam_buf[ch2]; else
					chatlines[line_now][ch++] = 0x20;
				ch2 += 2;
			}
			chatlines[line_now][ch] = 0;
			fprintf (fp, "%s\n", &chatlines[line_now][0]);
			line_now++;
			if (line_now > 31) line_now = 0;
			chatlines[line_now][0] = 0;
			fprintf (fp, "%s\n", &chatlines[line_now][0]);

			/* Passed up like 3 lines.. */

			chat_start += 3;
			if (chat_start > 31) chat_start = chat_start - 32;
			fclose (fp);

			UpdateChatWindow();
		}
		break;
#endif
	case 0x93:
		memcpy ( &rewrite_buf[0], &c_exam_buf[0x00], c_exam_data_in_buf );
		/* Temporary shit */
		// rewrite_buf[0x0024] = 0x38;
		// rewrite_buf[0x0025] = 0x31;
		/* END */
		memcpy ( &check_port, &c_exam_buf[0x0C], 2 );
		if (check_port == my_used_port)
		{
			// Schthack fix #3
			memcpy (&rewrite_buf[0x08], &last_used_addr[0], 4);
			memcpy (&rewrite_buf[0x0C], &last_used_port, 2);
		}
		rewrite_len = c_exam_data_in_buf;
		break;
	case 0x98:
		if ( quest_learn )
		{
			quest_learn = 0;
			client_broadcast ("Quest learning complete!!!");
		}
		break;
	case 0xC1:
		quest_gametype = c_exam_buf[0x52];
		quest_episode = c_exam_buf[0x53];
	case 0xEC:
		team_create = 1;
		break;
	case 0xE7:
		/* Client is logging out. */
		debug ("Client is logging out.");
		debug ("Program exiting...");
		program_exit = 1;
		break;
	default:
		break;
	}

	/* Use the proxy's encryption indexes to reencrypt data and pass it through to server,
	but only if the command wasn't a chat command, because the chat examination procedure
	above will determine whether or not to send a packet to the server.
	(Because of custom commands...) */

	if ( c_exam_buf[0x02] != 0x06 )
	{
		if ( !rewrite_len )
		{
			memcpy ( &rewrite_buf[0], &c_exam_buf[0], c_exam_data_in_buf );
			rewrite_len += c_exam_data_in_buf;
			if (!ddos) 
				debug ("Client (Decrypted:%u):", rewrite_len );
		}
		else
			if (!ddos)
				debug ("ModifiedClient (Decrypted:%u):", rewrite_len );


		if (!ddos)
			display_packet (&rewrite_buf[0], rewrite_len );

		if ( ( quest_learn ) && ( quest_step == 2 ) )
		{
			if ( rewrite_buf[0x02] != 0x1D )
			{
				if ( ( rewrite_buf[0x02] == 0x62 ) && ( rewrite_buf[0x08] == 0x5A ) )
					debug ("Not writing pickup packet to Quest file..."); 
				else
					if ( ( rewrite_buf[0x02] == 0x60 ) && ( rewrite_buf[0x08] == 0x61 ) )
						debug ("Not writing level up confirmation to Quest file...");
					else
					{
						unsigned short quest_len;

						fp = fopen ( &learn_FileName[0], "ab" );
						fwrite ( &quest_delay, 1, 4, fp );
						quest_delay = 0;
						memcpy ( &quest_len, &rewrite_buf[0], 2);
						if ( fwrite ( &rewrite_buf[0], 1, quest_len, fp ) != quest_len )
						{
							client_broadcast ("Failed to write quest packet to %s", (char*) &learn_FileName[0] );
							quest_step = 0;
							quest_learn = 0;
							client_broadcast ("Quest learning aborted.");
						}
						fclose ( fp );
					}
			}
		}

		PSRV_COPY (&rewrite_buf[0], rewrite_len);
		
	}

	c_exam_data_in_buf = 0;
}

//#define DEBUG_SKIN

unsigned test_class = 0x0C;

char quest_file_name[0x10];
unsigned quest_packet_size;

void unencrypt_examine()
{   
	int clientOK = 0;
	unsigned s;

	unsigned short sp;
	struct in_addr sa;

	/* Data received from server needs to be sent to the client.
	Should probably check for overflow here, but it shouldn't happen as unencrypted
	packets only get send during login and just as you connect to a ship. */

	memcpy ( &client_buf[client_data_in_buf], &exam_buf[0], exam_data_in_buf );

	switch (exam_buf[0x02])
	{
	case 0x02:
		/* New keys for patch server */
		clientOK = 1;
		debug ("New encryption keys obtained.");

		memcpy (&s, &exam_buf[68], 4);
		CRYPT_PC_CreateKeys(&p_server_to_client,s);
		CRYPT_PC_CreateKeys(&p_proxy_to_client,s);
		memcpy (&s, &exam_buf[72], 4);
		CRYPT_PC_CreateKeys(&p_client_to_server,s);
		CRYPT_PC_CreateKeys(&p_proxy_to_server,s);

		crypt_on = 1;
		break;
	case 0x03:
		/* New keys! */
		clientOK = 1;
		debug ("New encryption keys obtained.");
		cipher_ptr = &server_to_client;
		pso_crypt_table_init_bb (cipher_ptr, &exam_buf[104]);
		cipher_ptr = &proxy_to_client;
		pso_crypt_table_init_bb (cipher_ptr, &exam_buf[104]);
		cipher_ptr = &client_to_server;
		pso_crypt_table_init_bb (cipher_ptr, &exam_buf[152]);
		cipher_ptr = &proxy_to_server;
		pso_crypt_table_init_bb (cipher_ptr, &exam_buf[152]);
		crypt_on = 1;
		break;
	case 0x19:
		if (exam_data_in_buf < 0x0E)
			memcpy (&sp, &fixup_buf[0x0C], 2);
		else
			memcpy (&sp, &exam_buf[0x0C], 2);

		if (exam_data_in_buf < 0x0C)
			memcpy (&sa, &fixup_buf[0x08], 4);
		else
			memcpy (&sa, &exam_buf[0x08], 4);

		debug ("Connecting to %s : %u", inet_ntoa(sa), sp);

		closesocket (s_sockfd);

		s_sockfd = tcp_sock_connect ( inet_ntoa (sa) , sp );

		debug ("Connection successful.");
		break;
	default:
		break;
	}

	if (clientOK)
		client_data_in_buf += exam_data_in_buf;
}



long CalculateGuildCardChecksum(void* data,unsigned long size)
{
    long offset,y,cs = 0xFFFFFFFF;
    for (offset = 0; offset < (long)size; offset++)
    {
        cs ^= *(unsigned char*)((long)data + offset);
        for (y = 0; y < 8; y++)
        {
            if (!(cs & 1)) cs = (cs >> 1) & 0x7FFFFFFF;
            else cs = ((cs >> 1) & 0x7FFFFFFF) ^ 0xEDB88320;
        }
    }
    return (cs ^ 0xFFFFFFFF);
}

unsigned char gcdata[54720+2];
unsigned gcdata_size = 0;
//#define LOG_EB
#ifdef LOG_EB
unsigned char EB_D[0x20000];
char EB_FN[32][64];
unsigned EB_FS[32];
unsigned EB_PO[32];
unsigned short EB_W = 0;
unsigned EB_S = 0;
unsigned char EB_T, EB_C = 0;
#endif

void server_examine_buffer()
{
	unsigned char_check;
	FILE *fp;
	int l_idx = 0, packet_filtered = 0, pass_Override = 0, p, line_now, name_stop = 0, chat_idx = 0, name_start = 0;
	unsigned short l_port, server_port;
	struct in_addr p_ip;
	unsigned gcn_temp;
	unsigned char qlv_query = 0;
	int qofs = 0, bypass_ship = 0;
	int ch;
	int ch2;
	unsigned backup_CRC32;
	unsigned short quest_adjust, bb_temp, bb_temp2;
	unsigned short* z_temp;
	int ch3;
	unsigned ch4;
	unsigned char cid;
	unsigned sc, found_sc = 0;

	/* Display packet before FireWall checks. */

	if ((!ddos)  && (!shoplog))
	{
		debug ("Server (Decrypted:%u):", exam_data_in_buf);
		display_packet (&exam_buf[0], exam_data_in_buf);
	}

	if (team_mode)
	{
		memcpy (&bb_temp, &exam_buf[0x02],  2);
		memcpy (&gcn_temp, &exam_buf[0x04], 4);
		memcpy (&bb_temp2, &exam_buf[0x08], 2);

		for (sc=0;sc<sc_index;sc++)
		{
			if ((exam_buf[0x02] != 0x06) &&
				(bb_temp  == server_commands1[sc]) &&
				((bb_temp2 == server_commands2[sc]) || (exam_data_in_buf <= 8)))
				found_sc = 1;
		}

		if ((!found_sc) && (sc_index < 8192))
		{
			server_commands1[sc_index] = bb_temp;
			server_commands2[sc_index++] = bb_temp2;
			fp = fopen ( "server_commands.txt", "a");
			if (!fp)
			{
				printf ("Unable to log packet data.\n");
			}
			fprintf (fp, "New command: %04X\n", bb_temp);
			fprintf (fp, "Flags: %04X\n", gcn_temp);
			if (exam_data_in_buf > 8)
				fprintf (fp, "More info: %04X\n", bb_temp2);
			fprintf (fp, "\n");
			fclose (fp);
		}
	}


	switch ( exam_buf[0x02] )
	{
	case 0x01:
		if (((ma4_loop) || (quest_play)) && (loop_index == 0x02))
		{
			client_broadcast ("Loop aborted!  Unable to create team!");
			ma4_loop = 0;
			quest_play = 0;
			loop_index = 0;
		}
		break;
	case 0x04:

		/* Remember our guild card # */

		if ((exam_buf[0x03] == 0x00) && (exam_buf[0x00] == 0x2C))
		{
			memcpy ( &my_gcn, &exam_buf[0x08], 4 );
			memcpy ( &gcfsod_pkt[0x10], &my_gcn, 4 );
		}
		break;
	case 0x11:

		/* Block full. */

		if ( ( exam_buf[0x00] == 0x48 ) && ( exam_buf[0x01] == 0x00 ) )
		{
			memcpy (&server_port, &exam_buf[exam_data_in_buf-4], 2);

			if ((server_port >= 9000) && (server_port < 10000))
			{						
				select_portidx ( &l_port, &l_idx );

				server_ports[l_idx] = server_port;

				creation_num++;
				listen_creation[l_idx] = creation_num;
				listen_ports[l_idx] = l_port;
				listen_active[l_idx] = 1;

				memcpy (&server_addrs[l_idx].s_addr, &exam_buf[exam_data_in_buf-8], 4);

				debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[l_idx]), server_port);
				debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);

				memcpy (&exam_buf[exam_data_in_buf-8], &proxy_addr[0], 4);
				memcpy (&exam_buf[exam_data_in_buf-4], &listen_ports[l_idx], 2);

				/* Setup listener... */

				debug ("Setting up listener..");

				memcpy (&p_ip.s_addr, &proxy_addr[0], 4 );
				listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );

				tcp_listen (listen_sockfds[l_idx]);

			}
		}
		break;
	case 0x14:

		/* Patch server redirect */

		exam_buf[0x0C] = exam_buf[0x09];
		exam_buf[0x0D] = exam_buf[0x08];
		memcpy (&server_port, &exam_buf[0x0C], 2);

		select_portidx ( &l_port, &l_idx );

		server_ports[l_idx] = server_port;

		creation_num++;
		listen_creation[l_idx] = creation_num;
		listen_ports[l_idx] = l_port;
		listen_active[l_idx] = 1;

		memcpy (&server_addrs[l_idx].s_addr, &exam_buf[0x04], 4);

		debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[l_idx]), server_port);
		debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);

		memcpy (&exam_buf[0x04], &proxy_addr[0], 4);

		memcpy (&exam_buf[0x0C], &listen_ports[l_idx], 2);
		exam_buf[0x08] = exam_buf[0x0D];
		exam_buf[0x09] = exam_buf[0x0C];

		/* Setup listener... */

		debug ("Setting up listener..");

		memcpy (&p_ip.s_addr, &proxy_addr[0], 4 );
		listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );

		tcp_listen (listen_sockfds[l_idx]);

		break;
	case 0x13:

		/* Quest logging */

		if ( steal_quest )
		{
			if ( steal_quest == 1 )
			{
				memcpy ( &quest_file_name[0], &exam_buf[0x08], 0x10 );
				fp = fopen ( &quest_file_name[0], "ab");
				memcpy ( &quest_packet_size, &exam_buf[0x418], 4 );
				fwrite ( &exam_buf[0x18], 1, quest_packet_size, fp ); 
				fclose ( fp );
			}
			else
			{
				memcpy ( &quest_file_name[0], &exam_buf[0x08], 0x10 );
				qofs = strlen (&quest_file_name[0] );
				quest_file_name[qofs-3] = 0; /* Chop! */
				strcat (&quest_file_name[0], "raw");
				fp = fopen ( &quest_file_name[0], "ab");
				memcpy ( &quest_adjust, &exam_buf[0x00], 2 );
				debug ("Quest adjust = %u", quest_adjust);
				if ( quest_adjust % 8 ) 
				{
					while ( quest_adjust % 8 )
						quest_adjust++;
				}
				fwrite ( &quest_adjust, 1, 2, fp );
				quest_adjust -= 2;
				fwrite ( &exam_buf[0x02], 1, 2, fp );
				quest_adjust -= 2;
				fwrite ( &exam_buf[0x04], 1, exam_data_in_buf-4, fp );
				quest_adjust -= ( exam_data_in_buf - 4 );
				if ( quest_adjust )
				{
					ch3 = exam_data_in_buf;
					ch4 = quest_adjust;
					while ( ch4 )
					{
						exam_buf[ch3++] = 0;
						ch4--;
					}
					fwrite ( &exam_buf[exam_data_in_buf], 1, quest_adjust, fp );
				}
				fclose ( fp );
			}
		}
		break;
	case 0x19:

		bypass_ship = 0;

		if ( trial_bypass )
		{
			memcpy ( &bb_temp, &exam_buf[0x0C], 2 );
			// Let's trial accounts play on the Japanese servers
			if ( bb_temp == 12020 )
			{
				PCLI_COPY ( &jp_ship_pkt[0], sizeof (jp_ship_pkt) );
				exam_buf[0x02] = 0x1D;
				exam_buf[0x03] = 0x00;
				bypass_ship = 1;
			}
		}

		if ( !bypass_ship )
		{
			/* Ship select packet. */

			select_portidx ( &l_port, &l_idx );

			creation_num++;
			listen_creation[l_idx] = creation_num;
			listen_ports[l_idx] = l_port;
			listen_active[l_idx] = 1;

			if (exam_data_in_buf < 0x0E)
				memcpy (&server_ports[l_idx], &fixup_buf[0x0C], 2);
			else
				memcpy (&server_ports[l_idx], &exam_buf[0x0C], 2);


			if (exam_data_in_buf < 0x0C)
				memcpy (&server_addrs[l_idx].s_addr, &fixup_buf[0x08], 4);
			else
				memcpy (&server_addrs[l_idx].s_addr, &exam_buf[0x08], 4);

			debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[l_idx]), server_ports[l_idx]);
			debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);

			/* Schthack fix #2 */

			exam_buf[0x00] = 0x10;
			exam_data_in_buf = 0x10;

			memcpy (&exam_buf[0x08], &proxy_addr[0], 4);
			memcpy (&exam_buf[0x0C], &listen_ports[l_idx], 2);		

			port_to_save = l_port;

			/* Setup listener... */

			debug ("Setting up listener..");

			memcpy (&p_ip.s_addr, &proxy_addr[0], 4 );
			listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );

			tcp_listen (listen_sockfds[l_idx]);
		}
		break;
	case 0x1A:
		if (!logged_in)
		{
			debug ("Received an error message during login.");
			debug ("Program exiting...");
			program_exit = 1;
		}
		break;
	case 0x1D:
	case 0x22:
		pass_Override = 1;
		break;
	case 0x41:

		/* Meet the user packet. */

		if (exam_buf[0x04] == 0x00)
		{
			select_portidx ( &l_port, &l_idx );

			creation_num++;
			listen_creation[l_idx] = creation_num;
			listen_ports[l_idx] = l_port;
			listen_active[l_idx] = 1;

			memcpy(&server_addrs[l_idx].s_addr, &exam_buf[28], 4);
			memcpy(&server_ports[l_idx], &exam_buf[32], 4);

			debug ("Meet the user result %s : %u", inet_ntoa(server_addrs[l_idx]), server_ports[l_idx]);
			debug ("Rewriting packet.  Result changed to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);

			memcpy (&exam_buf[28], &proxy_addr[0], 4);
			memcpy (&exam_buf[32], &listen_ports[l_idx], 4);

			// Setting up listener.

			debug ("Setting up listener..");

			memcpy (&p_ip.s_addr, &proxy_addr[0], 4 );
			listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );

			tcp_listen (listen_sockfds[l_idx]);
		}
		break;
	case 0x44:
		if ((quest_learn) && (!quest_step))
		{
			quest_step = 1;
			if ( fp = fopen ( &learn_FileName[0], "ab" ) )
			{
				quest_identifier = 0xBEEFDEAD;
				fwrite ( &quest_identifier, 1, 0x04, fp );
				fwrite ( &quest_gametype, 1, 0x01, fp );
				fwrite ( &quest_episode, 1, 0x01, fp );
				if ( fwrite ( &quest_learn_pkt[0], 1, 0x10, fp ) != 0x10 )
				{
					client_broadcast ("Failed to write quest starting packet to %s", (char*) &learn_FileName[0] );
					quest_step = 0;
					quest_learn = 0;
					client_broadcast ("Quest learning aborted.");
				}
				fclose ( fp );
			}
			else
			{
				client_broadcast ("Could not open %s for writing", (char*) &learn_FileName[0] );
				quest_step = 0;
				quest_learn = 0;
				client_broadcast ("Quest learning aborted.");
			}
		}

		/* Quest logging */

		if ( steal_quest == 2 )
		{
			memcpy ( &quest_file_name[0], &exam_buf[0x2C], 0x10 );
			qofs = strlen (&quest_file_name[0] );
			quest_file_name[qofs-3] = 0; /* Chop! */
			strcat (&quest_file_name[0], "raw");
			fp = fopen ( &quest_file_name[0], "ab");
			memcpy ( &quest_adjust, &exam_buf[0x00], 2 );
			debug ("Quest adjust = %u", quest_adjust);
			if ( quest_adjust % 8 ) 
			{
				while ( quest_adjust % 8 )
					quest_adjust++;
			}
			fwrite ( &quest_adjust, 1, 2, fp );
			quest_adjust -= 2;
			fwrite ( &exam_buf[0x02], 1, 2, fp );
			quest_adjust -= 2;
			fwrite ( &exam_buf[0x04], 1, exam_data_in_buf-4, fp );
			quest_adjust -= ( exam_data_in_buf - 4 );
			if ( quest_adjust )
			{
				ch3 = exam_data_in_buf;
				ch4 = quest_adjust;
				while ( ch4 )
				{
					exam_buf[ch3++] = 0;
					ch4--;
				}
				fwrite ( &exam_buf[exam_data_in_buf], 1, quest_adjust, fp );
			}
			fclose ( fp );
		}
		break;

	case 0x60:
		switch (exam_buf[0x08])
		{
		case 0x0A:
			if ( ( kill_steal ) || ( invinci_mob ) ) 
			{
				// Do damage and finish monster.
				damage_modifier += rand() % 100;
				if (damage_modifier > 2700)
					damage_modifier = 0;
				if (!invinci_mob)
					bb_temp = 0x7530 + damage_modifier; else
					bb_temp = 0xEC77 - damage_modifier;
				memcpy (&exam_buf[0x0E], &bb_temp, 2);
				//display_packet (&exam_buf[0], exam_data_in_buf );
				PSRV_COPY (&exam_buf[0], exam_data_in_buf);
				if (!invinci_mob)
				{
					memcpy (&kill_pkt[0x0A], &exam_buf[0x0A], 4 );
					kill_pkt[0x0E] = my_clientid;
					PSRV_COPY (&kill_pkt[0], sizeof (kill_pkt));
					kill_pkt[0x0E] = 0x03 - my_clientid;
					PCLI_COPY (&kill_pkt[0], sizeof (kill_pkt));
					memcpy (&request_drop_pkt[0x0A], &exam_buf[0x0A], 2 );
					request_drop_pkt[0x0C] = clientarea[my_clientid];
					PSRV_COPY (&request_drop_pkt[0], sizeof (request_drop_pkt));
					PCLI_COPY (&request_drop_pkt[0], sizeof (request_drop_pkt));
				}
			}
			break;
		case 0x1F:
			// Remember client's position.
			cid = exam_buf[0x0A];
			if ( cid < 0x0C )
				memcpy ( &clientarea[cid], &exam_buf[0x0C], 4 );
			break;
		case 0x20:
			// Remember client's position.
			cid = exam_buf[0x0A];
			if ( cid < 0x0C )
			{
				memcpy ( &clientarea[cid], &exam_buf[0x0C], 4 );
				memcpy ( &clienty[cid], &exam_buf[0x18], 4 );
				memcpy ( &clientx[cid], &exam_buf[0x10], 4 );
				memcpy ( &clienth[cid], &exam_buf[0x14], 4 );
				clientz[cid] = 0;
				z_temp = ((unsigned short*) &clientz[cid] ) + 2;
				memcpy ( z_temp, &exam_buf[0x0E], 2 );
			}
			break;
		case 0x3E:
		case 0x3F:
			// Remember client's position.
			cid = exam_buf[0x0A];
			if ( cid < 0x0C )
			{
				memcpy ( &clientz[cid], &exam_buf[0x0C], 4 );
				memcpy ( &clientx[cid], &exam_buf[0x14], 4 );
				memcpy ( &clienth[cid], &exam_buf[0x18], 4 );
				memcpy ( &clienty[cid], &exam_buf[0x1C], 4 );
			}
			break;
		case 0x40:
		case 0x42:
			cid = exam_buf[0x0A];
			if ( cid < 0x0C )
			{
				memcpy ( &clientx[cid], &exam_buf[0x0C], 4 );
				memcpy ( &clienty[cid], &exam_buf[0x10], 4 );
			}
			if ( ( kill_steal ) || ( invinci_mob ) )
			{
				// This is actually a positioner...
				exam_buf[0x0A] = my_clientid;
				PSRV_COPY (&exam_buf[0], exam_data_in_buf);
				exam_buf[0x0A] = cid;
			}
			break;
		case 0x43:
			// Attack attempt
			if ( ( kill_steal ) || ( invinci_mob ) )
			{
				tid = exam_buf[0x0A];
				exam_buf[0x0A] = my_clientid;
				//display_packet (&exam_buf[0], exam_data_in_buf );
				PSRV_COPY (&exam_buf[0], exam_data_in_buf);
				exam_buf[0x0A] = tid;
			}
			break;
		case 0x46:
			// Attack result
			if ( ( kill_steal ) || ( invinci_mob ) )
			{
				tid = exam_buf[0x0A];
				exam_buf[0x0A] = my_clientid;
				//display_packet (&exam_buf[0], exam_data_in_buf );
				PSRV_COPY (&exam_buf[0], exam_data_in_buf);
				exam_buf[0x0A] = tid;
			}
			break;
		case 0x59:
			if ( quest_play )
			{
				// Will desync client if it knows it got an item without picking it up itself...
				exam_buf[0x02] = 0x1D;
				exam_buf[0x03] = 0x00;
			}
			break;
		case 0x5D:
		case 0x5F:

			// Auto item pickup enabled?
			if ( ( pickup_times ) && ( exam_buf[0x08] == 0x5D ) )
			{
				debug ("Picking up server generated item!");
				//display_packet ( &pickup_pkt[0], sizeof (pickup_pkt) );
				memcpy ( &pickup_pkt[0x0C], &exam_buf[0x24], 4 );
				PSRV_COPY ( &pickup_pkt[0], sizeof (pickup_pkt) );
				pickup_times--;
				client_broadcast ("Received an item, %u to go...", pickup_times);
			}

			// $qplay pickups
			if ( ( quest_play ) && ( QLInvSpace ) )
			{
				// Don't pick up item by default.
				int do_pickup = 0;
				unsigned itemid;
				unsigned char itemslots;
				unsigned char iteminfo[4];

				if ( exam_buf[0x08] == 0x5D )
				{
					memcpy ( &iteminfo[0], &exam_buf[0x18], 4 ); 
					itemslots = exam_buf[0x1D];
					memcpy ( &itemid, &exam_buf[0x24], 4 );
				}
				else
				{
					memcpy ( &iteminfo[0], &exam_buf[0x1C], 4 ); 
					itemslots = exam_buf[0x21];
					memcpy ( &itemid, &exam_buf[0x28], 4 );
				}

				switch (iteminfo[0])
				{
				case 0x00:
					// Weapons
					if ((iteminfo[1] > 0x0D) || (iteminfo[2] > 0x04))
						do_pickup = 1;
					break;
				case 0x01:
					switch (iteminfo[1])
					{
					case 0x01:
						// Armor
						if ((iteminfo[2] > 0x17) || (itemslots == 0x04))
							do_pickup = 1;
						break;
					case 0x02:
						// Barriers
						if (iteminfo[2] > 0x14)
							do_pickup = 1;
						break;
					case 0x03:
						// Units
						if (iteminfo[2] < 0x64)
						{
							if (unit_check[(iteminfo[2])])
								do_pickup = 1;
						}
						else
							do_pickup = 1;
						break;
					default:
						break;
					}
					break;
				case 0x02:
					// Mags
					do_pickup = 1;
					break;
				case 0x03:
					// Items
					if (iteminfo[1] > 0x08)
					{
						switch (iteminfo[1])
						{
						case 0x09:
							// Scape Doll
							if (doll_check)
								do_pickup = 1;
							break;
						case 0x0A:
							// Grinders
							if ((iteminfo[2] < 0x03) && (grinder_check[(iteminfo[2])]))
								do_pickup = 1;
							break;
						case 0x0B:
							// Materials
							if ((iteminfo[2] < 0x07) && (mat_check[(iteminfo[2])]))
								do_pickup = 1;
							break;
						default:
							do_pickup = 1;
							break;
						}
					}
					else
						if ((iteminfo[1] == 0x02) && (iteminfo[2] >= (qtechlv - 1)))
							// Techniques
							do_pickup = 1;
					break;
				default:
					break;
				}

				if (do_pickup)
				{
					// Pick up the item.
					debug ("Picking up server generated item!");
					memcpy ( &pickup_pkt[0x0C], &itemid, 4 );
					PSRV_COPY ( &pickup_pkt[0], sizeof (pickup_pkt) );
					client_broadcast ("Received a valuable item!!! (Change blocks after stopping $qplay)");
					QLInvSpace --;
				}
			}
			break;
		case 0xB6:
			if (shoplog)
			{
				memcpy ( &bb_temp, &exam_buf[0], 2);
				if ( bb_temp > 0x200 )
					debug ("WARNING shop size > 0x200");
				fwrite ( &exam_buf[0], 1, 0x200, shopf );
				shopcount++;
				if (shopcount == 1000)
				{
					shoplog = 0;
					fclose (shopf);
					client_broadcast ("Done logging shop data!");
				}
				else
					debug ("Logged %u shops.", shopcount);
			}
			break;
		case 0xC8:
			// Remember last killed monster.
			memcpy ( &kill_pkt[0], &exam_buf[0], exam_data_in_buf );
			break;
		default:
			break;
		}
		break;
	case 0x64:
	case 0x65:

		/* Joining a team or another user has joined this team. */

		memcpy ( &lobby_packet[0], &exam_buf[0], exam_data_in_buf );
		lobby_packet[exam_data_in_buf] = 0x00;
		lobby_pktsize = exam_data_in_buf;

		if ( exam_buf[0x02] == 0x64 )
		{
			if (!team_create)
			{
				reset_yes = 1;
			}
			allow_quest = 1;
			team_mode = 1;
			lobby_erase = 1;
		}
		else
		{
			team_mode = 2;
			lobby_erase = 0;
		}
		process_lobby_data(lobby_erase);
		break;
	case 0x66:

		/* A user has left the team. */

		if ( exam_buf[0x03] < 0x04)
		{
			tlc = exam_buf[0x08];
			lobby_user_active[tlc] = 0;
			lobby_update = 1;
		}
		break;
	case 0x67:
	case 0x68:

		/* Users joining or syncing up with lobby? */

		team_create = 0;
		reset_yes = 0;
		team_mode = 0;
		allow_quest = 0;
		lobby_pktsize = exam_data_in_buf;

		memcpy ( &lobby_packet[0], &exam_buf[0], exam_data_in_buf );
		lobby_packet[exam_data_in_buf] = 0x00;

		/* If so, go ahead and process the lobby data. */

		if (exam_buf[0x02] == 0x67) lobby_erase = 1; else lobby_erase = 0;

		process_lobby_data(lobby_erase);
		break;
	case 0x69:

		/* User has left the lobby. */

		if ( exam_buf[0x03] < 0x0C )
		{
			tlc = exam_buf[0x08];
			lobby_user_active[tlc] = 0;
			lobby_update = 1;
		}
	/*case 0x95:
		debug ("95 packet logged.");
		memcpy ( &bb_temp, &exam_buf[0], 2 );
		fp = fopen ( "95packet.bin", "wb" );
		fwrite ( &exam_buf[0], 1, bb_temp, fp );
		fclose ( fp );
		break;*/
	case 0xC4:

		/* Choice search. */

		for (p=0x04;p<exam_data_in_buf;)
		{
			memcpy ( &server_port, &exam_buf[p+108], 2 );
			if ( ( server_port >= 9000 ) && ( server_port < 10000 ) )
			{
				select_portidx ( &l_port, &l_idx );

				creation_num++;
				listen_creation[l_idx] = creation_num;
				listen_ports[l_idx] = l_port;
				listen_active[l_idx] = 1;

				memcpy(&server_addrs[l_idx].s_addr, &exam_buf[p+104], 4);
				memcpy(&server_ports[l_idx], &server_port, 4);

				debug ("Choice search result %s : %u", inet_ntoa(server_addrs[l_idx]), server_ports[l_idx]);
				debug ("Rewriting packet.  Result changed to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);

				memcpy (&exam_buf[p+104], &proxy_addr[0], 4);
				memcpy (&exam_buf[p+108], &listen_ports[l_idx], 4);

				// Setting up listener.

				debug ("Setting up listener..");

				memcpy (&p_ip.s_addr, &proxy_addr[0], 4 );
				listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );

				tcp_listen (listen_sockfds[l_idx]);
			}
			p += 212;
		}

		break;
	/*
	case 0xDC:
		if ( exam_buf[0x03] == 0x02 )
		{
			memcpy (&gcdata[gcdata_size], &exam_buf[0x10], exam_data_in_buf - 0x10);
			gcdata_size += (exam_data_in_buf - 0x10);
			if ( exam_data_in_buf < 26640 )
					debug ("Calculated Guild Card Data Checksum: %08X", (unsigned) CalculateGuildCardChecksum (&gcdata[0], gcdata_size ) );
		} 
		break; 
	case 0xE2:
		debug ("E2 packet logged.");
		memcpy ( &bb_temp, &exam_buf[0], 2 );
		fp = fopen ( "e2packet.bin", "wb" );
		fwrite ( &exam_buf[0], 1, bb_temp, fp );
		fclose ( fp );
		break;
		*/
	case 0xE5:

		/* Name and skin color changing */

		if ( name_override ) memcpy ( &exam_buf[0x2C], &bb_name[0], 4);
		if ( ( clone_now == -1 ) && ( skin_id ) )
		{
			/* Will cause crashing if not checked. */
			exam_buf[0x30] = skin_id - 1;
			exam_buf[0x46] = 0x06;
			//exam_buf[0x44] = 0xFA;
			//exam_buf[0x4C] = 0;
			//exam_buf[0x54] = 0;
			/*
			0x4C = Outfit
			0x54 = Hair 
			*/
		}

		break;
	case 0xE6:
		/* Store guild card # */
		if (exam_buf[0x03] == 0x00) memcpy ( &my_gcn, &exam_buf[0x10], 4 );
		break;
	case 0xE7:
		logged_in = 1;
		/* Backup data */
		ch2 = 0;
		for (ch=0x03C8;ch<0x03CC+0x0018;ch+=2)
		{
			memcpy (&char_check, &exam_buf[ch], 2);
			if (char_check == 0x0000)
				break;
			if ((char_check > 0x0020) && (char_check < 0x0100))
					backup_FileName[ch2++] = exam_buf[ch];
			else
			{
				if (char_check == 0x0009)
					ch+=2;
				else
					backup_FileName[ch2++] = 0x20;
			}
		}
		backup_FileName[ch2++] = 0x20;
		backup_FileName[ch2] = 0;
		trail_work[0] = 0;
		GetLocalTime ( &now_t );
		_itoa ( now_t.wMonth, &convert_buffer[0], 10 );
		strcat (&trail_work[0], &convert_buffer[0]);
		strcat (&trail_work[0], "_");
		_itoa ( now_t.wDay, &convert_buffer[0], 10 );
		strcat (&trail_work[0], &convert_buffer[0]);
		strcat (&trail_work[0], "_");
		_itoa ( now_t.wYear, &convert_buffer[0], 10 );
		strcat (&trail_work[0], &convert_buffer[0]);
		strcat (&trail_work[0], "_");
		_itoa ( now_t.wHour, &convert_buffer[0], 10 );
		strcat (&trail_work[0], &convert_buffer[0]);
		strcat (&trail_work[0], "_");
		_itoa ( now_t.wMinute, &convert_buffer[0], 10 );
		strcat (&trail_work[0], &convert_buffer[0]);
		strcat (&trail_work[0], ".bbc");
		strcat (&backup_FileName[0], &trail_work[0]);
		/* Laffo */
		exam_buf[4] = 0xDE;
		exam_buf[5] = 0xAD;
		exam_buf[6] = 0xBE;
		exam_buf[7] = 0xEF;
		backup_CRC32 = CRC32 ( ( unsigned char* ) &exam_buf[0], 14752 );
		//debug ("CRC32 calculated as %08X \n", backup_CRC32 );
		memcpy (&exam_buf[4], &backup_CRC32, 4 );
		fp = fopen ( &backup_FileName[0], "wb" );
		fwrite ( &exam_buf[0], 1, 14752, fp );
		fclose ( fp );
		exam_buf[4] = 0x00;
		exam_buf[5] = 0x00;
		exam_buf[6] = 0x00;
		exam_buf[7] = 0x00;

		/* Character log */

		if ( name_override ) memcpy ( &exam_buf[0x390], &bb_name[0], 4);
		if ( clone_now != -1 ) 
		{
			if ( clone_now == 0xFF) 
			{
				memcpy (&exam_buf[0x0008], &my_data_full[0], 1244 );
				clone_now = -1;
			}
			else
			{
				if (partial_clone)
					memcpy (&exam_buf[0x0390], &stored_full[0x388], 58 );
				else
					memcpy (&exam_buf[0x0008], &stored_full[0], 1244 );
			}
		}
		else	  
			if (skin_id)
			{
				exam_buf[0x394] = skin_id - 1;
				exam_buf[0x3AA] = 0x06;

				/* Outfit */

				memcpy ( &exam_buf[0x3B0], &null_outfit[0], 10 );

				//exam_buf[936] = 0xFA;
			}
			break;
	case 0xEA:
		if ( ( quest_learn ) && ( quest_step == 1 ) )
			quest_step = 2;
		/*
		if (exam_buf[0x03] == 0x15)
		{
			memcpy (&gcn_temp, &exam_buf[0x08], 4);
			if (gcn_temp == my_gcn)
			{
				debug ("EA packet logged.");
				memcpy ( &bb_temp, &exam_buf[0], 2 );
				fp = fopen ( "eapacket.bin", "wb" );
				fwrite ( &exam_buf[0], 1, bb_temp, fp );
				fclose ( fp );
				break;
			}
		}*/
		break;
#ifdef LOG_EB
	case 0xEB:
		switch (exam_buf[0x03])
		{
		case 0x01:
			EB_S = 0x08;
			EB_T = 0x00;
			debug ("Expecting %u files", exam_buf[0x04] );
			for (EB_C=0x00;EB_C<exam_buf[0x04];EB_C++)
			{
				// Size of file
				memcpy (&EB_FS[EB_T], &exam_buf[EB_S], 4);
				// Packet offset to start of file
				memcpy (&EB_PO[EB_T], &exam_buf[EB_S+0x08], 4);
				// File name
				memcpy (&EB_FN[EB_T][0], &exam_buf[EB_S+0x0C], 64);
				debug ("File %s, size %u", (char*) &EB_FN[EB_T][0], EB_FS[EB_T] );
				EB_T++;
				EB_S += 0x4C;
			}
			EB_C = 0;
			EB_S = 0;
			break;
		case 0x02:
			EB_W = 0x0C;
			memcpy (&bb_temp, &exam_buf[0x00], 2 );
			while ((EB_W < bb_temp) && (EB_C < EB_T))
			{
				EB_D[EB_S++] = (unsigned char) exam_buf[EB_W++];
				if (EB_S >= EB_FS[EB_C])
				{
					// write file
					//
					debug ("Wrote file: %s", &EB_FN[EB_C]);
					fp = fopen ( &EB_FN[EB_C][0], "wb" );
					fwrite ( &EB_D[0], 1, EB_FS[EB_C], fp );
					fclose ( fp );
					debug ("Calculated PSO checksum of file ""%s"" %08X", &EB_FN[EB_C], (unsigned) CalculateGuildCardChecksum (&EB_D[0], EB_FS[EB_C] ) );
					EB_S = 0;
					EB_C++;
				}
			}
			break;
		}
#endif
	case 0xEF:
		if ( trial_bypass )
		{
			exam_buf[0x02] = 0x1D;
			exam_buf[0x03] = 0x00;
		}
		break;
#ifndef NO_CHAT

	case 0x81:
		if (exam_buf[0x03] == 0x00)
		{
			fp = fopen ( &chat_file_name[0], "a");
			if (!fp)
			{
				printf ("Unable to log to %s\n", &chat_file_name[0]);
			}
			if (chat_start == 0) line_now = 31; else line_now = chat_start - 1;
			chatlines[line_now][0] = 0;
			fprintf (fp, "%s\n", &chatlines[line_now][0]);
			line_now++;
			if (line_now > 31) line_now = 0;
			chatlines[line_now][chat_idx++] = 91;
			chatlines[line_now][chat_idx] = 0;
			_itoa ( now_t.wHour, &convert_buffer[0], 10 );
			if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
			strcat (&chatlines[line_now][0], &convert_buffer[0]);
			strcat (&chatlines[line_now][0], ":");
			_itoa ( now_t.wMinute, &convert_buffer[0], 10 );
			if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
			strcat (&chatlines[line_now][0], &convert_buffer[0]);
			chat_idx = strlen (&chatlines[line_now][0]);
			chatlines[line_now][chat_idx++] = 93;  /*] <*/
			chatlines[line_now][chat_idx++] = 32;
			memcpy (&gcn_temp, &exam_buf[0x0C], 4);
			ch = 0;
			for (ch2=16;ch2<40;ch2+=2)
			{
				if (exam_buf[ch2] == 0x09) ch2 += 2; else
				{
					if ((exam_buf[ch2] > 0x20) || (exam_buf[ch2] == 0x00))
						name_temp[ch++] = exam_buf[ch2]; else
						name_temp[ch++] = 0x20;
				}
			}
			name_temp[ch] = 0;
			sprintf (&chatlines[line_now][chat_idx], "Received mail from %s:%u ", &name_temp[0], gcn_temp );
			fprintf (fp, "%s\n", &chatlines[line_now][0]);
			line_now++;
			if (line_now > 31) line_now = 0;
			//sprintf (&chatlines[line_now][0], "\"%s\"", &c_exam_buf[0x22]); /* Naw mean? */
			ch = 0;
			ch2 = 96;
			while (exam_buf[ch2] != 0x00)
			{
				if (exam_buf[ch2] > 0x20)
					chatlines[line_now][ch++] = exam_buf[ch2]; else
					chatlines[line_now][ch++] = 0x20;
				ch2 += 2;
			}
			chatlines[line_now][ch] = 0;
			fprintf (fp, "%s\n", &chatlines[line_now][0]);
			line_now++;
			if (line_now > 31) line_now = 0;
			chatlines[line_now][0] = 0;
			fprintf (fp, "%s\n", &chatlines[line_now][0]);

			/* Passed up like 3 lines.. */

			chat_start += 3;
			if (chat_start > 31) chat_start = chat_start - 32;
			fclose (fp);

			UpdateChatWindow();
		}
		break;

	case 0x06:
		if (exam_buf[0x03] == 0x00)
		{
			chat_idx = 0;
			if (chat_start == 0) line_now = 31; else line_now = chat_start - 1;

			/* Time Stampage -- Hope this isn't too slow, lawlz.. */

			GetLocalTime ( &now_t );
			chatlines[line_now][chat_idx++] = 91;
			chatlines[line_now][chat_idx] = 0;
			_itoa ( now_t.wHour, &convert_buffer[0], 10 );
			if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
			strcat (&chatlines[line_now][0], &convert_buffer[0]);
			strcat (&chatlines[line_now][0], ":");
			_itoa ( now_t.wMinute, &convert_buffer[0], 10 );
			if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
			strcat (&chatlines[line_now][0], &convert_buffer[0]);
			chat_idx = strlen (&chatlines[line_now][0]);
			chatlines[line_now][chat_idx++] = 93;  /*] <*/
			chatlines[line_now][chat_idx++] = 32;
			chatlines[line_now][chat_idx++] = 60;

			p = 16;
			if (exam_buf[p] == 0x09) p += 4;

			name_start = chat_idx;

			while ((!name_stop) && (p < exam_data_in_buf))
			{
				if (exam_buf[p] == 0x09)
				{
					if (exam_buf[p+2] == 0x09) 
						name_stop = 1; else
						p+=2;
				}
				else
				{
						if ((exam_buf[p] > 0x20) || (exam_buf[p] == 0x00))
							chatlines[line_now][chat_idx++] = exam_buf[p]; else
							chatlines[line_now][chat_idx++] = 0x20;
				}
				p+=2;
			}

			chatlines[line_now][chat_idx] = 0;

			_itoa ( getgcn (&chatlines[line_now][name_start]), &convert_buffer[0], 10 );
			chatlines[line_now][chat_idx++] = 58;
			chatlines[line_now][chat_idx++] = 0;
			strcat (&chatlines[line_now][0], &convert_buffer[0]);

			chat_idx = strlen (&chatlines[line_now][0]);

			chatlines[line_now][chat_idx++] = 62;
			chatlines[line_now][chat_idx++] = 32;

			/* Name and GCN acquired */

			name_stop = 0;

			while ((!name_stop) && (p < exam_data_in_buf))
			{
				if (exam_buf[p] != 0x00)
				{
					if (exam_buf[p] == 0x09) p+=2;
					else
					{
						if (exam_buf[p] > 0x20)
							chatlines[line_now][chat_idx++] = exam_buf[p]; else
							chatlines[line_now][chat_idx++] = 0x20;
					}
				}
				else
					name_stop = 1;
				p+=2;
			}

			chatlines[line_now][chat_idx] = 0;

			fp = fopen ( &chat_file_name[0], "a");
			if (!fp)
			{
				printf ("Unable to log to %s\n", &chat_file_name[0]);
			}
			fprintf (fp, "%s\n", &chatlines[line_now][0]);
			fclose (fp);

			UpdateChatWindow();

		}
		break;

#endif

	default:
		break;

	}


	/* The following code merely enables you to become an NPC like Momoka, Bank Lady, and so on...

	If you uncomment it and change the 0x0F value, you'll get different NPCs.

	You will have to relog on for it to take effect.  I think I had it only working with the
	Hunewearl class.

	You'll probably crash in the lobby after logging on and I had it enabled via a toggle
	command which I removed.

	If you readd your own toggle command, toggle it on just before you enter a game and 
	start a quest.  If people join while the quest is in progress, they'll see your NPC.

	However, once the both of you go down to the Forest, you'll both FSOD. : )


	if ((exam_buf[0x02] == 0xE5) && (exam_buf[0x45] == 0x01))
	{
	exam_buf[0x30] = 0x01;
	exam_buf[0x44] = 0;
	exam_buf[0x45] = 0x0F;
	exam_buf[0x46] = 0;
	}
	if ((exam_buf[0x02] == 0xE7) && (exam_buf[0x3A9] == 0x01))
	{
	exam_buf[0x394] = 0x01;
	exam_buf[0x3A8] = 0;
	exam_buf[0x3A9] = 0x0F;
	exam_buf[0x3AA] = 0;
	exam_buf[0x1ACE] = 0;
	exam_buf[0x1ACF] = 0x0F;
	}
	*/


	// debug ("Firewall: Analyzing sub-packet %02X (%u)", exam_buf[0], exam_data_in_buf );

	if ((!ddos) && (!shoplog))
	{
		if ( check_packet ( &exam_buf[0] ) ) packet_filtered = 1;
	}

	if (((!ddos) && (!shoplog)) || (pass_Override))
	{
		/* Reencrypt data and pass it through to client. */

		PCLI_COPY (&exam_buf[0], exam_data_in_buf);		
	}

	exam_data_in_buf = 0;

}


// Sends a custom packet to BOTH the client and server WITHOUT checking if it's volatile.

void CP (char* pkt, unsigned pkl)
{

	debug ("Custom Packet (%u):", pkl);
	display_packet (pkt, pkl);
	PSRV_COPY (pkt, pkl);
	PCLI_COPY (pkt, pkl);
}

// Grabs an IP address from a specified line of a text file.

void get_ip_from_file ( unsigned char *srv_addr, const char* theFile, unsigned lineNum )
{
	unsigned p,p2,p3;
	FILE *IPFile;
	char IPData[256];

	if (  ( IPFile = fopen (theFile, "r") ) == NULL )
	{
		debug ("File not present.\n");
		if (strcmp("proxyip.txt",theFile))
		{
			debug ("\nSPSOF cannot continue without %s!!", theFile);
			exit(1);
		}
	}
	else
	{
		int alpha = 0;
		for (p=0;p<lineNum;p++)
		{
			if (fgets (&IPData[0], 255, IPFile) == NULL)
			{
				debug ("%s appears to be corrupted.", theFile);
				exit (1);
			}
		}
		p = strlen (&IPData[0]);
		for (p=0;p<strlen(&IPData[0]);p++)
			if (((IPData[p] >= 65 ) && (IPData[p] <= 90)) ||
				((IPData[p] >= 97 ) && (IPData[p] <= 122)))
				alpha = 1;
		if (alpha)
		{
			struct hostent *IP_host;

			IPData[strlen(&IPData[0])-1] = 0x00;
			debug ("Resolving %s ...", (char*) &IPData[0] );
			debug ("\nWARNING: This will FAIL to resolve the real server IP if an entry \nfor the host \"%s\" exists in your HOSTS file!!\n", (char*) &IPData[0]);
			IP_host = gethostbyname (&IPData[0]);
			if (!IP_host)
			{
				debug ("Could not resolve host name.");
				exit (1);
			}
			memcpy (srv_addr, IP_host->h_addr, 4 );
		}
		else
		{
		p2 = 0;
		p3 = 0;
		for (p=0;p<strlen(&IPData[0]);p++)
		{
			if ((IPData[p] > 0x20) && (IPData[p] != 46))
				convert_buffer[p3++] = IPData[p]; else
			{
				convert_buffer[p3] = 0;
				if (IPData[p] == 46) // .
				{
					srv_addr[p2] = atoi (&convert_buffer[0]);
					p2++;
					p3 = 0;
					if (p2>3)
					{
						debug ("%s appears to be corrupted.", theFile);
						exit (1);
					}
				}
				else
				{
					srv_addr[p2] = atoi (&convert_buffer[0]);
					if (p2 != 3)
					{
						debug ("%s appears to be corrupted.", theFile);
						exit (1);
					}
					break;
				}
			}
		}
		}
		fclose ( IPFile );
	}
}

void get_port_from_file ( unsigned short *srv_port, const char* theFile, unsigned lineNum )
{
	unsigned p;
	FILE *PortFile;
	char PortData[256];

	if (  ( PortFile = fopen (theFile, "r") ) == NULL )
	{
		debug ("File not present.\n\nSPSOF cannot continue without %s!!", theFile);
		exit(1);
	}
	else
	{
		for (p=0;p<lineNum;p++)
		{
			if (fgets (&PortData[0], 255, PortFile) == NULL)
			{
				debug ("%s appears to be corrupted.", theFile);
				exit (1);
			}
		}
		*srv_port = atoi ( &PortData[0] );
		fclose ( PortFile );
	}
}



#ifndef NO_IND
unsigned char decrypt_buf[BUF_SIZE+32];
#endif
unsigned char encrypt_buf[BUF_SIZE+32];
unsigned char c_encrypt_buf[BUF_SIZE+32];
unsigned char s_encrypt_buf[BUF_SIZE+32];
unsigned short c_expect = 8;
unsigned short s_expect = 8;
unsigned short c_rcvd = 0;
unsigned short s_rcvd = 0;

/*****************************************************************************/
int main(int argc, char *argv[])
{

#ifndef NO_IND

	/* Pointer to the actual .IND file. */

	FILE *INDFile;

	/* Using v1.1 IND? */

	int INDv11 = 0;

	/* Storage space for the identifier value which is right at the beginning of an .IND file. */

	unsigned INDIdentifier;

	/* Variable for the length of the command line description text. */

	unsigned INDDescLen;

	/* Pointer to the actual command line description text. */

	char* INDDesc;

	/* Variable for the number of commands inside of the .IND file.

	Note: Editing this text is not supported in the .IND file editor.  You can use INDMerge for that.

	*/

	unsigned INDNumCommands = 0;

	/* Variables for the packet lengths of each packet. */

	unsigned INDPktLen[256];

	/* Variables for the actual packets themselves. */

	unsigned char INDPkt[256][8192];

	/* Packet key as read from the file. */

	unsigned short INDPktKeys[256];

	/* Targets */

	unsigned short INDTarget[256];

	int pktKeys_deleted = 0;
	unsigned char my_target = 0;
	int process_UserPkts = 0;
	int shift_down = 0;
	int tab_down = 0;

	/* F keys included. */

	unsigned short FKeys_Down[256];

	/* Dummy variable used during file reads to ensure that data was read correctly. */

	unsigned readOK;

	unsigned short tPos;

	int kidx;

	/* Keys down? */

	unsigned short INDPktKeys_Down[256];

#endif
	int wserror;
	int tmp_sockfd, p_sockfd, pkt_len;	
	struct in_addr p_ip;
	struct in_addr temp_ip;
	unsigned short l_port;
	int l_idx;
	struct sockaddr_in csa;
	unsigned int csa_len;
	int bytes_sent = 0;
	int ch;
	struct in_addr pso_ip;
	unsigned short pso_p, random_damage;
	unsigned short pkt_bytes_left = 0;
	int no_windows = 0, arg_idx;
	unsigned i, j, swap;
	FILE* qp_file;

	/* Packet read counter */

	unsigned p,di;

	WSADATA winsock_data;

	for (ch=0;ch<30;ch++)
		itemok[ch] = 0;

	for (ch=0;ch<0x0C;ch++)
	{
		lobby_user_active[ch] = 0;
	}

	log_file_name[0] = 0;
	chat_file_name[0] = 0;
	trail_work[0] = 0;

	strcat (&log_file_name[0], "spsof ");
	strcat (&chat_file_name[0], "chat ");
	GetLocalTime ( &now_t );
	_itoa ( now_t.wMonth, &convert_buffer[0], 10 );
	strcat (&trail_work[0], &convert_buffer[0]);
	strcat (&trail_work[0], "_");
	_itoa ( now_t.wDay, &convert_buffer[0], 10 );
	strcat (&trail_work[0], &convert_buffer[0]);
	strcat (&trail_work[0], "_");
	_itoa ( now_t.wYear, &convert_buffer[0], 10 );
	strcat (&trail_work[0], &convert_buffer[0]);
	strcat (&trail_work[0], "_");
	_itoa ( now_t.wHour, &convert_buffer[0], 10 );
	strcat (&trail_work[0], &convert_buffer[0]);
	strcat (&trail_work[0], "_");
	_itoa ( now_t.wMinute, &convert_buffer[0], 10 );
	strcat (&trail_work[0], &convert_buffer[0]);
	strcat (&trail_work[0], ".txt");
	strcat (&log_file_name[0], &trail_work[0]);
	strcat (&chat_file_name[0], &trail_work[0]);
	debug ("Logging session to: %s", &log_file_name[0] );

#ifndef NO_CHAT
	debug ("Logging chat to: %s", &chat_file_name[0] );
#endif

	WSAStartup(MAKEWORD(1,1), &winsock_data);

	debug ("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
	debug ("Phantasy Star Online Blue Burst FireWall & Packet Editor v%s", SPSOF_VERSION);
	debug ("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
	debug ("PSO encryption functions by Myria");
	debug ("Proxy, FireWall & Packet code by Sodaboy");
	debug ("Goofy ass people in the GUI window [Schthack, Val-Chan, Opt, Bones,");
	debug ("Spider-Man, Venom, Carnage, Richter Belmont, Dracula]");
	debug ("Program (c) 2005,2008 Sodaboy (ragnarok@vengefulsoda.com)");
	debug ("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");


#ifndef NO_IND

	/* Grab packets from editor file */

	/* Attempt to open up the .IND file for reading. */

	debug ("Loading packet control file: spsofedit.ind  ");
	if (  ( INDFile = fopen ("spsofedit.ind", "rb") ) == NULL )
	{
		debug_perror ("Failed to open spsofedit.ind for editing.\n");
		exit (1);
	}
	readOK = fread ( &INDIdentifier, 4, 1, INDFile );
	if ( readOK != 1 )
	{
		debug_perror ("Unexpected end of file.\n");
		exit (2);
	}

	if (INDIdentifier == 0xDEADBEEF)
	{
		INDv11 = 1;
	}

	/* Read the length of the command line description text. */

	readOK = fread ( &INDDescLen, 4, 1, INDFile );
	if ( readOK != 1 )
	{
		debug_perror ("Unexpected end of file.\n");
		exit (2);
	}

	/* Allocate memory for the description text and read it from the file. */

	INDDesc = (char*) malloc ( INDDescLen );

	readOK = fread ( INDDesc, INDDescLen, 1, INDFile );
	if ( readOK != 1 )
	{
		debug_perror ("Unexpected end of file.\n");
		exit (2);
	}

	INDDesc[INDDescLen] = 0;

	/* Read the number of packet commands from the file. */

	readOK = fread ( &INDNumCommands, 4, 1, INDFile );
	if ( readOK != 1 )
	{
		debug_perror ("Unexpected end of file.\n");
		exit (2);
	}

	pktKeys_deleted = 0;

	/* Read all packets from the file. */

	for (p=0;p<INDNumCommands;p++)
	{
		kidx = p - pktKeys_deleted;

		/* Read virtual key assigned to packet. */

		readOK = fread ( &INDPktKeys[kidx], 2, 1, INDFile );
		if ( readOK != 1 )
		{
			debug_perror ("Unexpected end of file.\n");
			exit (2);
		}

		if ((( INDPktKeys[kidx] < 0x0070 ) && ( INDPktKeys[kidx] > 0x007B )) || (INDPktKeys[kidx] == 0x00A1) || (INDPktKeys[kidx] == 0x00A3) || (INDPktKeys[kidx] == 0x0009))
		{
			debug ("A packet was ignored in the .IND file.\nPlease do not use reserved keys. (Fx, Right Ctrl, Right Shift, TAB)");
			pktKeys_deleted++;
		}

		/* Get key state. */

		GetAsyncKeyState(INDPktKeys[kidx]);

		/* Set keys down variable for key to 0. */

		INDPktKeys_Down[kidx] = 0;

		/* Supporting targetting? */

		if ( INDv11 )
		{
			readOK = fread ( &INDTarget[kidx], 2, 1, INDFile );
			if ( readOK != 1 )
			{
				debug_perror ("Unexpected end of file.\n");
				exit (2);
			}
		}
		else
		{
			INDTarget[kidx] = 0;
		}

		/* Read length of packet. */

		readOK = fread ( &INDPktLen[kidx], 4, 1, INDFile );
		if ( readOK != 1 )
		{
			debug_perror ("Unexpected end of file.\n");
			exit (2);
		}

		/* Read the actual packet. */
		readOK = fread ( &INDPkt[kidx][0], INDPktLen[kidx], 1, INDFile );
		if ( readOK != 1 )
		{
			debug_perror ("Unexpected end of file.\n");
			exit (2);
		}

	}

	fclose (INDFile);

	/* Adjust for # of commands deleted. */

	INDNumCommands -= pktKeys_deleted;

#endif

	for (p=0;p<10;p++) 
		null_outfit[p] = 0x00;

	bb_name[0] = 0xFF;
	bb_name[1] = 0xFF;
	bb_name[2] = 0xFF;
	bb_name[3] = 0xFF;

#ifdef SCHTHACK

	schthack_mode = 1;

#endif


	for (arg_idx=1;arg_idx<argc;arg_idx++)
	{
		switch (argv[arg_idx][0])
		{
		case 45:
			if ((argv[arg_idx][1] == 119) || (argv[arg_idx][1] == 87))
			{
				debug ("All windows disabled.\n");
				no_windows = 1;
			}

			if (argv[arg_idx][1] == 97)
			{
				debug ("Connecting to alternate server.");
				alternate_server = 1;
			}

			if (argv[arg_idx][1] == 116)
			{
				debug ("Allowing trial account on full ships.");
				trial_bypass = 1;
				schthack_mode = 0;
				debug ("Automatically enabling Japanese server compatibility...");
			}

			if (argv[arg_idx][1] == 106)
			{
				debug ("Japanese server compatibility turned on.");
				schthack_mode = 0;
			}

			break;
		case 83:
			skin_id = atoi (&argv[arg_idx][1]);
			debug ("Skin mode activated.");
			switch (skin_id) 
			{
			case 0: 
				debug ("No change.\n");
				break;
			case 1:
				debug ("Enabling use of GM skin.\n");
				break;
			case 2:
				debug ("Enabling use of Rico skin.\n");
				break;
			case 3:
				debug ("Enabling use of Sonic skin.\n");
				break;
			case 4:
				debug ("Enabling use of Knuckles skin.\n");
				break;
			case 5:
				debug ("Enabling use of Tails skin.\n");
				break;
			case 6:
				debug ("Enabling use of Flowen skin.\n");
				break;
			case 7:
				debug ("Enabling use of Elly skin.\n");
				break;
			default:
				debug ("Unknown skin ID selected.\n");
				skin_id = 0;
				break;
			}
			break;				  

		default:
			if (strlen (argv[arg_idx]) > 5)
			{
				debug ("Name color activated.");
				name_override = 1;
				bb_name[3] = 0xFF;
				_strupr (&argv[arg_idx][0]);
				bb_name[2] = hexToByte (&argv[arg_idx][0]);
				bb_name[1] = hexToByte (&argv[arg_idx][2]);
				bb_name[0] = hexToByte (&argv[arg_idx][4]);
				debug ("Red saturation of name set to %02X.", bb_name[2]);
				debug ("Green saturation of name set to %02X.", bb_name[1]);
				debug ("Blue saturation of name set to %02X.\n", bb_name[0]);
			}

			break;


		}
	}

	/* Loading proxyip.txt */

	debug ("Loading proxy override IP information: proxyip.txt");
	get_ip_from_file ((unsigned char*) &proxy_addr[0], "proxyip.txt", 1);

	/* Loading serverip.txt */

	debug ("Loading main server IP information: serverip.txt");

	get_ip_from_file ((unsigned char*) &mainpatch_addr[0], "serverip.txt", 1);
	get_ip_from_file (&mainsrv_addr[0], "serverip.txt", 2);
	get_port_from_file (&mainpatch_port, "serverip.txt", 3 );
	get_port_from_file (&mainsrv_port, "serverip.txt", 4 );

	debug ("Patch server: %u.%u.%u.%u:%u", mainpatch_addr[0], mainpatch_addr[1], mainpatch_addr[2], mainpatch_addr[3], mainpatch_port, mainpatch_port  );
	debug ("Game server: %u.%u.%u.%u:%u", mainsrv_addr[0], mainsrv_addr[1], mainsrv_addr[2], mainsrv_addr[3], mainsrv_port, mainsrv_port  );

	/* Loading serverip2.txt */

	debug ("\nLoading alternate server IP information: serverip2.txt");

	get_ip_from_file (&altpatch_addr[0], "serverip2.txt", 1);
	get_ip_from_file (&altsrv_addr[0], "serverip2.txt", 2);
	get_port_from_file (&altpatch_port, "serverip2.txt", 3 );
	get_port_from_file (&altsrv_port, "serverip2.txt", 4 );

	debug ("Patch server: %u.%u.%u.%u:%u", altpatch_addr[0], altpatch_addr[1], altpatch_addr[2], altpatch_addr[3], altpatch_port, altpatch_port  );
	debug ("Game server: %u.%u.%u.%u:%u", altsrv_addr[0], altsrv_addr[1], altsrv_addr[2], altsrv_addr[3], altsrv_port, altsrv_port  );

	if (alternate_server)
	{
		memcpy(&pso_ip.s_addr, &altpatch_addr[0], 4);
		BB_PORT = altsrv_port;
		BB_PATCH = altpatch_port;
	}
	else
	{
		memcpy(&pso_ip.s_addr, &mainpatch_addr[0], 4);
		BB_PORT = mainsrv_port;
		BB_PATCH = mainpatch_port;
	}


	ma4_nummobs = sizeof (ma4_monster_list) / 4L;

	/* Loading positions.txt */

	debug ("\nLoading recall positions: positions.txt ");

	if ( ( OwnFile = fopen ("positions.txt", "r" ) ) == NULL )
	{
		debug ("File not present.\n");
	}
	else
	{
		int good_position = 1;
		while (good_position)
		{
			if (fgets (&positions_Name[positions_Count][0], 255, OwnFile) != NULL)
			{
				p = strlen (&positions_Name[positions_Count][0]);
				if (positions_Name[positions_Count][p-1] == 0x0A)
					positions_Name[positions_Count][p--] = 0x00;
				positions_Name[positions_Count][p] = 0x00;
				_strupr (&positions_Name[positions_Count][0]);
			}
			else
				good_position = 0;
			if (fgets (&OwnData[0], 255, OwnFile) != NULL)
				positions_Data[positions_Count][0] = (float) atof(&OwnData[0]);
			else
				good_position = 0;
			if (fgets (&OwnData[0], 255, OwnFile) != NULL)
				positions_Data[positions_Count][1] = (float) atof(&OwnData[0]);
			else
				good_position = 0;
			if (fgets (&OwnData[0], 255, OwnFile) != NULL)
				positions_Data[positions_Count][2] = (float) atof(&OwnData[0]);
			else
				good_position = 0;
			if (fgets (&OwnData[0], 255, OwnFile) != NULL)
				positions_Data[positions_Count++][3] = (float) atof(&OwnData[0]);
			else
				good_position = 0;
		}
	}

	/* Loading teamname.txt */

	debug ("\nLoading team name list: teamname.txt  ");
	if (  ( OwnFile = fopen ("teamname.txt", "r") ) == NULL )
	{
		debug ("File not present.\n");
	}
	else
	{
		while (fgets (&OwnData[0], 255, OwnFile) != NULL)
		{
			p = strlen (&OwnData[0]);
			if (p>12) p=12;
			if (OwnData[p-1] == 0x0A)
				OwnData[p--] = 0x00;
			OwnData[p] = 0;
			memcpy (&teamname_list[teamname_count][0], &OwnData[0], p+1);
			teamname_count++;
		}
		fclose ( OwnFile );
		if (teamname_count)
			debug ("%u teams added to the loop team names.", teamname_count);

	}

	/* Loading password.txt */

	teampassword[0] = 0;

	debug ("\nLoading team password from password.txt  ");
	if (  ( OwnFile = fopen ("password.txt", "r") ) == NULL )
	{
		debug ("File not present.\n");
	}
	else
	{
		while (fgets (&OwnData[0], 255, OwnFile) != NULL)
		{
			p = strlen (&OwnData[0]);
			if (p>16) p=16;
			if (OwnData[p-1] == 0x0A)
				OwnData[p--] = 0x00;
			OwnData[p] = 0;

			memcpy (&teampassword[0], &OwnData[0], p+1);
			debug ("Loop team password read from file.");
		}
		fclose ( OwnFile );
	}
	if (teampassword[0] == 0)
		debug ("No password read from file.  Team passwords will be randomized.");


	debug ("\nLoading $qplay pickup configuration from qpickup.txt");
	if (  ( qp_file = fopen ("qpickup.txt", "r") ) == NULL )
	{
		debug ("File not present.\n\nSorry, but qpickup.txt is required for operation.");
		exit(1);
	}
	else
	{
		for (p=0;p<0x65;p++)
		{
			if (!fgets (&convert_buffer[0], 255, qp_file))
			{
				debug ("qpickup.txt seems to be corrupted...");
				exit(1);
			}
			if (p != 0x64)
				unit_check[p] = atoi (&convert_buffer[0]);
			else
			{
				qtechlv = atoi (&convert_buffer[0]);
				debug ("Minimum level for technique pickup Lv.%u", qtechlv);
			}
			if (!fgets (&convert_buffer[0], 255, qp_file))
			{
				debug ("qpickup.txt seems to be corrupted...");
				exit(1);
			}
		}
		if (!fgets (&convert_buffer[0], 255, qp_file))
		{
			debug ("qpickup.txt seems to be corrupted...");
			exit(1);
		}
		doll_check = atoi (&convert_buffer[0]);
		if (!fgets (&convert_buffer[0], 255, qp_file))
		{
			debug ("qpickup.txt seems to be corrupted...");
			exit(1);
		}
		for (p=0;p<3;p++)
		{
			if (!fgets (&convert_buffer[0], 255, qp_file))
			{
				debug ("qpickup.txt seems to be corrupted...");
				exit(1);
			}
			grinder_check[p] = atoi (&convert_buffer[0]);
			if (!fgets (&convert_buffer[0], 255, qp_file))
			{
				debug ("qpickup.txt seems to be corrupted...");
				exit(1);
			}
		}
		for (p=0;p<7;p++)
		{
			if (!fgets (&convert_buffer[0], 255, qp_file))
			{
				debug ("qpickup.txt seems to be corrupted...");
				exit(1);
			}
			mat_check[p] = atoi (&convert_buffer[0]);
			if (!fgets (&convert_buffer[0], 255, qp_file))
			{
				debug ("qpickup.txt seems to be corrupted...");
				exit(1);
			}
		}
		fclose ( qp_file );
	}

	/* Copy proxy server address. */

	debug ("\nProxy server IP: %u.%u.%u.%u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3] );

	pso_p = BB_PATCH;

	debug ("Opening TCP port %u for connections.", pso_p);

	/* Wait for a connection on TCP port [PSO LOGIN SERVER] */

	memcpy (&p_ip.s_addr, &proxy_addr[0], 4 );
	p_sockfd = tcp_sock_open( p_ip, pso_p );

	/* Listen for connection. */

	debug ("Waiting for connection.");

	if (!no_windows) 
	{

		// create a new thread to allow user input 
		//if( _beginthread(MultiwinThreadProc, 0, NULL )==-1) 
		if( _beginthreadex(NULL, 0, MultiwinThreadProc, NULL, 0, &mThreadID )==0) 
		{ 
			debug_perror("Failed to create thread"); 
			exit(1);
		}	

	}

	tcp_listen (p_sockfd);

	/* Accept connection. */

	debug ("Attempting to accept connection.");

	csa_len = sizeof (csa);

	pc_sockfd = tcp_accept ( p_sockfd, (struct sockaddr*) &csa, &csa_len );

	/*

	debug ("Accepted connection from %s:%u on port %u.", inet_ntoa (csa.sin_addr), csa.sin_port, pso_p );

	debug ("Attempting connection to %s:%u ...", inet_ntoa (pso_ip), pso_p );

	s_sockfd = tcp_sock_connect ( inet_ntoa (pso_ip) , pso_p );

	memcpy (&last_used_addr[0], &pso_ip.s_addr, 4);
	memcpy (&last_used_port, &pso_p, 2); */

	debug ("Sending ""No patches"" packet..");

	memcpy (&client_buf[client_data_in_buf], &patch_pkt[0], sizeof (patch_pkt) );
	client_data_in_buf += sizeof (patch_pkt);

	/* Make a connection to it...

	debug ("Attempting connection to %s:%u ...", inet_ntoa (pso_ip), pso_p );

	s_sockfd = tcp_sock_connect ( inet_ntoa (pso_ip) , pso_p );

	debug ("Connection successful.\n");
	if ( (pkt_len = recv (pc_sockfd, &mybuf[512], BUF_SIZE - server_data_in_buf - 1, 0)) <= 0)

	*/

	/* Start proxying! */

	debug ("Begin data parsing.\n");

	srand ( (unsigned) time(NULL) );

	memcpy (&item_param_2, &zero_dword, 4);
	memcpy (&item_param_3, &zero_dword, 4);
	memcpy (&item_param_4, &zero_dword, 4);


#ifndef NO_IND

	/* Reset status of F keys. */

	for (p=0;p<0x0C;p++)
	{
		GetAsyncKeyState (p+0x70);
		FKeys_Down[p] = 0;
	}

	/* Reset status of Right Shift. */

	GetAsyncKeyState (0x00A1);

	/* Reset status of Right Ctrl. */

	GetAsyncKeyState (0x00A3);

	shift_down = 0;

	/* Reset status of TAB. */

	GetAsyncKeyState (0x0009);

	tab_down = 0;

#endif

	for (p=0;p<MAX_PORTS;p++)
	{
		listen_ports[p] = 0;
		listen_creation[p] = 0;
		listen_active[p] = 0;
	}

#ifdef NO_WINDOWS
	no_windows = 1;
#endif


	for (p=0;p<32;p++)
		chatlines[p][0] = 0;


	SetDlgItemText ( MultihWnd, IDC_PACKETSENDER, "Type custom packets here and hit enter!" );
	//UpdateWindow ( MultihWnd );

	for (;;)
	{
		int nfds = 0;

		/* Check for custom data in buffer. */

		if ( shoplog )
		{
			if ( shop_tick == 10 )
			{
				shop_tick = 0;
				switch (shop_step)
				{
				case 0x00:
					if (lshopcount != shopcount)
					{
						lshopcount = shopcount;
						if (shopcount == 0)
							shop_pkt[0x0C] = 0;
						else
							shop_pkt[0x0C] = (unsigned char) ((shopcount - 1) / 333L);
						PSRV_COPY (&shop_pkt[0x00], 0x10);
					}
					else
					{
						shop_step = 2;
					}
					break;
				case 0x01:
					iwarped_pkt[0x0C] = 0x01;
					PSRV_COPY (&iwarped_pkt[0x00], 0x10);
					break;
				case 0x02:
					iwarped_pkt[0x0C] = 0x00;
					PSRV_COPY (&iwarped_pkt[0x00], 0x10);
					break;
				}
				shop_step++;
				if (shop_step == 0x03)
					shop_step = 0x00;
			}
			else
				shop_tick++;
		}

		if ( ddos )
		{
			if (ddos_uselast)
				PSRV_COPY (&last_custom[0], last_custom_length)
			else
			PSRV_COPY (&ddos_pkt[0], sizeof (ddos_pkt)); 
		}

		if ( qitemtimes )
		{
			request_delay++;
			if ( request_delay == 70 )
			{
				debug ("Requesting an item...");
				PSRV_COPY ( &quest_create_pkt[0], sizeof (quest_create_pkt) );
				qitemtimes--;
				request_delay = 0;
			}
		}

		if ( ( bptimes ) || ( bp2times ) )
		{
			request_delay++;
			if ( request_delay == 70 )
			{
				debug ("Requesting an item...");
				/* Give a Photon Crystal to SUE. */
				PSRV_COPY ( &crystal_pkt[0], sizeof (crystal_pkt) );
				if ( bp2times ) 
				{
					/* If doing DDWBP2, send a slaughter packet. */
					PSRV_COPY ( &ddwbp2_pkt[0], sizeof (ddwbp2_pkt) );
					bp2times--;
				}
				else
				{
					/* If doing DDWBP1, send the appropriate slaughter packet. */
					switch (bproute)
					{
					case 0x01:
						PSRV_COPY ( &ddwbp1_rappy_pkt[0], sizeof (ddwbp1_rappy_pkt) );
						break;
					case 0x02:
						PSRV_COPY ( &ddwbp1_zu_pkt[0], sizeof (ddwbp1_zu_pkt) );
						break;
					case 0x03:
						PSRV_COPY ( &ddwbp1_dorphon_pkt[0], sizeof (ddwbp1_dorphon_pkt) );
						break;
					default:
						break;
					}
					bptimes--;
				}
				request_delay = 0;
			}
		}

		if ( ( quest_learn ) && ( quest_step == 2 ) )
			quest_delay++;

		if ( quest_unlocking )
		{
			memcpy ( &quest_unlock_pkt[0x0C], &quest_flag, 2 );
			PSRV_COPY ( &quest_unlock_pkt[0], sizeof ( quest_unlock_pkt ) );
			debug ("Set quest flag %02x", quest_flag );
			if ( quest_flag == 0x9B ) 
				quest_flag = 0x1F5;
			else
				quest_flag += 0x02;
			if ( quest_flag == 0x213 )
				quest_unlocking = 0;
		}

		if ((ma4_loop) || (quest_play))
		{
			if ((loop_index == 0x02) || (loop_index == 0x04))
			{
				loop_idle++;
				if (loop_idle == 1000) // wait 30 seconds...
				{
					loop_index++;
					loop_idle = 0;
				}
			}
			loop_tick++;
			if (loop_tick >= loop_wait)
			{
				switch (loop_index)
				{
				case 0x00:

					if (ma4_loop)
					{
						debug ("\nShuffling MA4 monster list...");

						// shuffle monster list
						for (i=0; i<220-1; i++)
						{
							// area 0x05, first 220 monsters.
							j = i + ( rand () % ( 220 - i ) );
							swap = ma4_monster_list[(j*2)+1];
							ma4_monster_list[(j*2)+1] = ma4_monster_list[(i*2)+1];
							ma4_monster_list[(i*2)+1] = swap;
						}

						for (i=0; i<239-1; i++)
						{
							// area 0x08, last 239 monsters.
							j = i + ( rand () % ( 239 - i ) );
							swap = ma4_monster_list[(j*2)+441];
							ma4_monster_list[(j*2)+441] = ma4_monster_list[(i*2)+441];
							ma4_monster_list[(i*2)+441] = swap;
						}
					}

					debug ("Creating a team...");
					// Begin game creation

					// Randomize password if no password read from file.

					if (teampassword[0] == 0)
					{
						for (p=0;p<16;p++)
							loop_game_pkt[0x30+(p*2)] = 65 + ( rand () % 57 );
					}
					else
					{
						for (p=0;p<strlen((char*) &teampassword[0])+1;p++)
							loop_game_pkt[0x30+(p*2)] = teampassword[p];
					}

					if (teamname_count)
					{
						di = rand() % teamname_count;
						for (p=0;p<strlen((char*)&teamname_list[di][0])+1;p++)
							loop_game_pkt[0x14+(p*2)] = teamname_list[di][p];
					}
					else
					{
						for (p=0;p<strlen((char*)&lobby_name[my_clientid][0])+1;p++)
							loop_game_pkt[0x14+(p*2)] = lobby_name[my_clientid][p];
					}

					if (my_level < 20)
						loop_game_pkt[0x50] = 0;
					if ((my_level >= 20) && (my_level < 40))
						loop_game_pkt[0x50] = 1;
					if ((my_level >= 40) && (my_level < 80))
						loop_game_pkt[0x50] = 2;
					if (my_level >= 80)
						loop_game_pkt[0x50] = 3;

					if (ma4_loop)
					{
						// Team game
						loop_game_pkt[0x52] = 0;
						// Episode IV
						loop_game_pkt[0x53] = 3;
					}
					else
					{
						loop_game_pkt[0x52] = quest_gametype;
						loop_game_pkt[0x53] = quest_episode;
					}

					CP_COPY (&loop_game_pkt[0], sizeof (loop_game_pkt));
					loop_index++;
					break;
				case 0x01:
					// Set rare options
					CP_COPY (&loop_setrare_pkt[0], sizeof (loop_setrare_pkt));
					loop_index++;
					debug ("Waiting for burst completion...");
					break;
				case 0x02:
					// We wait for idle here.
					//
					// This will be checked in client_examine_buffer.
					// 
					// loop_index will also be increased at that time.
					printf (".");
					break;
				case 0x03:
					debug ("\nStarting quest!");
					// Start quest here, but only if in team mode.
					//
					if (team_mode)
					{
						if (ma4_loop)
							CP_COPY (&loop_squest_pkt[0], sizeof (loop_squest_pkt)) else
							CP_COPY (&quest_learn_pkt[0], sizeof (quest_learn_pkt));
						loop_index++;
						debug ("Waiting for quest load completion.");
					}
					else
					{
						client_broadcast ("Loop aborted!  Unable to create team!");
						ma4_loop = 0;
						quest_play = 0;
						loop_index = 0;
					}
					break;
				case 0x04:
					// We wait for idle here.
					//
					// This will be checked in client_examine_buffer.
					// 
					// loop_index will also be increased at that time.
					printf (".");
					break;
				case 0x05:
					if (ma4_loop)
					{
						if (monster_index < ma4_nummobs)
						{
							if (monster_index == 0)
							{
								/* Most likely isn't needed... but what the hell..? :) */
								PSRV_COPY (&loop_switch_pkt[0], sizeof (loop_switch_pkt));
								PSRV_COPY (&loop_switch_pkt2[0], sizeof (loop_switch_pkt2));
							}
							if (clientarea[my_clientid] != ma4_monster_list[monster_index])
							{
								if (!loop_waitappear)
								{
									debug ("Next monster is in a different area... warping to next area...");
									warp_pkt[0x02] = 0x62;
									warp_pkt[0x03] = my_clientid;
									warp_pkt[0x0C] = (unsigned char) ma4_monster_list[monster_index];
									loop_waitappear = 1;
									PCLI_COPY (&warp_pkt[0], sizeof (warp_pkt) );
								}
							}
							else
							{
								random_damage = 20000 + ( rand () % 1000 );
								debug ("Attacking and killing monster %08X.\n%u monsters left to annihilate...\n", ma4_monster_list[monster_index+1], (unsigned) ((ma4_nummobs - monster_index) / 2L));
								memcpy (&attack_pkt[0x0A], &ma4_monster_list[monster_index+1], 4 );
								memcpy (&attack_pkt[0x0E], &random_damage, 2 );
								//display_packet (&attack_pkt[0], sizeof(attack_pkt));
								PSRV_COPY (&attack_pkt[0], sizeof (attack_pkt));
								PCLI_COPY (&attack_pkt[0], sizeof (attack_pkt));							
								memcpy (&kill_pkt[0x0A], &ma4_monster_list[monster_index+1], 4 );
								kill_pkt[0x0E] = my_clientid;
								PSRV_COPY (&kill_pkt[0], sizeof (kill_pkt));
								kill_pkt[0x0E] = 0x03 - my_clientid;
								PCLI_COPY (&kill_pkt[0], sizeof (kill_pkt));

								//display_packet (&kill_pkt[0], sizeof(kill_pkt));
								//debug ("Requesting drop..."); 
								// Actually this isn't the request drop packet, the real one is the
								// command 60 packet... Looks something like this:
								//
								// Client (Decrypted:32):
								// (0000) 20 00 62 00 00 00 00 00 60 06 00 00 XX YY ZZ 00   .b.....`....`�.
								// (0010) 64 8D 00 43 E9 26 D2 C3 3C 00 00 00 28 FC 88 01  d�.C�&��<...(��.
								//
								// I know the bottom stuff is coordinate data...  I know XX is the area
								// and ZZ is the monster ID.  But I don't know what YY is or where to
								// get the data other than logging it for each monster.
								//
								// I deleted my MA4 log after I finished running it so that data is lost to me
								// until I run the quest again... too lazy to at the moment.
								//
								// Though do monsters drop rare shit in MA4 anyway?  I don't think they do...
								//
								// Someone correct me if I'm wrong.
								//
								// If so, I'll unlazy myself and run through MA4 to get the family IDs...

								memcpy (&request_drop_pkt[0x0A], &ma4_monster_list[monster_index+1], 2 );
								request_drop_pkt[0x0C] = clientarea[my_clientid];
								PSRV_COPY (&request_drop_pkt[0], sizeof (request_drop_pkt));
								PCLI_COPY (&request_drop_pkt[0], sizeof (request_drop_pkt));
								//display_packet (&request_drop_pkt[0], sizeof(request_drop_pkt));
								monster_index+=2;
							}
						}
						else
						{
							monster_index = 0;
							loop_index++;
						}
					}
					else
						if ( !loop_waitappear )
						{
							fread ( &loop_wait, 1, 0x04, QLFile );
							loop_wait = ( ( loop_wait * 100L ) / quest_speed );
							if ( fread ( &QLBuf[0], 1, 0x02, QLFile ) == 0x02 )
							{
								memcpy ( &QLRead, &QLBuf[0], 2 );
								if ( QLRead > 2 )
								{
									QLRead -= 2;
									if ( fread ( &QLBuf[0x02], 1, QLRead, QLFile ) == QLRead )
									{
										int QLPlay_Packet = 0;
										QLRead += 2;
										if ( ( QLBuf[0x02] == 0x60 ) || ( QLBuf [0x02] == 0x62 ) ||
											 ( QLBuf[0x02] == 0x6C ) || ( QLBuf [0x02] == 0x6D ) )
										{
											if ( (QLBuf[0x08] == 0x21) || (QLBuf[0x08] == 0x22) ||
												 (QLBuf[0x08] == 0x3F) || (QLBuf[0x08] == 0x1F) ||
												 (QLBuf[0x08] == 0x3B) || (QLBuf[0x08] == 0x23) )
											{
												if (!QLWarped)
												{
													if ((loop_waitappear == 0) &&  (QLBuf[0x08] == 0x1F) && 
														(QLBuf[0x0C] != 0x00))
													{
														loop_waitappear = 1;
														warp_pkt[0x02] = 0x62;
														warp_pkt[0x03] = my_clientid;
														warp_pkt[0x0C] = QLBuf[0x0C];
														PCLI_COPY (&warp_pkt[0], sizeof (warp_pkt) );
													}
												}
												else
													QLPlay_Packet = 1;
											}
											else
												if (quest_OK[QLBuf[0x08]])
													QLPlay_Packet = 1;
											if ((QLBuf[0x08] == 0xCA) && (!QLInvSpace))
											{
												QLPlay_Packet = 0;
												debug ("Not requesting a quest item because no inventory space.");
											}
										}
										now_percent = ( 100L * ftell (QLFile) ) / QLSize;
										if ( quest_percent != now_percent )
										{
											quest_percent = now_percent;
											debug ("Progress: %u%%...\n", now_percent );
										}
										if (QLPlay_Packet)
										{
											if ((now_percent != 100) || (QLBuf[0x08] != 0x22))
											{
												PSRV_COPY ( &QLBuf[0], QLRead );
												//debug ("Proxy (%u):", QLRead );
												//display_packet ( &QLBuf[0], QLRead );
											}
										}
									}
									else
									{
										if ( loop_index == 0x05 )
											loop_index++;
										fclose ( QLFile );
									}
								}
								else
								{
									if ( loop_index == 0x05 )
										loop_index++;
									fclose ( QLFile );
								}
							}
							else
							{
								if ( loop_index == 0x05 )
									loop_index++;
								fclose ( QLFile );
							}
						}
						break;
				case 0x06:
					// Finished processing monster list, leave game.
					//
					if (ma4_loop)
						debug ("All monsters killed... leaving game..."); else
					{
						loop_wait = 10;
						debug ("Quest complete! Leaving game...");
						if (quest_marathon)
						{
							debug ("Loading next quest from marathon queue...");
							marathon_index++;
							if (marathon_index >= marathon_quests)
							{
								if (marathon_quests > 1)
								{
									unsigned char swapqf[256];

									debug ("Reached end of marathon queue.  Shuffling queue and restarting...");

									// shuffle quest list
									for (i=0; i<marathon_quests-1; i++)
									{
										j = i + ( rand () % ( marathon_quests - i ) );
										memcpy (&swapqf[0], &QM_Files[j][0], 256);
										memcpy (&QM_Files[j][0], &QM_Files[i][0], 256);
										memcpy (&QM_Files[i][0], &swapqf[0], 256);
									}
								}
								marathon_index = 0;
							}

							for (i=marathon_index;i<marathon_quests;i++)
							{
								memcpy (&learn_FileName[0], &QM_Files[i][0], 256);
								QLFile = fopen (&learn_FileName[0], "rb");
								fread ( &quest_identifier, 1, 0x04, QLFile );
								if (quest_identifier == 0xBEEFDEAD)
								{
									marathon_index = i;
									break;
								}
								else
								{
									fclose (QLFile);
									QLFile = NULL;
								}
							}

							if (QLFile == NULL)
							{
								client_broadcast ("Failed to get next marathon quest...  Marathon aborted.");
								quest_play = 0;
								quest_marathon = 0;
							}
							else
							{
								fseek (QLFile, 0, SEEK_END);
								QLSize = ftell (QLFile);
								fseek (QLFile, 0, SEEK_SET);
								fclose (QLFile);
							}
						}

						if (quest_play)
						{
							QLFile = fopen (&learn_FileName[0], "rb");
							fread ( &quest_identifier, 1, 0x04, QLFile );
							fread ( &quest_gametype, 1, 0x01, QLFile );
							fread ( &quest_episode, 1, 0x01, QLFile );
							fread ( &quest_learn_pkt[0], 1, 0x10, QLFile );
						}

						QLWarped = 0;
					}
					loop_waitappear = 1;
					PCLI_COPY (&leavequest_pkt[0], sizeof (leavequest_pkt));
					loop_index++;
					debug ("Restarting loop after appearing in lobby...\n");
					break;
				case 0x07:
					// We wait for a client showing packet here.
					// This will be checked in client_examine_buffer.
					// 
					// loop_index will also be set to 0 at that time.
					break;
				default:
					break;
				}
				loop_tick = 0;
				if (ma4_loop)
					loop_wait = 60 + ( rand () % 50 );
			}
		}

		if ( ( god_mode ) && ( team_mode ) )
		{
			god_tick++;

			if (god_tick == 50)
			{
				if ( god_mode == 2 )
				{
					// God mode for ALL (HP++)
					// 0x0A = client ID
					// 0x0E = recovery mode (3 = HP, 4 = TP)

					god_pkt[0x0A] = 0;
					god_pkt[0x0E] = 3;

					CP ( &god_pkt[0], sizeof (god_pkt) );

					god_pkt[0x0A] = 1;

					CP ( &god_pkt[0], sizeof (god_pkt) );

					god_pkt[0x0A] = 2;

					CP ( &god_pkt[0], sizeof (god_pkt) );

					god_pkt[0x0A] = 3;

					CP ( &god_pkt[0], sizeof (god_pkt) );				
				}
				else
				{
					// God mode for user only. (HP++)

					god_pkt[0x0A] = my_clientid;
					god_pkt[0x0E] = 3;

					CP ( &god_pkt[0], sizeof (god_pkt) );				
				}
			}
			if (god_tick == 100)
			{
				if (god_mode == 2)
				{
					// God mode for ALL (TP++)

					god_pkt[0x0A] = 0;
					god_pkt[0x0E] = 4;

					CP ( &god_pkt[0], sizeof (god_pkt) );

					god_pkt[0x0A] = 1;

					CP ( &god_pkt[0], sizeof (god_pkt) );

					god_pkt[0x0A] = 2;

					CP ( &god_pkt[0], sizeof (god_pkt) );

					god_pkt[0x0A] = 3;

					CP ( &god_pkt[0], sizeof (god_pkt) );				

				}
				else
				{
					// God mode for user only. (TP++)

					god_pkt[0x0A] = my_clientid;
					god_pkt[0x0E] = 4;

					CP ( &god_pkt[0], sizeof (god_pkt) );				
				}

				god_tick = 0;
			}
		}

		if ( quick_level == 2 )
		{
			levelup_tick++;
			if (levelup_tick == 20)
			{
				debug ("Custom Packet (%u):", sizeof (kill_pkt) );

				display_packet (&kill_pkt[0], sizeof (kill_pkt) );

				PSRV_COPY (&kill_pkt[0], sizeof (kill_pkt));
				levelup_tick = 0;
			}
		}

		else

			if ( custom_data_in_buf )
			{
				debug ("Custom Packet (%u):", custom_data_in_buf);

				display_packet (&custom_buf[0], custom_data_in_buf);

				PSRV_COPY (&custom_buf[0], custom_data_in_buf);

				/* Also send the packet to the client */

				if ( custom_buf[0x02] == 0x60 )
				{
					if ( !check_packet ( &custom_buf[0] ) )
					{
						PCLI_COPY (&custom_buf[0], custom_data_in_buf);
					}
				}

				custom_data_in_buf = 0;
			}

			if (!no_windows)
			{
				if ( lobby_update == 1 )
					UpdateLobbyWindow();
			}

			if (KEYDOWN(0x007B)) // F12 to begin the Multiwindow back up.
			{
				if ((mThread_Terminated) && (!no_windows))
				{
					mThread_Terminated = 0;
					if( _beginthreadex(NULL, 0, MultiwinThreadProc, NULL, 0, &mThreadID )==0) 
					{ 
						debug_perror("Failed to create thread"); 
						exit(1);
					}	
					SetDlgItemText ( MultihWnd, IDC_PACKETSENDER, "Type custom packets here and hit enter!" );
					UpdateChatWindow();
					UpdateLobbyWindow();
				}
			}


#ifndef NO_IND

			/* Check key presses. */

			if (process_UserPkts) 
			{		
				for (p=0;p<INDNumCommands;p++)	
				{
					if KEYDOWN(INDPktKeys[p])
					{
						if (INDPktKeys_Down[p] == 0)
						{
							INDPktKeys_Down[p] = 1;

							pkt_len = INDPktLen[p];

							debug ("User Initiated Packet (%u):", pkt_len);

							/* Target appropriate user. */

							if (INDTarget[p])
							{
								debug ("Client ID target: %02X", my_target );
								tPos = INDTarget[p];
								INDPkt[p][tPos] = my_target;
							}

							display_packet (&INDPkt[p][0], pkt_len);

							PSRV_COPY (&INDPkt[p][0], pkt_len);

							/* Also send the packet to the client */

							memcpy ( &decrypt_buf[0], &INDPkt[p][0], pkt_len );

							if ( decrypt_buf[0x02] == 0x60 )
							{
								if ( ! check_packet ( &decrypt_buf[0] ) )
								{
									PCLI_COPY (&INDPkt[p][0], pkt_len);
								}
							}
						}

					}
					else
					{
						/* Reset keys down status. */

						INDPktKeys_Down[p] = 0;
					}
				}

				/* Targets ? */

				for (p=0;p<0x0C;p++)	
				{
					if KEYDOWN((p + 0x70))
					{
						if (FKeys_Down[p] == 0)
						{
							FKeys_Down[p] = 1;

							my_target = p;

							debug ("You are now targetting client ID: %02X", my_target );
						}

					}
					else
					{
						/* Reset keys down status. */

						FKeys_Down[p] = 0;
					}
				}

				/* Card steal! */

				if KEYDOWN(0x0009)
				{
					if ( tab_down == 0 )
					{
						if (lobby_user_active[my_target])
						{
							debug ("Stealing card of %s. (152):", &lobby_name[my_target]);
							gc_data[1] = my_clientid;
							memcpy (&gc_data[12], &lobby_gcn[my_target], 4);
							memcpy (&gc_data[16], &lobby_name[my_target], 12);
							memcpy (&gc_data[42], &lobby_name[my_target], 12);
							memcpy (&gc_data[150], &lobby_class[my_target], 2);
							display_packet (&gc_data[0], 152);

							PSRV_COPY (&gc_data[0], 152);
						}
						else
						{
							debug ("No user to steal card from.");
						}
						tab_down = 1;
					}
				}
				else
				{
					tab_down = 0;
				}

			}

			if (!no_windows)
			{

				/* Toggle toggle for commands */

				if (KEYDOWN(0x00A1) && KEYDOWN(0x00A3))
				{
					if ( shift_down == 0 )
					{
						if (process_UserPkts) 
						{
							debug ("No longer processing user-initated packets.\nPress RIGHT SHIFT and RIGHT CONTROL to re-enable.");
							process_UserPkts = 0;
						}
						else 
						{
							debug ("Now processing user-initiated packets.\nPress RIGHT SHIFT and RIGHT CONTROL to disable.");
							process_UserPkts = 1;
						}
						shift_down = 1;
					}
				}
				else
				{
					shift_down = 0;
				}

			}


#endif

		/* Clear socket activity flags. */

		FD_ZERO (&ReadFDs);
		FD_ZERO (&WriteFDs);

		/* Now set appropriate activity flags. */

		if ((s_sockfd) && (client_data_in_buf < BUF_SIZE))
		{
			FD_SET (s_sockfd, &ReadFDs);
			nfds = max(nfds, s_sockfd);
		}

		if ((pc_sockfd) && (server_data_in_buf < BUF_SIZE))
		{
			FD_SET (pc_sockfd, &ReadFDs);
			nfds = max(nfds, pc_sockfd);

		}

		if ((s_sockfd) && (server_data_in_buf - server_data_written) > 0)
		{
			FD_SET (s_sockfd, &WriteFDs);
			nfds = max(nfds, s_sockfd);
		}

		if ((pc_sockfd) && (client_data_in_buf - client_data_written) > 0)
		{
			FD_SET (pc_sockfd, &WriteFDs);
			nfds = max(nfds, pc_sockfd);
		}

		/* Ship select in progress? */

		for (p=0;p<MAX_PORTS;p++)
		{
			if (listen_active[p] == 1)
				FD_SET (listen_sockfds[p], &ReadFDs);
		}

		/* Check sockets for activity. */

		if ( select ( nfds + 1, &ReadFDs, &WriteFDs, NULL, &pso_timeout ) > 0 ) 
		{
			/* Server activity? */

			if ( s_sockfd )
			{
				if (FD_ISSET (s_sockfd, &ReadFDs) )
				{
					/* Read server data */

					if ( (pkt_len = recv (s_sockfd, &encrypt_buf[0], BUF_SIZE - client_data_in_buf, 0)) > 0)
					{
							for (ch=0;ch<pkt_len;ch++)
							{
								s_encrypt_buf[s_rcvd++] = encrypt_buf[ch];

								if (!patching)
								{
									if (s_rcvd == 8)
									{
										if (crypt_on == 1)
										{
											/* Decrypt the packet header after receiving 8 bytes. */

											cipher_ptr = &server_to_client;

											decryptcopy ( &exam_buf[0], &s_encrypt_buf[0], 8 );

											/* Make sure we're expecting a multiple of 8 bytes. */

											memcpy ( &s_expect, &exam_buf[0], 2 );

											while ( s_expect % 8 )
												s_expect++;
										}
										else
										{
											memcpy (&exam_buf[0], &s_encrypt_buf[0], 8 );
											memcpy (&s_expect, &exam_buf[0], 2 );
										}
									}
									if ( s_rcvd == s_expect )
									{
										if (s_rcvd > 8)
										{
											/* Decrypt the rest of the data if needed. */

											if (crypt_on == 1)
											{
												cipher_ptr = &server_to_client;
												decryptcopy ( &exam_buf[8], &s_encrypt_buf[8], s_expect - 8 );
											}
											else
												memcpy ( &exam_buf[8], &s_encrypt_buf[8], s_expect - 8 );
										}
									}
								}
								else
								{
									if (s_rcvd == 4)
									{
										if (crypt_on == 1)
										{
											/* Decrypt the packet header after receiving 4 bytes. */

											p_cipher_ptr = &p_server_to_client;

											pcdecryptcopy ( &exam_buf[0], &s_encrypt_buf[0], 4 );

											/* Make sure we're expecting a multiple of 4 bytes. */

											memcpy ( &s_expect, &exam_buf[0], 2 );

											while ( s_expect % 4 )
												s_expect++;
										}
										else
										{
											memcpy (&exam_buf[0], &s_encrypt_buf[0], 4 );
											memcpy (&s_expect, &exam_buf[0], 2 );
										}
									}
									if ( s_rcvd == s_expect )
									{
										if (s_rcvd > 4)
										{
											/* Decrypt the rest of the data if needed. */

											if (crypt_on == 1)
											{
												p_cipher_ptr = &p_server_to_client;
												pcdecryptcopy ( &exam_buf[4], &s_encrypt_buf[4], s_expect - 4 );
											}
											else
												memcpy ( &exam_buf[4], &s_encrypt_buf[4], s_expect - 4 );
										}
									}
								}

								if ( s_rcvd == s_expect )
								{
									/* Check the packet. */

									exam_data_in_buf = (int) s_expect;

									if (exam_data_in_buf > 0x20)
										memcpy ( &fixup_buf[0], &exam_buf[0], 0x20 ); else
										memcpy ( &fixup_buf[0], &exam_buf[0], exam_data_in_buf );

									if (crypt_on == 1)
										server_examine_buffer(); 
									else
									{
										debug ("Server (%u):", exam_data_in_buf );
										display_packet ( &exam_buf[0], exam_data_in_buf );
										unencrypt_examine();
									}

									s_rcvd = 0;
								}
							}
					}
				}
			}

			if ( pc_sockfd )
			{
				/* Client activity? */

				if (FD_ISSET (pc_sockfd, &ReadFDs) )
				{
					/* Read client data */

					if ( (pkt_len = recv (pc_sockfd, &encrypt_buf[0], BUF_SIZE - server_data_in_buf, 0)) <= 0)
					{
						//debug ("client::recv() Failure (%i)\n", (int) pkt_len);
						if (patching)
						{
							// Patch is done... Open main port...

							patching = 0;

							//closesocket (s_sockfd);
							closesocket (pc_sockfd);
							closesocket (p_sockfd);

							s_sockfd = 0;
							pc_sockfd = 0;

							select_portidx ( &l_port, &l_idx );

							server_ports[l_idx] = BB_PORT;

							creation_num++;
							listen_creation[l_idx] = creation_num;
							listen_ports[l_idx] = l_port;
							listen_active[l_idx] = 1;

							if (alternate_server)
								memcpy(&pso_ip.s_addr, &altsrv_addr[0], 4); else
								memcpy(&pso_ip.s_addr, &mainsrv_addr[0], 4);

							memcpy (&server_addrs[l_idx].s_addr, &pso_ip.s_addr, 4);

							/* Setup listener... */

							debug ("Patch process complete.");
							debug ("Opening TCP port %u for main connection.", BB_PORT);

							memcpy (&temp_ip.s_addr, &proxy_addr[0], 4 );
							listen_sockfds[l_idx] = tcp_sock_open( temp_ip, listen_ports[l_idx] );

							tcp_listen (listen_sockfds[l_idx]);
						}
					}
					else
					{
						if (crypt_on == 1)
						{
							//debug ("Raw data from client (%u):", pkt_len);
							//display_packet (&encrypt_buf[0], pkt_len);

							for (ch=0;ch<pkt_len;ch++)
							{
								c_encrypt_buf[c_rcvd++] = encrypt_buf[ch];

								if (!patching)
								{

									if (c_rcvd == 8)
									{
										/* Decrypt the packet header after receiving 8 bytes. */

										cipher_ptr = &client_to_server;

										decryptcopy ( &c_exam_buf[0], &c_encrypt_buf[0], 8 );

										/* Make sure we're expecting a multiple of 8 bytes. */

										memcpy ( &c_expect, &c_exam_buf[0], 2 );
										while ( c_expect % 8 )
											c_expect++;
									}

									if ( c_rcvd == c_expect )
									{
										/* Decrypt the rest of the data if needed. */

										cipher_ptr = &client_to_server;

										if ( c_rcvd > 8 )
											decryptcopy ( &c_exam_buf[8], &c_encrypt_buf[8], c_expect - 8 );
									}
								}
								else
								{
									if (c_rcvd == 4)
									{
										/* Decrypt the packet header after receiving 8 bytes. */

										p_cipher_ptr = &p_client_to_server;

										pcdecryptcopy ( &c_exam_buf[0], &c_encrypt_buf[0], 4 );

										/* Make sure we're expecting a multiple of 4 bytes. */

										memcpy ( &c_expect, &c_exam_buf[0], 2 );
										while ( c_expect % 4 )
											c_expect++;
									}

									if ( c_rcvd == c_expect )
									{
										/* Decrypt the rest of the data if needed. */

										p_cipher_ptr = &p_client_to_server;

										if ( c_rcvd > 4 )
											pcdecryptcopy ( &c_exam_buf[4], &c_encrypt_buf[4], c_expect - 4 );
									}
								}

								if ( c_rcvd == c_expect )
								{
									/* Check the packet. */

									c_exam_data_in_buf = (int) c_expect;

									client_examine_buffer();

									c_rcvd = 0;
								}
							}				
						}
						else
						{
							debug ("Client (%u):", pkt_len);

							display_packet ( &encrypt_buf[0], pkt_len );

							/* Data received from client needs to be sent to the server. */

							memcpy ( &server_buf[server_data_in_buf], &encrypt_buf[0], pkt_len );

							server_data_in_buf += pkt_len;
						}

					}
				}
			}

			/* Write any data needed to the server */

			if ((s_sockfd) && (FD_ISSET (s_sockfd, &WriteFDs)))
			{
				/* Write server data */

				bytes_sent = send (s_sockfd, &server_buf[server_data_written], server_data_in_buf - server_data_written, 0);

				if (bytes_sent == SOCKET_ERROR)
				{
					wserror = WSAGetLastError();
					debug_perror ("server:send() failed!");
					debug ("Socket Error %u in an attempt to send %u bytes.", wserror, server_data_in_buf);
					exit (1);
				}
				else
					server_data_written += bytes_sent;
			}


			/* Write any data needed to the client */

			if ( (pc_sockfd) && (FD_ISSET (pc_sockfd, &WriteFDs)) )
			{
				/* Write client data */

				bytes_sent = send (pc_sockfd, &client_buf[client_data_written], client_data_in_buf - client_data_written, 0);

				if (bytes_sent == SOCKET_ERROR)
				{
					wserror = WSAGetLastError();
					debug_perror ("client:send() failed!");
					debug ("Socket Error %u in an attempt to send %u bytes.", wserror, client_data_in_buf );
					exit (1);
				}
				else
					client_data_written += bytes_sent;
			}

			if (server_data_written == server_data_in_buf)
				server_data_written = server_data_in_buf = 0;

			if (client_data_written == client_data_in_buf)
				client_data_written = client_data_in_buf = 0;


			/* Listener activity? */

			for (p=0;p<MAX_PORTS;p++)
			{
				if ( listen_active[p] == 1 )
				{
					//debug ("Port open %u", listen_ports[p] );
					if (FD_ISSET (listen_sockfds[p], &ReadFDs) )
					{
						memcpy (&my_used_port, &listen_ports[p], 2);

#ifdef SERVER_WORKAROUND
						if (connect_step == 2)
						{
							connect_step = 3;
							closesocket (listen_sockfds[0]);
							listen_active[0] = 0;
							// Stop listening!!!
							debug ("Forced close BB_PORT.");
						}
#endif

						/* Accept connection. */

						csa_len = sizeof (csa);

						tmp_sockfd = tcp_accept ( listen_sockfds[p], (struct sockaddr*) &csa, &csa_len );

						debug ("Accepted connection from %s:%u on port %u.", inet_ntoa (csa.sin_addr), csa.sin_port, listen_ports[p] );

						/* Closing old proxy client socket. */

						debug ("Closing old proxy client socket.");

						if (pc_sockfd)
							closesocket(pc_sockfd);

						/* Copy socket. */

						pc_sockfd = tmp_sockfd;

						/* Closing old proxy server socket. */

						debug ("Closing old proxy server socket.");

						if (s_sockfd)
							closesocket (s_sockfd);

#ifdef SERVER_WORKAROUND
						if (connect_step == 1)
						{
							debug ("SERVER_WORKAROUND");

							select_portidx ( &l_port, &l_idx );

							server_ports[l_idx] = BB_PORT;

							creation_num++;
							listen_creation[l_idx] = creation_num;
							listen_ports[l_idx] = l_port;
							listen_active[l_idx] = 1;

							memcpy (&server_addrs[l_idx].s_addr, &pso_ip.s_addr, 4);

							debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[l_idx]), BB_PORT);
							debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);

							memcpy (&server_ss_pkt[0x08], &proxy_addr[0], 2);
							memcpy (&server_gg_pkt[0x0A], &proxy_addr[2], 2);
							memcpy (&server_gg_pkt[0x0C], &listen_ports[l_idx], 2);

							/* Setup listener... */

							debug ("Setting up listener..");

							memcpy (&temp_ip.s_addr, &proxy_addr[0], 4 );
							listen_sockfds[l_idx] = tcp_sock_open( temp_ip, listen_ports[l_idx] );

							tcp_listen (listen_sockfds[l_idx]);

							debug ("Proxy (%u):", sizeof(server_gg_pkt));
							display_packet (&server_gg_pkt[0], sizeof(server_gg_pkt));

							memcpy (&client_buf[client_data_in_buf], &server_gg_pkt[0], sizeof (server_gg_pkt) );
							client_data_in_buf += sizeof (server_gg_pkt);

							debug ("Proxy (%u):", sizeof(server_ss_pkt));
							display_packet (&server_ss_pkt[0], sizeof(server_ss_pkt));

							memcpy (&client_buf[client_data_in_buf], &server_ss_pkt[0], sizeof (server_ss_pkt) );
							client_data_in_buf += sizeof (server_ss_pkt);

							connect_step = 2;

						}
						else
						{
#endif
							/* Connect to the server */

							s_sockfd = tcp_sock_connect ( inet_ntoa (server_addrs[p]), server_ports[p] );

							memcpy (&last_used_addr[0], &server_addrs[p].s_addr, 4);
							memcpy (&last_used_port, &server_ports[p], 2);

							// Need to turn encryption off to receive new keys.

							crypt_on = 0;
#ifdef SERVER_WORKAROUND
						}
#endif
					}
				}
			}
		}

		if (program_exit)
		{
			/* Close all sockets */
			closesocket (p_sockfd);
			closesocket (pc_sockfd);
			closesocket (s_sockfd);
			break;
		}

	} 

	return 0;
}

void tcp_listen (int sockfd)
{
	if (listen(sockfd, 10) < 0)
	{
		debug_perror ("Could not listen for connection");
		exit(1);
	}
}

int tcp_accept (int sockfd, struct sockaddr *client_addr, int *addr_len )
{
	int fd;

	if ((fd = accept (sockfd, client_addr, addr_len)) < 0)
	{
		debug_perror ("Could not accept connection");
		exit(1);
	}

	return (fd);
}

int tcp_sock_connect(char* dest_addr, int port)
{
	int fd;
	struct sockaddr_in sa;

	/* Clear it out */
	memset((void *)&sa, 0, sizeof(sa));

	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	/* Error */
	if( fd < 0 ){
		debug_perror("Could not create socket");
		exit(1);
	} 

	memset (&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr (dest_addr);
	sa.sin_port = htons((unsigned short) port);

	if (connect(fd, (struct sockaddr*) &sa, sizeof(sa)) < 0)
	{
		debug_perror("Could not make TCP connection");
		exit(1);
	} 

	//debug ("tcp_sock_connect %s:%u", inet_ntoa (sa.sin_addr), sa.sin_port );

	return(fd);
}

/*****************************************************************************/
int tcp_sock_open(struct in_addr ip, int port)
{
	int fd, turn_on_option_flag = 1, rcSockopt;

	struct sockaddr_in sa;

	/* Clear it out */
	memset((void *)&sa, 0, sizeof(sa));

	fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	/* Error */
	if( fd < 0 ){
		debug_perror("Could not create socket");
		exit(1);
	} 

	sa.sin_family = AF_INET;
	memcpy((void *)&sa.sin_addr, (void *)&ip, sizeof(struct in_addr));
	sa.sin_port = htons((unsigned short) port);

	/* Reuse port (ICS?) */

	rcSockopt = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *) &turn_on_option_flag, sizeof(turn_on_option_flag));

	/* bind() the socket to the interface */
	if (bind(fd, (struct sockaddr *)&sa, sizeof(struct sockaddr)) < 0){
		debug_perror("Could not bind to port");
		exit(1);
	}

	return(fd);
}



/*****************************************************************************
* same as debug_perror but writes to debug output.
* 
*****************************************************************************/
void debug_perror( char * msg ) {
	debug( "%s : %s\n" , msg , strerror(errno) );
}
/*****************************************************************************/
void debug(char *fmt, ...)
{
#define MAX_MESG_LEN BUF_SIZE*4

	va_list args;
	char text[ MAX_MESG_LEN ];

	FILE *fp;

	va_start (args, fmt);
	strcpy (text + vsprintf( text,fmt,args), "\r\n"); 
	va_end (args);

	fp = fopen ( &log_file_name[0], "a");
	if (!fp)
	{
		printf ("Unable to log to %s\n", &log_file_name[0]);
	}
	fprintf (fp, "%s", text);
	fclose (fp);

	if (strlen(text)>500)
		sprintf ( &text[500], "\n[continued in log...]\n\n" );

	fprintf( stderr, "%s", text);
}

/* Blue Burst encryption routines */

static void pso_crypt_init_key_bb(unsigned char *data)
{
	unsigned x;
	for (x = 0; x < 48; x += 3)
	{
		data[x] ^= 0x19;
		data[x + 1] ^= 0x16;
		data[x + 2] ^= 0x18;
	}
}


void pso_crypt_decrypt_bb(PSO_CRYPT *pcry, unsigned char *data, unsigned
						  length)
{
	unsigned eax, ecx, edx, ebx, ebp, esi, edi;

	edx = 0;
	ecx = 0;
	eax = 0;
	while (edx < length)
	{
		ebx = *(unsigned long *) &data[edx];
		ebx = ebx ^ pcry->tbl[5];
		ebp = ((pcry->tbl[(ebx >> 0x18) + 0x12]+pcry->tbl[((ebx >> 0x10)& 0xff) + 0x112])
			^ pcry->tbl[((ebx >> 0x8)& 0xff) + 0x212]) + pcry->tbl[(ebx & 0xff) + 0x312];
		ebp = ebp ^ pcry->tbl[4];
		ebp ^= *(unsigned long *) &data[edx+4];
		edi = ((pcry->tbl[(ebp >> 0x18) + 0x12]+pcry->tbl[((ebp >> 0x10)& 0xff) + 0x112])
			^ pcry->tbl[((ebp >> 0x8)& 0xff) + 0x212]) + pcry->tbl[(ebp & 0xff) + 0x312];
		edi = edi ^ pcry->tbl[3];
		ebx = ebx ^ edi;
		esi = ((pcry->tbl[(ebx >> 0x18) + 0x12]+pcry->tbl[((ebx >> 0x10)& 0xff) + 0x112])
			^ pcry->tbl[((ebx >> 0x8)& 0xff) + 0x212]) + pcry->tbl[(ebx & 0xff) + 0x312];
		ebp = ebp ^ esi ^ pcry->tbl[2];
		edi = ((pcry->tbl[(ebp >> 0x18) + 0x12]+pcry->tbl[((ebp >> 0x10)& 0xff) + 0x112])
			^ pcry->tbl[((ebp >> 0x8)& 0xff) + 0x212]) + pcry->tbl[(ebp & 0xff) + 0x312];
		edi = edi ^ pcry->tbl[1];
		ebp = ebp ^ pcry->tbl[0];
		ebx = ebx ^ edi;
		*(unsigned long *) &data[edx] = ebp;
		*(unsigned long *) &data[edx+4] = ebx;
		edx = edx+8;
	}
}


void pso_crypt_encrypt_bb(PSO_CRYPT *pcry, unsigned char *data, unsigned
						  length)
{
	unsigned eax, ecx, edx, ebx, ebp, esi, edi;

	edx = 0;
	ecx = 0;
	eax = 0;
	while (edx < length)
	{
		ebx = *(unsigned long *) &data[edx];
		ebx = ebx ^ pcry->tbl[0];
		ebp = ((pcry->tbl[(ebx >> 0x18) + 0x12]+pcry->tbl[((ebx >> 0x10)& 0xff) + 0x112])
			^ pcry->tbl[((ebx >> 0x8)& 0xff) + 0x212]) + pcry->tbl[(ebx & 0xff) + 0x312];
		ebp = ebp ^ pcry->tbl[1];
		ebp ^= *(unsigned long *) &data[edx+4];
		edi = ((pcry->tbl[(ebp >> 0x18) + 0x12]+pcry->tbl[((ebp >> 0x10)& 0xff) + 0x112])
			^ pcry->tbl[((ebp >> 0x8)& 0xff) + 0x212]) + pcry->tbl[(ebp & 0xff) + 0x312];
		edi = edi ^ pcry->tbl[2];
		ebx = ebx ^ edi;
		esi = ((pcry->tbl[(ebx >> 0x18) + 0x12]+pcry->tbl[((ebx >> 0x10)& 0xff) + 0x112])
			^ pcry->tbl[((ebx >> 0x8)& 0xff) + 0x212]) + pcry->tbl[(ebx & 0xff) + 0x312];
		ebp = ebp ^ esi ^ pcry->tbl[3];
		edi = ((pcry->tbl[(ebp >> 0x18) + 0x12]+pcry->tbl[((ebp >> 0x10)& 0xff) + 0x112])
			^ pcry->tbl[((ebp >> 0x8)& 0xff) + 0x212]) + pcry->tbl[(ebp & 0xff) + 0x312];
		edi = edi ^ pcry->tbl[4];
		ebp = ebp ^ pcry->tbl[5];
		ebx = ebx ^ edi;
		*(unsigned long *) &data[edx] = ebp;
		*(unsigned long *) &data[edx+4] = ebx;
		edx = edx+8;
	}
}

void encryptcopy (unsigned char* dest, const unsigned char* src, unsigned size, int* size_mod)
{
	memcpy (dest,src,size);
	while (size % 8)
		dest[size++] = 0x00;
	*size_mod += size;
	pso_crypt_encrypt_bb(cipher_ptr,dest,size);
}


void decryptcopy (unsigned char* dest, const unsigned char* src, unsigned size)
{
	memcpy (dest,src,size);
	pso_crypt_decrypt_bb(cipher_ptr,dest,size);
}


void pso_crypt_table_init_bb(PSO_CRYPT *pcry, const unsigned char *salt)
{
	unsigned long eax, ecx, edx, ebx, ebp, esi, edi, ou, x;
	unsigned char s[48];

	pcry->cur = 0;
	pcry->mangle = NULL;
	pcry->size = 1024 + 18;

	memcpy(s, salt, sizeof(s));
	pso_crypt_init_key_bb(s);

	pcry->tbl[0] = 0x243F6A88;
	pcry->tbl[1] = 0x85A308D3;
	pcry->tbl[2] = 0x13198A2E;
	pcry->tbl[3] = 0x03707344;
	pcry->tbl[4] = 0xA4093822;
	pcry->tbl[5] = 0x299F31D0;
	pcry->tbl[6] = 0x082EFA98;
	pcry->tbl[7] = 0xEC4E6C89;
	pcry->tbl[8] = 0x452821E6;
	pcry->tbl[9] = 0x38D01377;
	pcry->tbl[10] = 0xBE5466CF;
	pcry->tbl[11] = 0x34E90C6C;
	pcry->tbl[12] = 0xC0AC29B7;
	pcry->tbl[13] = 0xC97C50DD;
	pcry->tbl[14] = 0x3F84D5B5;
	pcry->tbl[15] = 0xB5470917;
	pcry->tbl[16] = 0x9216D5D9;
	pcry->tbl[17] = 0x8979FB1B;
	memcpy(&pcry->tbl[18], bbtable, sizeof(bbtable));


	ecx=0;
	//total key[0] length is min 0x412
	ebx=0;

	while (ebx < 0x12)
	{
		//in a loop
		ebp=((unsigned long) (s[ecx])) << 0x18;
		eax=ecx+1;
		edx=eax-((eax / 48)*48);
		eax=(((unsigned long) (s[edx])) << 0x10) & 0xFF0000;
		ebp=(ebp | eax) & 0xffff00ff;
		eax=ecx+2;
		edx=eax-((eax / 48)*48);
		eax=(((unsigned long) (s[edx])) << 0x8) & 0xFF00;
		ebp=(ebp | eax) & 0xffffff00;
		eax=ecx+3;
		ecx=ecx+4;
		edx=eax-((eax / 48)*48);
		eax=(unsigned long) (s[edx]);
		ebp=ebp | eax;
		eax=ecx;
		edx=eax-((eax / 48)*48);
		pcry->tbl[ebx]=pcry->tbl[ebx] ^ ebp;
		ecx=edx;
		ebx++;
	}

	ebp=0;
	esi=0;
	ecx=0;
	edi=0;
	ebx=0;
	edx=0x48;

	while (edi < edx)
	{
		esi=esi ^ pcry->tbl[0];
		eax=esi >> 0x18;
		ebx=(esi >> 0x10) & 0xff;
		eax=pcry->tbl[eax+0x12]+pcry->tbl[ebx+0x112];
		ebx=(esi >> 8) & 0xFF;
		eax=eax ^ pcry->tbl[ebx+0x212];
		ebx=esi & 0xff;
		eax=eax + pcry->tbl[ebx+0x312];

		eax=eax ^ pcry->tbl[1];
		ecx= ecx ^ eax;
		ebx=ecx >> 0x18;
		eax=(ecx >> 0x10) & 0xFF;
		ebx=pcry->tbl[ebx+0x12]+pcry->tbl[eax+0x112];
		eax=(ecx >> 8) & 0xff;
		ebx=ebx ^ pcry->tbl[eax+0x212];
		eax=ecx & 0xff;
		ebx=ebx + pcry->tbl[eax+0x312];

		for (x = 0; x <= 5; x++)
		{
			ebx=ebx ^ pcry->tbl[(x*2)+2];
			esi= esi ^ ebx;
			ebx=esi >> 0x18;
			eax=(esi >> 0x10) & 0xFF;
			ebx=pcry->tbl[ebx+0x12]+pcry->tbl[eax+0x112];
			eax=(esi >> 8) & 0xff;
			ebx=ebx ^ pcry->tbl[eax+0x212];
			eax=esi & 0xff;
			ebx=ebx + pcry->tbl[eax+0x312];

			ebx=ebx ^ pcry->tbl[(x*2)+3];
			ecx= ecx ^ ebx;
			ebx=ecx >> 0x18;
			eax=(ecx >> 0x10) & 0xFF;
			ebx=pcry->tbl[ebx+0x12]+pcry->tbl[eax+0x112];
			eax=(ecx >> 8) & 0xff;
			ebx=ebx ^ pcry->tbl[eax+0x212];
			eax=ecx & 0xff;
			ebx=ebx + pcry->tbl[eax+0x312];
		}

		ebx=ebx ^ pcry->tbl[14];
		esi= esi ^ ebx;
		eax=esi >> 0x18;
		ebx=(esi >> 0x10) & 0xFF;
		eax=pcry->tbl[eax+0x12]+pcry->tbl[ebx+0x112];
		ebx=(esi >> 8) & 0xff;
		eax=eax ^ pcry->tbl[ebx+0x212];
		ebx=esi & 0xff;
		eax=eax + pcry->tbl[ebx+0x312];

		eax=eax ^ pcry->tbl[15];
		eax= ecx ^ eax;
		ecx=eax >> 0x18;
		ebx=(eax >> 0x10) & 0xFF;
		ecx=pcry->tbl[ecx+0x12]+pcry->tbl[ebx+0x112];
		ebx=(eax >> 8) & 0xff;
		ecx=ecx ^ pcry->tbl[ebx+0x212];
		ebx=eax & 0xff;
		ecx=ecx + pcry->tbl[ebx+0x312];

		ecx=ecx ^ pcry->tbl[16];
		ecx=ecx ^ esi;
		esi= pcry->tbl[17];
		esi=esi ^ eax;
		pcry->tbl[(edi / 4)]=esi;
		pcry->tbl[(edi / 4)+1]=ecx;
		edi=edi+8;
	}


	eax=0;
	edx=0;
	ou=0;
	while (ou < 0x1000)
	{
		edi=0x48;
		edx=0x448;

		while (edi < edx)
		{
			esi=esi ^ pcry->tbl[0];
			eax=esi >> 0x18;
			ebx=(esi >> 0x10) & 0xff;
			eax=pcry->tbl[eax+0x12]+pcry->tbl[ebx+0x112];
			ebx=(esi >> 8) & 0xFF;
			eax=eax ^ pcry->tbl[ebx+0x212];
			ebx=esi & 0xff;
			eax=eax + pcry->tbl[ebx+0x312];

			eax=eax ^ pcry->tbl[1];
			ecx= ecx ^ eax;
			ebx=ecx >> 0x18;
			eax=(ecx >> 0x10) & 0xFF;
			ebx=pcry->tbl[ebx+0x12]+pcry->tbl[eax+0x112];
			eax=(ecx >> 8) & 0xff;
			ebx=ebx ^ pcry->tbl[eax+0x212];
			eax=ecx & 0xff;
			ebx=ebx + pcry->tbl[eax+0x312];

			for (x = 0; x <= 5; x++)
			{
				ebx=ebx ^ pcry->tbl[(x*2)+2];
				esi= esi ^ ebx;
				ebx=esi >> 0x18;
				eax=(esi >> 0x10) & 0xFF;
				ebx=pcry->tbl[ebx+0x12]+pcry->tbl[eax+0x112];
				eax=(esi >> 8) & 0xff;
				ebx=ebx ^ pcry->tbl[eax+0x212];
				eax=esi & 0xff;
				ebx=ebx + pcry->tbl[eax+0x312];

				ebx=ebx ^ pcry->tbl[(x*2)+3];
				ecx= ecx ^ ebx;
				ebx=ecx >> 0x18;
				eax=(ecx >> 0x10) & 0xFF;
				ebx=pcry->tbl[ebx+0x12]+pcry->tbl[eax+0x112];
				eax=(ecx >> 8) & 0xff;
				ebx=ebx ^ pcry->tbl[eax+0x212];
				eax=ecx & 0xff;
				ebx=ebx + pcry->tbl[eax+0x312];
			}

			ebx=ebx ^ pcry->tbl[14];
			esi= esi ^ ebx;
			eax=esi >> 0x18;
			ebx=(esi >> 0x10) & 0xFF;
			eax=pcry->tbl[eax+0x12]+pcry->tbl[ebx+0x112];
			ebx=(esi >> 8) & 0xff;
			eax=eax ^ pcry->tbl[ebx+0x212];
			ebx=esi & 0xff;
			eax=eax + pcry->tbl[ebx+0x312];

			eax=eax ^ pcry->tbl[15];
			eax= ecx ^ eax;
			ecx=eax >> 0x18;
			ebx=(eax >> 0x10) & 0xFF;
			ecx=pcry->tbl[ecx+0x12]+pcry->tbl[ebx+0x112];
			ebx=(eax >> 8) & 0xff;
			ecx=ecx ^ pcry->tbl[ebx+0x212];
			ebx=eax & 0xff;
			ecx=ecx + pcry->tbl[ebx+0x312];

			ecx=ecx ^ pcry->tbl[16];
			ecx=ecx ^ esi;
			esi= pcry->tbl[17];
			esi=esi ^ eax;
			pcry->tbl[(ou / 4)+(edi / 4)]=esi;
			pcry->tbl[(ou / 4)+(edi / 4)+1]=ecx;
			edi=edi+8;
		}
		ou=ou+0x400;
	}
}


void CRYPT_PC_MixKeys(CRYPT_SETUP* pc)
{
    unsigned long esi,edi,eax,ebp,edx;
    edi = 1;
    edx = 0x18;
    eax = edi;
    while (edx > 0)
    {
        esi = pc->keys[eax + 0x1F];
        ebp = pc->keys[eax];
        ebp = ebp - esi;
        pc->keys[eax] = ebp;
        eax++;
        edx--;
    }
    edi = 0x19;
    edx = 0x1F;
    eax = edi;
    while (edx > 0)
    {
        esi = pc->keys[eax - 0x18];
        ebp = pc->keys[eax];
        ebp = ebp - esi;
        pc->keys[eax] = ebp;
        eax++;
        edx--;
    }
}

void CRYPT_PC_CreateKeys(CRYPT_SETUP* pc,unsigned long val)
{
    unsigned long esi,ebx,edi,eax,edx,var1;
    esi = 1;
    ebx = val;
    edi = 0x15;
    pc->keys[56] = ebx;
    pc->keys[55] = ebx;
    while (edi <= 0x46E)
    {
        eax = edi;
        var1 = eax / 55;
        edx = eax - (var1 * 55);
        ebx = ebx - esi;
        edi = edi + 0x15;
        pc->keys[edx] = esi;
        esi = ebx;
        ebx = pc->keys[edx];
    }
    CRYPT_PC_MixKeys(pc);
    CRYPT_PC_MixKeys(pc);
    CRYPT_PC_MixKeys(pc);
    CRYPT_PC_MixKeys(pc);
    pc->pc_posn = 56;
}

unsigned long CRYPT_PC_GetNextKey(CRYPT_SETUP* pc)
{    
    unsigned long re;
    if (pc->pc_posn == 56)
    {
        CRYPT_PC_MixKeys(pc);
        pc->pc_posn = 1;
    }
    re = pc->keys[pc->pc_posn];
    pc->pc_posn++;
    return re;
}

void CRYPT_PC_CryptData(CRYPT_SETUP* pc,void* data,unsigned long size)
{
    unsigned long x;
    for (x = 0; x < size; x += 4) *(unsigned long*)((unsigned long)data + x) ^= CRYPT_PC_GetNextKey(pc);
}

void pcdecryptcopy ( void* dest, void* source, unsigned size )
{
	CRYPT_PC_CryptData(p_cipher_ptr,source,size);
	memcpy (dest,source,size);
}

void pcencryptcopy ( void* dest, void* source, unsigned size )
{
	CRYPT_PC_CryptData(p_cipher_ptr,source,size);
	memcpy (dest,source,size);
}